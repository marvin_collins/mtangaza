Hi there, <br>
{{ $data['name'] }} just posted a request via the contact page as detailed below:<br>
Email: <br><strong>{{ $data['email'] }}</strong><br>
Subject: <br><strong>{{ title_case($data['subject']) }}</strong><br>
Message: <br><strong>{{ title_case($data['message']) }} </strong><br>