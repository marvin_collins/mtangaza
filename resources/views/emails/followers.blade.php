<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <title>Welcome</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        @import url('http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,300,600,700');
        .invoice-head td {
            padding: 0 8px;
        }
        .invoice-body{
            background-color:transparent;
        }
        .logo {
            padding-bottom: 10px;
        }
        .table th {
            vertical-align: bottom;
            font-weight: bold;
            padding: 8px;
            line-height: 20px;
            text-align: left;
            border-bottom: 1px groove #999;
        }
        .table td {
            padding: 8px;
            line-height: 20px;
            text-align: left;
            vertical-align: top;
            border-top: 1px solid #dddddd;
            border-bottom: 1px groove #999;
        }
        .well {
            margin-top: 15px;
        }
        .btn {
            display: inline-block;
            margin-bottom: 0;
            font-weight: 300;
            text-align: center;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            background-image: none;
            border: 1px solid transparent;
            white-space: nowrap;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.428571429;
            border-radius: 4px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
    </style>
</head>

<body style="background: #fff; font-size: 14px; font-family: 'Open Sans'; font-weight: 500;">
<div class="container" style="padding-top:30px; width: 80%; margin: 0 auto;">
    <p>Great! You have a new connection <br><br>
        Your business has just been connected to {{ $follower->name }}. Login to your account to confirm the request.
        <br><br>
        <a class="btn btn-primary" href="{{ route('client.login.index') }}">Click Here to Log In</a>
        <br><br>
        You are receiving this email because you have signed up for an account at mTangaza
        <br>
        We may need to communicate important service level issues with you from time to time, so it's important we have an up-to-date email address for you on file.
        <br><br>
        Thanks, mTangaza.
        <strong><small>This is an automated message. Please do not reply to this message.</small></strong>
    </p>
</div>
</body>
</html>

