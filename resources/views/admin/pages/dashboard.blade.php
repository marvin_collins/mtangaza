@extends('admin.layout')
@section('content')
<section class="content-header">
    <h1>
        Admin Dashboard
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        {{--<li><a href="#">Layout</a></li>--}}
        <li class="active">Dashboard</li>
    </ol>
</section>
<section class="content">
   <div class="container-fluid">
       <div class="raw">
           <div class="col-sm-6">
               <div class="box box-solid box-default">
                   <div class="box-header with-border">
                       <h3 class="box-title text-capitalize">Customer analysis</h3>
                   </div>
                   <div class="box-body">
                       <table class="table table-responsive table-condensed table-hover">
                           <thead>
                           <th>Business</th>
                           <th class="text-right">Total</th>
                           </thead>
                           <tbody>
                           <tr><td>Registered and Paid</td><td class="text-right">{{$registered}}</td></tr>
                           <tr><td>Registered and Non-Paid</td><td class="text-right">{{$notregistered}}</td></tr>
                           </tbody>
                       </table>
                   </div>
               </div>
               <div class="box box-solid box-default">
                   <div class="box-header with-border">
                       <h3 class="box-title text-capitalize">Hits Registered Today</h3>
                   </div>
                   <div class="box-body">
                       <table class="table table-responsive table-condensed table-hover">
                           <thead>
                           <th>Plan</th>
                           <th class="text-right">Total</th>
                           </thead>
                           <tbody>
                           <tr><td>SILVER</td><td class="text-right">{{$todayhits['silver']}}</td></tr>
                           <tr><td>GOLD</td><td class="text-right">{{$todayhits['gold']}}</td></tr>
                           <tr><td>PLATINUM</td><td class="text-right">{{$todayhits['platinum']}}</td></tr>
                           <tr><th>TOTAL</th><td class="text-right">{{$todayhits['silver'] + $todayhits['gold'] + $todayhits['platinum']}}</td></tr>
                           </tbody>
                       </table>
                   </div>
               </div>
               <div class="box box-solid box-default">
                   <div class="box-header with-border">
                       <h3 class="box-title text-capitalize">Handshake done</h3>
                   </div>
                   <div class="box-body">
                       <table class="table table-responsive table-condensed table-hover">
                           <thead>
                           <th>Date</th>
                           <th class="text-right">Total</th>
                           </thead>
                           <tbody>
                           <tr><td>TODAY</td><td class="text-right">{{$todayfollowers}}</td></tr>
                           <tr><td>THIS MONTH</td><td class="text-right">{{$monthfollowers}}</td></tr>
                           <tr><td>THIS YEAR</td><td class="text-right">{{$yearfollowers}}</td></tr>
                           <tr><th>TILL DATE</th><td class="text-right">{{$allfollowers}}</td></tr>
                           </tbody>
                       </table>            </div>
               </div>
           </div>
           <div class="col-sm-6">
               <div class="box box-solid box-default">
                   <div class="box-header with-border">
                       <h3 class="box-title text-capitalize">Plan used</h3>
                   </div>
                   <div class="box-body">
                       <table class="table table-responsive table-condensed table-hover">
                           <thead>
                           <th>Plan</th>
                           <th class="text-right">Total</th>
                           </thead>
                           <tbody>
                           <tr><td>SILVER</td><td class="text-right">{{$silver}}</td></tr>
                           <tr><td>GOLD</td><td class="text-right">{{$gold}}</td></tr>
                           <tr><td>PLATINUM</td><td class="text-right">{{$platinum}}</td></tr>
                           <tr><th>TOTAL</th><td class="text-right">{{$silver+$gold+$platinum}}</td></tr>
                           </tbody>
                       </table>
                   </div>
               </div>
               <div class="box box-solid box-default">
                   <div class="box-header with-border">
                       <h3 class="box-title text-capitalize">Hits registered total</h3>
                   </div>
                   <div class="box-body">
                       <table class="table table-responsive table-condensed table-hover">
                           <thead>
                           <th>Plan</th>
                           <th class="text-right">Total</th>
                           </thead>
                           <tbody>
                           <tr><td>SILVER</td><td class="text-right">{{$totalhits['silver']}}</td></tr>
                           <tr><td>GOLD</td><td class="text-right">{{$totalhits['gold']}}</td></tr>
                           <tr><td>PLATINUM</td><td class="text-right">{{$totalhits['platinum']}}</td></tr>
                           <tr><th>TOTAL</th><td class="text-right">
                                   {{$totalhits['silver'] + $totalhits['gold'] + $totalhits['platinum']}}
                               </td></tr>
                           </tbody>
                       </table>
                   </div>
               </div>
               <div class="box box-solid box-default">
                   <div class="box-header with-border">
                       <h3 class="box-title text-capitalize">New Business Registered</h3>
                   </div>
                   <div class="box-body">
                       <table class="table table-responsive table-condensed table-hover">
                           <thead>
                           <th>Date</th>
                           <th class="text-right">Total</th>
                           </thead>
                           <tbody>
                           <tr><td>TODAY</td><td class="text-right">{{$todaybusinesses}}</td></tr>
                           <tr><td>THIS MONTH</td><td class="text-right">{{$monthbusinesses}}</td></tr>
                           <tr><td>THIS YEAR</td><td class="text-right">{{$yearbusinesses}}</td></tr>
                           <tr><th>TILL DATE</th><td class="text-right">{{$allbusinesses}}</td></tr>
                           </tbody>
                       </table>
                   </div>
               </div>
           </div>
       </div>
   </div>
</section>
@endsection