<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>mTangaza | Administrator</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{ asset('css/admin.css') }}">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <img src="{{ asset('images/top-logo.png') }}" alt="mTangaza">
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Change Password</p>

        <form action="{{ route('admin.login.update') }}" method="post">
            {{ csrf_field() }}
            {{ method_field('PUT') }}

            <div class="form-group has-feedback">
                <input type="password" class="form-control" name="old_password" placeholder="Old Password" required>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control" name="password" placeholder="New Password" required>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-4 col-sm-offset-2">
                    <input type="submit" class="btn btn-primary btn-block btn-flat" value="Save">
                </div>
                <div class="col-xs-4">
                    <a class="btn btn-primary btn-block btn-flat" href="{{ URL::previous() }}">Cancel</a>
                </div>
            </div>
        </form>
    </div>
    <!-- /.login-box-body -->
</div>
<script src="{{ asset('js/admin.js') }}"></script>

@if(session()->has('flash_message'))
    <script>
        $(function() {
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "positionClass": "toast-top-right",
                "onclick": null,
                "showDuration": "1000",
                "hideDuration": "1000",
                "timeOut": "9000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
            toastr['{{ session('flash_message_status') }}']("{!! session('flash_message') !!}");
        });
    </script>
@endif
@if(count($errors) > 0)
    <?php
    $displayErrors = "";
    foreach($errors->all() as $error)
    {
        $displayErrors .= $error . "<br>";
    }
    ?>

    <script>
        $(function() {
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "positionClass": "toast-top-right",
                "onclick": null,
                "showDuration": "1000",
                "hideDuration": "1000",
                "timeOut": "9000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
            toastr['error']("{!! $displayErrors !!}");
        });
    </script>
@endif
</body>
</html>
