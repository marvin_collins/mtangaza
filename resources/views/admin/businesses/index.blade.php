@extends('admin.layout')
@section('content')
<section class="content-header">
    <h1>
        Businesses
        <small>Manage the businesses that are registered in the system.</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="{{ route('manage.businesses.index') }}">Businesses</a></li>
    </ol>
</section>
<section class="content">
    <div class="box box-solid box-danger">
        <div class="box-header with-border">
            <h3 class="box-title">Currently Registered Businesses</h3>
            <div class="box-tools pull-right">
                <a href="{{ route('manage.businesses.create') }}" class="btn btn-xs btn-primary">Add Business</a>
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <table class="table table-responsive table-condensed table-hover data-table">
                <thead>
                <tr class="success">
                    <th>Business Name</th>
                    <th>Category</th>
                    <th>E-Mail</th>
                    <th>Contacts</th>
                    <th>Status</th>
                    <th>Created At</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($businesses as $business)
                    <tr>
                        <td><a href="{{ route('manage.businesses.show', $business->id) }}">{{ $business->name }}</a></td>
                        <td><a href="{{ route('manage.categories.show', $business->category->id) }}">{{ $business->category->name }}</a></td>
                        <td>{{ $business->email }}</td>
                        <td>{{ $business->contact_numbers }}</td>
                        <td><span class="btn btn-xs {{ $business->active == 0 ? 'btn-danger' : 'btn-success' }}">{{ $business->active == 0 ? 'Unpublished' : 'Published' }}</span></td>
                        <td>{{ $business->created_at->format('d F, Y') }}</td>
                        <td><a class="btn btn-xs btn-info" href="{{ route('manage.businesses.edit', $business->id) }}">Edit</a></td>
                        <td><a class="btn btn-xs {{ $business->active == 1 ? 'btn-danger' : 'btn-success' }}" href="{{ route('manage.businesses.state', $business->id) }}">{{ $business->active == 1 ? 'Unpublish' : 'Publish' }}</a></td>
                        <td><a class="btn btn-xs btn-danger" href="{{ route('manage.businesses.destroy', $business->id) }}" data-method="delete" rel="nofollow" data-confirm="Are you sure you want to delete this?" data-token="{{ csrf_token() }}">Delete</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

</section>
<section class="content">
    <div class="box box-solid box-danger">
        <div class="box-header with-border">
            <h3 class="box-title">Pending Businesses Changes</h3>
            {{--<div class="box-tools pull-right">--}}
                {{--<a href="{{ route('manage.businesses.create') }}" class="btn btn-xs btn-primary">Add Business</a>--}}
                {{--<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">--}}
                    {{--<i class="fa fa-minus"></i>--}}
                {{--</button>--}}
            {{--</div>--}}
        </div>
        <div class="box-body">
            <table class="table table-responsive table-condensed table-hover data-table">
                <thead>
                <tr class="success">
                    <th>Business Name</th>
                    <th>Category</th>
                    <th>E-Mail</th>
                    <th>Contacts</th>
                    <th>Status</th>
                    <th>Created At</th>
                </tr>
                </thead>
                <tbody>
                @foreach($pending as $business)
                    <tr>
                        <td><a href="{{ route('manage.pending.show', $business->id) }}">{{ $business->name }}</a></td>
                        <td><a href="{{ route('manage.categories.show', $business->category->id) }}">{{ $business->category->name }}</a></td>
                        <td>{{ $business->email }}</td>
                        <td>{{ $business->contact_numbers }}</td>
                        <td><span class="btn btn-xs {{ $business->active == 0 ? 'btn-danger' : 'btn-success' }}">{{ $business->active == 0 ? 'Unpublished' : 'Published' }}</span></td>
                        <td>{{ $business->created_at->format('d F, Y') }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

</section>
@endsection