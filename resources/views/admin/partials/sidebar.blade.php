<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <ul class="sidebar-menu">
            <li>
                <a href="{{ route('admin.home') }}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <li class="header">System Configuration</li>
            <li>
                <a href="{{ route('manage.users.index') }}">
                    <i class="fa fa-users"></i> <span>System Users</span>
                </a>
            </li>
            <li>
                <a href="{{ route('manage.features.index') }}">
                    <i class="fa fa-ticket"></i> <span>Features</span>
                </a>
            </li>
            <li>
                <a href="{{ route('manage.plans.index') }}">
                    <i class="fa fa-cc-visa"></i> <span>Packages & Plans</span>
                </a>
            </li>
            <li>
                <a href="{{ route('manage.categories.index') }}">
                    <i class="fa fa-list-alt"></i> <span>Business Categories</span>
                </a>
            </li>
            <li>
                <a href="{{ route('manage.fields.index') }}">
                    <i class="fa fa-building"></i> <span>Fields of Work</span>
                </a>
            </li>
            <li class="header">Clients & Businesses</li>
            <li>
                <a href="{{ route('manage.clients.index') }}">
                    <i class="fa fa-asl-interpreting"></i> <span>Clients</span>
                </a>
            </li>
            <li>
                <a href="{{ route('manage.businesses.index') }}">
                    <i class="fa fa-suitcase"></i> <span>Businesses</span>
                </a>
            </li>
            <li>
                <a href="{{ route('manage.payments.index') }}">
                    <i class="fa fa-money"></i> <span>Payments</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
