<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; {{ \Carbon\Carbon::now()->year }} <a href="http://wizag.biz">Wise & Agile Solutions Limited</a>.</strong> All rights
    reserved.
</footer>