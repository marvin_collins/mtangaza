@extends('admin.layout')
@section('content')
    <section class="content-header">
        <h1>
            Packages & Plans
            <small>Manage the plans available to the users.</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="{{ route('manage.plans.index') }}">Packages & Plans</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-solid box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Package & Plan details</h3>
                <div class="box-tools pull-right">

                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>
                                <input type="checkbox" class="icheck" id="all">  Select All
                            </label>
                            <label>
                                <input type="checkbox" class="icheck" id="inverse"> Select inverse
                            </label>
                            <label>
                                <input type="checkbox" class="icheck" id="clear"> Clear
                            </label>
                        </div>
                    </div>
                    <form action="{{ route('manage.plans.update', $plan->id) }}" role="form" method="post">
                        {{ csrf_field() }}
                        {{ method_field('put') }}
                        <div class="container">
                            <p>Please check the features that will be available on the package</p>
                            @foreach($features as $feature)
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>
                                            <input name="feature_{{ $feature->id }}" type="checkbox" class="icheck checkbox" {{ is_null($plan->features->whereLoose('id', $feature->id)->first()) ? '' : 'checked' }}>
                                            {{ $feature->name }}
                                        </label>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="cost">Package Cost</label>
                                <input type="number" name="cost" class="form-control" id="cost" value="{{ $plan->cost }}">
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary" value="Edit Plan">
                                <input type="reset" class="btn btn-warning" value="Clear Form">
                                <a href="{{ URL::previous() }}" class="btn btn-danger">Back</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('footer')
    <script>
        $('#all').on('ifChecked', function () {
            $('#clear').iCheck('uncheck');
            $('#inverse').iCheck('uncheck');
           $.each($('.checkbox'),function (index,values) {
               $(values).iCheck('check');
           })
        });
        $('#clear').on('ifChecked', function () {
            $('#all').iCheck('uncheck');
            $('#inverse').iCheck('uncheck');
           $.each($('.checkbox'),function (index,values) {
               $(values).iCheck('uncheck');
           })
        });
        $('#inverse').on('ifChecked', function () {
            $('#all').iCheck('uncheck');
            $('#clear').iCheck('uncheck');
           $.each($('.checkbox'),function (index,values) {
               $(values).iCheck('toggle');
           })
        });
    </script>

    @endsection