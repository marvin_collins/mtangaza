@extends('admin.layout')
@section('content')
<section class="content-header">
    <h1>
        Packages & Plans
        <small>Manage the plans available to the users.</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="{{ route('manage.plans.index') }}">Packages & Plans</a></li>
    </ol>
</section>
<section class="content">
    <div class="box box-solid box-danger">
        <div class="box-header with-border">
            <h3 class="box-title">Current Packages & Plans</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <table class="table table-responsive table-condensed table-hover data-table">
                <thead>
                <tr class="success">
                    <th>Name</th>
                    <th>Description</th>
                    <th class="text-center">Cost</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($plans as $plan)
                    <tr>
                        <td><a href="{{ route('manage.plans.show', $plan->id) }}">{{ title_case(str_replace('_', ' ', $plan->name))}}</a></td>
                        <td>{{ $plan->description }}</td>
                        <td class="text-right">KES {{ number_format($plan->cost, 2) }}</td>
                        <td class="text-center"><a class="btn btn-xs btn-info" href="{{ route('manage.plans.edit', $plan->id) }}">Edit</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

</section>
@endsection