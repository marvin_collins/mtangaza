@extends('admin.layout')
@section('content')
    <section class="content-header">
        <h1>
            Packages & Plans
            <small>Manage the plans available to the users.</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="{{ route('manage.plans.index') }}">Packages & Plans</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-solid box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Package & Plan details</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="container">
                        <p>The {{ str_replace('_',' ', title_case($plan->name)) }} Package Plan has the following modules</p>
                        @foreach($features as $feature)
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>
                                        <input name="feature_{{ $feature->id }}" type="checkbox" class="icheck" {{ is_null($plan->features->whereLoose('id', $feature->id)->first()) ? '' : 'checked' }} disabled>
                                        {{ $feature->name }}
                                    </label>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="col-md-6">
                        <div class="container">
                            <strong>Package Cost: </strong>
                            KShs. {{ number_format($plan->cost, 2) }}
                        </div>
                        <hr>
                        <div class="form-group">
                            <a class="btn btn-info" href="{{ route('manage.plans.edit', $plan->id) }}">Edit</a>
                            <a href="{{ URL::previous() }}" class="btn btn-danger">Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection