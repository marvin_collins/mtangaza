@extends('admin.layout')
@section('content')
    <section class="content-header">
        <h1>
            Payments
            <small>Manage your payments.</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="{{ route('manage.payments.index') }}">Payments</a></li>
            <li class="active">View Business</li>
        </ol>
    </section>

    <section class="content">
        <div class="box box-solid box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Payment Details</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <h5>Business Details</h5>
                        <hr>
                        <div class="form-group">
                            <label for="name">Business Name*</label>
                            <h5>
                                <a href="{{ route('manage.businesses.show', $payment->business->id) }}">{{ $payment->business->name }}</a></h5>
                        </div>
                        <div class="form-group">
                            <label for="last_name">Business Logo</label>
                            <img src="{{ asset($payment->business->logo) }}" alt="logo" class="img-responsive">
                        </div>
                        <div class="form-group">
                            <label for="email">Company Email</label>
                            <p>{{ $payment->business->email }}</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="contact_numbers">Paybill Number</label>
                            <p>{{ $payment->paybill_number }}</p>
                        </div>
                        <div class="form-group">
                            <label for="city_id">Account Number</label>
                            <p>{{ $payment->account_number }}</p>
                        </div>
                        <div class="form-group">
                            <label for="website">Amount Paid</label>
                            <p>{{ number_format($payment->amount_paid, 2) }}</p>
                        </div>
                        <div class="form-group">
                            <label for="twitter">Transaction Number</label>
                            <p>{{ $payment->transaction_number }}</p>
                        </div>

                        <div class="form-group">
                        @if($payment->status == 0)
                            <a href="{{ route('manage.payments.edit', ['act' => 'approve', 'payments' => $payment->id]) }}" class="btn btn-success">Approve & Create Subscription</a>
                            <a href="{{ route('manage.payments.edit', ['act' => 'disapprove', 'payments' => $payment->id]) }}" class="btn btn-warning">Disapprove</a>
                        @endif
                            <a href="{{ URL::previous() }}" class="btn btn-danger">Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
