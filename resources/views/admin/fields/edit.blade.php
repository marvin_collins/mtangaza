@extends('admin.layout')
@section('content')
    <section class="content-header">
        <h1>
            Field of Work
            <small>Manage the field of work that are registered in the system.</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="{{ route('manage.fields.index') }}">Field of Work</a></li>
            <li class="active"><a href="#">Edit</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-solid box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Edit field of work</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <form action="{{ route('manage.fields.update', $field->id) }}" role="form" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('put') }}
                        <input type="hidden" name="fieldId" value="{{ $field->id }}">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Category Name*</label>
                                <input value="{{ $field->name }}" type="text" name="name" id="name" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>
                                    @if($field->image_path != 'null')
                                    <img id="icon" width="35px" height="35px" src="{{asset($field->image_path)}}">
                                    @endif
                                    <input name="featured" {{$field->featured == 0 ? '' : 'checked'}} type="checkbox" id="change" class="icheck">
                                    Set Featured
                                </label>
                            </div>
                            <div id="image" class="form-group">
                                <label for="image">Field image</label>
                                <input name="image_path" type="file" />
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary" value="Save Changes">
                                <input type="reset" class="btn btn-danger" value="Clear Form">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('footer')
    <script>
        $(document).ready(function () {
            var icon = '{{$field->image_path}}';
            if(icon == 'null')
            {
                $('#image').hide();
                console.log(icon);
            }
        })
        $('#change').on('ifChecked', function() {
            $('#image').show();
            $('#icon').show();
        });
        $('#change').on('ifUnchecked', function() {
            $('#image').hide();
            $('#icon').hide();
        });
    </script>
@endsection