@extends('admin.layout')
@section('content')
    <section class="content-header">
        <h1>
            Field of Work
            <small>Manage the field of work that are registered in the system.</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="{{ route('manage.fields.index') }}">Field of Work</a></li>
            <li class="active"><a href="#">Create</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-solid box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Add new field of work</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <form action="{{ route('manage.fields.store') }}" role="form" method="post" file="true" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Field of Work*</label>
                                <input value="{{ old('name') }}" type="text" name="name" id="name" class="form-control" required>
                            </div>
                            @if(\SmoDav\Models\FieldOfWork::where('featured',1)->get()->count() != 12)
                            <div class="form-group">
                                <label>
                                <input name="featured" type="checkbox" id="change" class="icheck">
                                    Set Featured
                                </label>
                            </div>
                            <div id="image" class="form-group">
                                <label for="image">Field image</label>
                                <input name="image" type="file" />
                            </div>
                            @endif
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary" value="Create Field">
                                <input type="reset" class="btn btn-danger" value="Clear Form">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

@endsection
@section('footer')
    <script>
        $('#image').hide();
      $('#change').on('ifChecked', function() {
          $('#image').show();
      });
        $('#change').on('ifUnchecked', function() {
            $('#image').hide();
      });
    </script>
@endsection