@extends('admin.layout')
@section('content')
<section class="content-header">
    <h1>
        Clients
        <small>Manage the clients that are registered in the system.</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="{{ route('manage.clients.index') }}">Clients</a></li>
    </ol>
</section>
<section class="content">
{{--@if(session()->has('client_email'))--}}
    {{--<div class="row">--}}
            {{--<div class="col-sm-8 col-sm-offset-4">--}}
                {{--<div class="box box-solid box-danger">--}}
                    {{--<div class="box-header with-border">--}}
                        {{--<h5 class="box-title">New Client Login Details</h5>--}}
                        {{--<div class="box-tools pull-right">--}}
                            {{--<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">--}}
                                {{--<i class="fa fa-minus"></i>--}}
                            {{--</button>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="box-body">--}}
                        {{--<p>Please note that this will only be displayed once. A copy of the same has been sent to the registered email.</p>--}}
                        {{--<table class="table table-responsive table-condensed table-hover">--}}
                            {{--<tr>--}}
                                {{--<td width="200"><strong>Email</strong></td>--}}
                                {{--<td>{{ session('client_email') }}</td>--}}
                            {{--</tr>--}}
                            {{--<tr>--}}
                                {{--<td><strong>Password</strong></td>--}}
                                {{--<td>{{ session('client_password') }}</td>--}}
                            {{--</tr>--}}
                        {{--</table>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
{{--@endif--}}
    <div class="box box-solid box-danger">
        <div class="box-header with-border">
            <h3 class="box-title">Current System Clients</h3>
            <div class="box-tools pull-right">
                <a href="{{ route('manage.clients.create') }}" class="btn btn-xs btn-primary">Add Client</a>
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <table class="table table-responsive table-condensed table-hover data-table">
                <thead>
                <tr class="success">
                    <th>Email</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Mobile</th>
                    <th>User Group</th>
                    <th>Last Login</th>
                    <th>Created At</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($clients as $client)
                    <tr>
                        <td><a href="{{ route('manage.clients.show', $client->id) }}">{{ $client->email }}</a></td>
                        <td>{{ $client->first_name }}</td>
                        <td>{{ $client->last_name }}</td>
                        <td>{{ $client->mobile }}</td>
                        <td>{{ $client->user->roles->first()->name }}</td>
                        <td>{{ is_null($client->user->last_login) ? 'Never' : \Carbon\Carbon::parse($client->user->last_login)->diffForHumans() }}</td>
                        <td>{{ $client->user->created_at->format('d F, Y') }}</td>
                        <td><a class="btn btn-xs btn-info" href="{{ route('manage.clients.edit', $client->id) }}">Edit</a></td>
                        <td><a class="btn btn-xs {{ $client->user->active == 1 ? 'btn-danger' : 'btn-success' }}" href="{{ route('manage.clients.state', $client->user->id) }}">{{ $client->user->active == 1 ? 'Deactivate' : 'Activate' }}</a></td>
                        <td><a class="btn btn-xs btn-danger" href="{{ route('manage.clients.destroy', $client->id) }}" data-method="delete" rel="nofollow" data-confirm="Are you sure you want to delete this?" data-token="{{ csrf_token() }}">Delete</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

</section>
@endsection