@extends('admin.layout')
@section('content')
    <section class="content-header">
        <h1>
            Clients
            <small>Manage the clients that are registered in the system.</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('manage.clients.index') }}">Clients</a></li>
            <li class="active">View Client Details</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-solid box-default">
            <div class="box-header with-border">
                <h3 class="box-title">{{ $client->first_name . ' ' . $client->last_name }} details</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <hr>
{{--                            {{json_encode($client)}}--}}
                            <span class="display-label">Title:</span>
                            <span><strong>{{ $client->title }}</strong></span>
                            <hr>
                            <span class="display-label">First Name:</span>
                            <span><strong>{{ $client->first_name }}</strong></span>
                            <hr>
                            <span class="display-label">Last Name:</span>
                            <span><strong>{{ $client->last_name }}</strong></span>
                            <hr>
                            <span class="display-label">Nationality:</span>
                            <span><strong>{{ $client->nationality }}</strong></span>
                            <hr>
                            <span class="display-label">Gender:</span>
                            <span><strong>{{ $client->gender == 'M' ? 'Male' : '' }}{{ $client->gender == 'U' ? 'Undisclosed' : '' }}{{ $client->gender == 'F' ? 'Female' : '' }}</strong></span>
                            <hr>
                        </div>
                        <div class="col-md-6">
                            <hr>
                            <span class="display-label">{{ $client->identification_type }}:</span>
                            <span><strong>{{ $client->identification_number }}</strong></span>
                            <hr>
                            <span class="display-label">Email:</span>
                            <span><strong>{{ $client->email }}</strong></span>
                            <hr>
                            <span class="display-label">Mobile:</span>
                            <span><strong>{{ $client->mobile }}</strong></span>
                            <hr>
                            <span class="display-label">Address:</span>
                            <span><strong>Kenya - {{ $client->city }}-{{$client->address1}}-{{empty($client->address2) ? '' : $client->address2 }}</strong></span>
                            <hr>
                            <div class="form-group">
                                <a href="{{ route('manage.clients.edit', $client->id) }}" class="btn btn-primary">Edit Account</a>
                                <a href="{{ URL::previous() }}" class="btn btn-danger">Back</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection