@extends('admin.layout')
@section('content')
<section class="content-header">
    <h1>
        Features
        <small>Manage the features available to the clients.</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="{{ route('manage.features.index') }}">Features</a></li>
    </ol>
</section>
<section class="content">
    <div class="box box-solid box-danger">
        <div class="box-header with-border">
            <h3 class="box-title">System Features</h3>
            <div class="box-tools pull-right">
                <a class="btn btn-xs btn-success" href="{{ route('manage.features.edit', 'all-1') }}">Activate All</a>
                <a class="btn btn-xs btn-primary" href="{{ route('manage.features.edit', 'all-0') }}">Deactivate All</a>
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <table class="table table-responsive table-condensed table-hover data-table">
                <thead>
                <tr class="success">
                    <th>Feature Name</th>
                    <th>Description</th>
                    <th>State</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($features as $feature)
                    <tr>
                        <td>{{ $feature->name }}</td>
                        <td>{{ $feature->description }}</td>
                        <td>{{ $feature->active == 1 ? 'Active' : 'Disabled' }}</td>
                        <td><a class="btn btn-xs {{ $feature->active == 1 ? 'btn-danger' : 'btn-success' }}" href="{{ route('manage.features.edit', $feature->id) }}">{{ $feature->active == 1 ? 'Deactivate' : 'Activate' }}</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</section>
@endsection