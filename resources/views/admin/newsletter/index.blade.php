@extends('admin.layout')
@section('content')
    <section class="content-header">
        <h1>
            Newsletter
            <small>Manage the newsletters you send to subscribers</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('manage.clients.index') }}">Newsletter</a></li>
            {{--<li class="active">Add Client</li>--}}
        </ol>
    </section>
    <section class="content">
        <div class="box box-solid box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Add new newsletter</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <form action="{{ url('/manage/newsletter/email') }}" enctype="multipart/form-data" role="form" method="post">
                        {{ csrf_field() }}
                        <div class="col-md-8 col-md-offset-2">
                            <div class="form-group">
                                <label for="subject">Email Subject*</label>
                                <input value="{{ old('subject') }}" type="text" name="subject" id="subject" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="body">Email Body*</label>
                                <textarea required name="body" id="body" class="form-control">{{ old('body') }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="files">Email Attachment</label>
                                <input type="file" name="files[]" id="files" multiple class="form-control">
                                <div id="show_image" class="well well-xs">
                                </div>
                                <p id="setbtn" onclick="reset()" class="btn pull-right btn-sm btn-warning">Reset selection</p>
                            </div>
                            <div class="row"></div>
                            <div style="margin-top: 12px" class="form-group pull-right">
                                <input type="submit" class="btn btn-primary" value="Send email">
                                <input type="reset" class="btn btn-danger" value="Clear Form">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('footer')
    <script type="text/javascript">
        $('#show_image').hide();
        $('#setbtn').hide();
        $('#files').change(function () {
            $('#show_image').show();
            $('#setbtn').show();
            data =  '<div class="alert alert-info " role="alert">'+
                    $("#files")[0].files[0].name+'</div>';
            $('#show_image').append(data);
        });
        function reset() {
            $("#images").val("");
            $.each($('#show_image div'),function (indx,vall) {
                $(vall).remove();
            });
            $('#show_image').hide();
            $('#setbtn').hide();
        }
        $("#body").wysihtml5();
        $('#title').select2(
                {
                    tags: true,
                    tokenSeparators: [',', ' ']
                }
        );
    </script>
@endsection