@extends('admin.layout')
@section('content')
<section class="content-header">
    <h1>
        Categories
        <small>Manage the categories that are registered in the system.</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="{{ route('manage.categories.index') }}">Categories</a></li>
    </ol>
</section>
<section class="content">
    <div class="box box-solid box-danger">
        <div class="box-header with-border">
            <h3 class="box-title">Current Setup Categories</h3>
            <div class="box-tools pull-right">
                <a href="{{ route('manage.categories.create') }}" class="btn btn-xs btn-primary">Add Category</a>
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <table class="table table-responsive table-condensed table-hover data-table">
                <thead>
                <tr class="success">
                    <th>Category Name</th>
                    <th>Created At</th>
                    <th>Updated At</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($categories as $category)
                    <tr>
                        <td><a href="{{ route('manage.categories.show', $category->id) }}">{{ $category->name }}</a></td>
                        <td>{{ $category->created_at->format('d F Y') }}</td>
                        <td>{{ $category->updated_at->format('d F Y') }}</td>
                        <td><a class="btn btn-xs btn-info" href="{{ route('manage.categories.edit', $category->id) }}">Edit</a></td>
                        <td><a class="btn btn-xs btn-danger" href="{{ route('manage.categories.destroy', $category->id) }}" data-method="delete" rel="nofollow" data-confirm="Are you sure you want to delete this?" data-token="{{ csrf_token() }}">Delete</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

</section>
@endsection