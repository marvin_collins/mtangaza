@extends('admin.layout')
@section('content')
    <section class="content-header">
        <h1>
            Categories
            <small>Manage the categories that are registered in the system.</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('manage.categories.index') }}">Categories</a></li>
            <li class="active"><a href="#">Create</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-solid box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Add new category</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <form action="{{ route('manage.categories.store') }}" role="form" method="post">
                        {{ csrf_field() }}
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Category Name*</label>
                                <input value="{{ old('name') }}" type="text" name="name" id="name" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary" value="Create Category">
                                <input type="reset" class="btn btn-danger" value="Clear Form">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection