@extends('client.layout')
@section('content')
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.3/js/bootstrap.min.js"
            integrity="sha384-ux8v3A6CPtOTqOzMKiuo3d/DomGaaClxFYdCu2HPMBEkf6x2xiDyJ7gkXU0MWwaD"
            crossorigin="anonymous"></script>

    <section class="content-header">
        <h1>
            Handshake
            <small>You have a handshake with {{$businessdetails->name}}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('client.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="{{ route('client.businesses.index') }}">Businesses</a></li>
            <li class="active">View Business</li>
        </ol>
    </section>
    {{--    {{$businessdetails}}--}}

    <section class="content">
        <div class="box box-solid box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Business Profile</h3>

                <div class="box-tools pull-right">
                    <a href="{{url()->previous()}}">
                        <button type="button" class="btn btn-danger">Back</button>
                    </a>
                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#shareDoc">Share
                        Doc
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">

                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12">
                                <h2>{{$businessdetails->name}}</h2>

                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <img style="margin: 0 auto;" src="{{ asset($businessdetails->logo) }}"
                                             alt="logo"
                                             class="img-responsive">
                                    </div>

                                </div>
                            </div>


                            <div class="col-md-12">
                                <h2>Category</h2><span><h4>{{\SmoDav\Models\Category::select('name')
                                ->where('id',$businessdetails->category_id)->pluck('name')->first()}}</h4></span>

                                <h3>Description</h3>

                                <div class="panel panel-default">
                                    <div class="panel-body">

                                        {!!$businessdetails->description!!}
                                    </div>

                                </div>
                            </div>

                            {{--                                    {{dd(\SmoDav\Models\BusinessImage::all()->toArray())}}--}}

                            @foreach(\SmoDav\Models\BusinessImage::select('image','business_id')->where('business_id',$businessdetails->id)
                            ->get() as $image)
                                <?php
                                if (!empty(\SmoDav\Models\BusinessImage::select('image', 'business_id')
                                        ->where('business_id', $businessdetails->id)
                                        ->get())
                                ) {
                                    echo '<div class="col-md-12">
                            <h2>Business Images</h2>

                            <div class="panel panel-default">
                                <div class="panel-body">';
                                    $imageLocation = $image['image'];
                                    $parts = explode('/', $imageLocation);
                                    $imageIndex = count($parts) - 1;
                                    $imageThumb = 'thumb-' . $parts[$imageIndex];
                                    $parts[$imageIndex] = $imageThumb;
                                    $imagePath = implode('/', $parts);

                                    echo '<div class="col-sm-2">
                                            <img src="' . asset($imagePath) . '" alt="' . $businessdetails->name . '"
                                            class="img-responsive">
                                        </div>

                                                </div>

                            </div>
                            </div>
                        ';
                                }
                                ?>
                            @endforeach
{{--{{dd($sharedDocs->flatten()->toArray())}}--}}
                            @if(!empty($sharedDocs))
                            <div class="col-md-12">
                                <h2>Shared Documents</h2>
                                <div class="panel panel-default">
                                    <h4>Public</h4>
                                    <div class="panel-body">
                            <?php
                            foreach($sharedDocs as $docs){

                                if(!empty($docs['url'])){
                                    echo '<a href="'. asset($docs['url']).'"  class="btn btn-default"
                                download>'.$docs['name'].'</a>';
                                }
                            }

                                  echo  '</div><h4>Shared with you</h4><div class="panel-body">';
                                            foreach($specificShares as $specificShare){
                                    if(!empty($specificShare['url'])){
                                        echo '<a href="'. asset($specificShare['url']).'"  class="btn btn-default"
                                download>'.$specificShare['name'].'</a>';
                                    }}
//                                            foreach($businessTobusinessFile as $file){
//                                                if(empty($file['file_path'])){
//                                            echo '<a href="'. asset
//                                                    ($file['file_url']).'"  class="btn btn-default"
//                                download>'.$file['name'].'</a>';
//                                            }}
                                        echo    '</div>';?>


                                </div>
                            </div>
                                @endif

                        </div>


                    </div>
                    <div class="col-md-6">
                        <div class="col-md-12">
                            <h2>Contact details</h2>

                            <ul class="list-group">
                                <li class="list-group-item">
                                    <span class="badge">Contact person</span>
                                    {{$businessdetails->contact_person}} - {{$businessdetails->contact_numbers}}
                                </li>
                                {!! $businessdetails->email == "" ? ' ': '<li class="list-group-item">
                                    <span class="badge">Email</span>'.
                                    $businessdetails->email.
                                '</li>'!!}
                                {!! $businessdetails->website == "" ? ' ': '<li class="list-group-item">
                                    <span class="badge">Website</span>'.
                                    $businessdetails->website.
                                '</li>'!!}
                                {!! $businessdetails->twitter == "" ? ' ': '<li class="list-group-item">
                                    <span class="badge">Twitter</span>'.
                                    $businessdetails->twitter.
                                '</li>'!!}
                                {!! $businessdetails->facebook == "" ? ' ': '<li class="list-group-item">
                                    <span class="badge">Facebook</span>'.
                                    $businessdetails->facebook.
                                '</li>'!!}

                                {!! $businessdetails->linked_in == "" ? ' ': '<li class="list-group-item">
                                    <span class="badge">Linked_in</span>'.
                                    $businessdetails->linked_in.
                                '</li>'!!}
                                {{--{!! $businessdetails->skype == "" ? ' ': '<li class="list-group-item">--}}
                                {{--<span class="badge">Facebook</span>'.--}}
                                {{--$businessdetails->skype.--}}
                                {{--'</li>'!!}--}}
                                {!! $businessdetails->skype == "" ? ' ': '<li class="list-group-item">
                                    <span class="badge">Skype</span>'.
                                    $businessdetails->skype.
                                '</li>'!!}
                            </ul>
                        </div>
                        <div class="col-md-12">
                            <h2>Map Location</h2>

                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <p id="map_location" value="{{
                                    $businessdetails->map_location }}"></p>

                                    <div id="map"></div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section>
        <div id="imagesView" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Business Images</h4>
                    </div>
                    <div class="modal-body">
                        <div id="business_images" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner" role="listbox">
                                {{--@foreach($business->images as $image)--}}
                                {{--<div class="item{{ $image->id == $business->images->first()->id ? ' active' : '' }}">--}}
                                {{--<img src="{{ asset($image->image) }}" alt="{{ $business->name }}" class="img-responsive">--}}
                                {{--</div>--}}
                                {{--@endforeach--}}
                            </div>
                            <a class="left carousel-control" href="#business_images" role="button" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#business_images" role="button" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Share Modal -->
    <div class="modal fade" id="shareDoc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="myModalLabel">Share document
                        with {{$businessdetails->name}}</h4>
                </div>
                <div class="modal-body">
                    <div class="text-content">
                        <div class="span7 offset1">{!! Form::open(
    array(
        'url' => array('upload'),
        'class' => 'form',
        'novalidate' => 'novalidate',
        'enctype'=>'multipart/form-data',
        'files' => true)) !!}

                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">Document name</span>
                                {!! Form::text('name', null, array(null=>'Enter document name','required','placeholder'=>'Eg Price list',
                                'required'=>'required','class'=>'form-control','aria-describedby'=>'basic-addon1')) !!}
                            </div>
                            <input type="hidden" value="{{$businessdetails->id}}" name="follower_id"/>

                            <div style="margin-top: 5px" class="panel panel-default">
                                {!! Form::label('Select the file you want to share') !!}
                                <div class="panel-body">
                                    {!! Form::file('doc', null) !!}
                                </div>
                            </div>

                            <div style="margin-top: 5px" class="panel panel-default">
                                <h5>Add other users</h5>

                                <div class="panel-body form-group">

                                    {{--//form not working on multiple select check tomorrow--}}
                                    <select style="width:100%" name="business_id[]" id="business_id" class="form-control
                            select2" multiple>

                                        @foreach(\SmoDav\Models\Business::all() as $busines)
                                            {{--dd($city)--}}
                                            <option {{ in_array([$busines->id], [$busines->name]) ?                                                 'selected' : '' }} value="{{
                                     $busines->id }}">{{ $busines->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="row col-md-12">
                                <div class=" pull-right">

                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

                                    {!! Form::submit('Share document',array('class'=>'btn btn-primary')) !!}

                                </div>
                            </div>

                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    {{--<button type="button" class="btn btn-primary">Share document</button>--}}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@section('footer')
    <script src="http://maps.googleapis.com/maps/api/js?key={{ env('MAP_API') }}"></script>
    <script>
        $("#description").wysihtml5();
        var map_locations = $('#map_location');

        function initialize() {
            var mapProp = {
                center: new google.maps.LatLng(-1.3048035, 36.8473969),
                zoom: 12,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map(document.getElementById("map"), mapProp);
            setupMarkers(map);
        }

        function setupMarkers(map) {
            if (map_locations.val().length < 10) {
                return;
            }
            var locations = map_locations.val().split(';');
            var latLong = [];
            var position = null;
            for (var i = 0; i < locations.length; i++) {
                latLong = locations[i].split(',');
                position = new google.maps.LatLng(latLong[0], latLong[1]);
                var marker = new google.maps.Marker({
                    position: position,
                    map: map
                });
            }
            map.panTo(position);
        }

        function placeMarker(position, map) {
            var marker = new google.maps.Marker({
                position: position,
                map: map
            });

            google.maps.event.addListener(marker, 'click', function () {
                removeMarker(this);
            });
            map.panTo(position);

            var locations = [];
            if (map_locations.val().length > 2) {
                locations = [map_locations.val()];
            }
            locations.push(position.lat() + ',' + position.lng());
            map_locations.val(locations.join(';'));
        }

        google.maps.event.addDomListener(window, 'load', initialize);

        function loadImages() {
            $('.carousel').carousel({
                interval: 1000
            });
            $('#imagesView').modal('show');
        }

        $('.prodImages img').on('click', loadImages);

    </script>
@endsection