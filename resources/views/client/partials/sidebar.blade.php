<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <ul class="sidebar-menu">
            <li>
                <a href="{{ route('client.home') }}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <li class="header">Your Resources</li>
            <li>
                <a href="{{ route('client.users.index') }}">
                    <i class="fa fa-users"></i> <span>Authorised Users</span>
                </a>
            </li>
            <li>
                <a href="{{ route('client.businesses.index') }}">
                    <i class="fa fa-suitcase"></i> <span>Businesses</span>
                </a>
            </li>
            <li>
                <a href="{{ route('client.payments.index') }}">
                    <i class="fa fa-money"></i> <span>Payments</span>
                </a>
            </li>
            @if(\SmoDav\Models\Business::where('client_id',session('client_id'))->get()->count() > 0)
            <li>
                <a href="#">
                    <i class="fa fa-file-o"></i>
                    <span data-toggle="modal" data-target="#sideUpload">
                        Uploads</span>
                </a>
            </li>
            @endif
            <li class="header">Your Relations</li>
            <li>
                <a href="{{ route('client.interests.index') }}">
                    <i class="fa fa-link"></i> <span>Interests</span>
                </a>
            </li>
            <li>
                <a href="{{ route('client.handshakes.index') }}">
                    <i class="fa fa-users"></i> <span>Handshakes</span>
                </a>
            </li>
            <li>
                <a href="{{ route('client.inquiries.index') }}">
                    <i class="fa fa-envelope"></i> <span>Inquiries</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<!--share model -->
<div class="modal fade" id="sideUpload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel">Upload Business Document</h4>
            </div>
            <div class="modal-body">
                <div class="text-content">
                    <div class="span7 offset1">{!! Form::open(
    array(
        'url' => 'upload',
        'class' => 'form',
        'novalidate' => 'novalidate',
        'enctype'=>'multipart/form-data',
        'files' => true)) !!}

                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1">Document name</span>
                            {!! Form::text('name', null, array(null=>'Enter document name','required','placeholder'=>'Eg Price list',
                            'required'=>'required','class'=>'form-control','aria-describedby'=>'basic-addon1')) !!}
                        </div>

                        <div style="margin-top: 5px" class="panel panel-default">
                            {!! Form::label('Select file,') !!}<span>The file will be available to businesses that
                                follow you and those you follow</span>
                            <div class="panel-body">
                                {!! Form::file('doc', null) !!}
                            </div>
                        </div>

                        <div style="margin-top: 5px" class="panel panel-default">
                            <h5>Select the business for which you want to upload the file</h5>

                            <div class="panel-body form-group">

                                {{--//form not working on multiple select check tomorrow--}}
                                <select style="width:100%" required="required" name="selected[]" id="selected" class="form-control
                            select2" multiple>

                                    @foreach(\SmoDav\Models\Business::where('client_id',session('client_id'))->get() as $busines)
                                        <option {{ in_array([$busines->id], [$busines->name]) ? 'selected' : '' }} value="{{
                                     $busines->id }}">{{ $busines->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row col-md-12">
                            <div class=" pull-right">

                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

                                {!! Form::submit('Upload',array('class'=>'btn btn-primary')) !!}

                            </div>
                        </div>

                    </div>

                </div>
            </div>
            <div class="modal-footer">
                {{--<button type="button" class="btn btn-primary">Share document</button>--}}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
