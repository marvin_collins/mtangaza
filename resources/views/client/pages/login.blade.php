<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>mTangaza | Client</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{ asset('css/admin.css') }}">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition login-page">
@if(session()->has('Confirm'))
    <div class="row">
        <div class="login-logo">
            <img src="{{ asset('images/top-logo.png') }}" alt="mTangaza">
        </div>
        <h4 class="text-center">Welcome </h4>
        <div class="col-sm-6 col-sm-offset-4" style="margin: 0 auto; float: none">
            <div class="box box-solid box-success">
                <div class="box-header with-border">
                    <h5 class="box-title text-center center-block">Confirmation message</h5>
                    <div class="box-tools pull-right">
                        {{--<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">--}}
                            {{--<i class="fa fa-minus"></i>--}}
                        {{--</button>--}}
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-responsive table-condensed table-hover">
                        <tr>
                            <td width="200"><strong>Confirmation</strong></td>
                            <td>{{ session('Confirm') }}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <a href="/"><button class="btn btn-default text-center">Back to home</button></a>
        </div>
    </div>
@else
<div class="login-box">
    <div class="login-logo">
        <img src="{{ asset('images/top-logo.png') }}" alt="mTangaza">
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>

        <form action="{{ route('client.login.store') }}" method="post">
            {{ csrf_field() }}
            <div class="form-group has-feedback">
                <input type="email" class="form-control" name="email" placeholder="Email" required>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control" name="password" placeholder="Password" required>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <label>
                            <input type="checkbox" name="remember"> Remember Me
                        </label>
                    </div>
                </div>
                <div class="col-xs-4">
                    <input type="submit" class="btn btn-primary btn-block btn-flat" value="Sign In">
                </div>
            </div>
        </form>

    </div>@endif
    <!-- /.login-box-body -->
</div>
<script src="{{ asset('js/admin.js') }}"></script>

@if(session()->has('flash_message'))
    <script>
        $(function() {
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "positionClass": "toast-top-right",
                "onclick": null,
                "showDuration": "1000",
                "hideDuration": "1000",
                "timeOut": "9000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
            toastr['{{ session('flash_message_status') }}']("{!! session('flash_message') !!}");
        });
    </script>
@endif
@if(count($errors) > 0)
    <?php
    $displayErrors = "";
    foreach($errors->all() as $error)
    {
        $displayErrors .= $error . "<br>";
    }
    ?>

    <script>
        $(function() {
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "positionClass": "toast-top-right",
                "onclick": null,
                "showDuration": "1000",
                "hideDuration": "1000",
                "timeOut": "9000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
            toastr['error']("{!! $displayErrors !!}");
        });
    </script>
@endif
</body>
</html>
