@extends('client.layout')
@section('content')
<section class="content">
<h1 class="text-center">Welcome to mTangaza</h1>
<h3>Disclaimer</h3>
<p>
    This site may contain third party advertisements and links to third party sites.
    mTangaza does not take any responsibility as to the accuracy or suitability of any
    of the information, representations, offers and/or invitations to treat which
    are present and/or may be inferred in these advertisements or sites and does not
    accept any responsibility or liability for the conduct or content of those advertisements
    and sites and the representations, offers and/or invitations to treat made by the said third parties.
</p>
<p>
    Third party advertisements and links to other sites where goods or services are advertised,
    are not endorsements or recommendations by mTangaza of the third party sites, goods or services.
    mTangaza takes no responsibility for the content of the advertisements, representations made,
    or the quality/reliability of the products, services, or positions offered in the advertisements.
</p>
<p>
    mTangaza website, www.mtangaza.com, reserves the right to decline any advertising. Classified
    advertisements that mislead visitors are prohibited. The subject material should be directly
    related to the position posted in the advertisement.
</p>
<p>
    mTangaza is not responsible for the content of third party sites that are directly or indirectly
    linked from our site. Advertising on this site does not imply endorsement of the advertised 
    product, company or service.
</p>
<p>
    mTangaza shall have complete discretion to determine the acceptability of any and all 
    advertisements and advertisers. Advertising agreements may be terminated without notice by 
    mTangaza immediately if the advertiser posts improper or misleading content.
    In addition, mTangaza may, in its complete discretion, refuse to insert any other advertising
    that it deems inappropriate.
</p>

</section>
@endsection