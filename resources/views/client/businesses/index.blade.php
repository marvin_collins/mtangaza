@extends('client.layout')
@section('content')
<section class="content-header">
    <h1>
        Businesses
        <small>Manage the businesses that you have registered.</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('client.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="{{ route('client.businesses.index') }}">Businesses</a></li>
    </ol>
    <div class="row">
        <a href="{{ route('client.businesses.create') }}" class="btn btn-xs btn-danger pull-right" style="margin-right: 15px">Add Business</a>
    </div>
</section>
<section class="content">
    <div class="box box-solid box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">All Registered Businesses</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <table class="table table-responsive table-condensed table-hover data-table">
                <thead>
                <tr class="success">
                    <th>Business Name</th>
                    <th>Category</th>
                    <th>Contacts</th>
                    <th>Status</th>
                    <th>Created At</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($businesses as $business)
                    <tr>
                        <td><a href="{{ route('client.businesses.show', $business->id) }}">{{ $business->name }}</a></td>
                        <td>{{ $business->category->name }}</td>
                        <td>{{ $business->contact_numbers }}</td>
                        <td><span class="btn btn-xs {{ $business->active == 0 ? 'btn-danger' : 'btn-success' }}">{{ $business->active == 0 ? 'Unpublished' : 'Published' }}</span></td>
                        <td>{{ $business->created_at->format('d F, Y') }}</td>
                        <td><a class="btn btn-xs btn-info" href="{{ route('client.businesses.edit', $business->id) }}">Edit</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</section>
<hr>
<section class="content">
    <div class="box box-solid box-danger">
        <div class="box-header with-border">
            <h3 class="box-title">Currently Unpublished Businesses</h3>
            <div class="box-tools pull-right">
                {{--<a href="{{ route('client.businesses.create') }}" class="btn btn-xs btn-danger">Add Business</a>--}}
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <table class="table table-responsive table-condensed table-hover data-table">
                <thead>
                <tr class="success">
                    <th>Business Name</th>
                    <th>Category</th>
                    <th>Contacts</th>
                    <th>Status</th>
                    <th>Created At</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($businesses->where('active', 0) as $business)
                    <tr>
                        <td><a href="{{ route('client.businesses.show', $business->id) }}">{{ $business->name }}</a></td>
                        <td>{{ $business->category->name }}</td>
                        <td>{{ $business->contact_numbers }}</td>
                        <td><span class="btn btn-xs {{ $business->active == 0 ? 'btn-danger' : 'btn-success' }}">{{ $business->active == 0 ? 'Unpublished' : 'Published' }}</span></td>
                        <td>{{ $business->created_at->format('d F, Y') }}</td>
                        <td><a class="btn btn-xs btn-info" href="{{ route('client.businesses.edit', $business->id) }}">Edit</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</section>
<hr>
<section class="content">
    <div class="box box-solid box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Currently Published Businesses</h3>
            <div class="box-tools pull-right">
                {{--<a href="{{ route('client.businesses.create') }}" class="btn btn-xs btn-danger">Add Business</a>--}}
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <table class="table table-responsive table-condensed table-hover data-table">
                <thead>
                <tr class="success">
                    <th>Business Name</th>
                    <th>Category</th>
                    <th>Contacts</th>
                    <th>Status</th>
                    <th>Created At</th>
                    <th>Subscription End</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($businesses->where('active', 1)  as $business)
                    <tr>
                        <td><a href="{{ route('client.businesses.show', $business->id) }}">{{ $business->name }}</a></td>
                        <td>{{ $business->category->name }}</td>
                        <td>{{ $business->contact_numbers }}</td>
                        <td><span class="btn btn-xs {{ $business->active == 0 ? 'btn-danger' : 'btn-success' }}">{{ $business->active == 0 ? 'Unpublished' : 'Published' }}</span></td>
                        <td class="text-right">{{ $business->created_at->format('d F, Y') }}</td>
                        <td class="text-right">{{ is_null($business->subscription_end) ? '' : $business->subscription_end->format('d F, Y') }}</td>
                        <td><a class="btn btn-xs btn-info" href="{{ route('client.businesses.edit', $business->id) }}">Edit</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</section>
@endsection