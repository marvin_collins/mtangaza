@extends('client.layout')
@section('content')
    <section class="content-header">
        <h1>
            Businesses
            <small>Manage the businesses that you have registered.</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('client.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="{{ route('client.businesses.index') }}">Businesses</a></li>
            <li class="active">Add Business</li>
        </ol>
    </section>

    <section class="content">
        <div class="box box-solid box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Add new business</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <form action="{{ route('client.businesses.store') }}" role="form" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="col-md-6">
                            <h5>Business Details</h5>
                            <hr>
                            <div class="form-group">
                                <label for="name">Business Name*</label>
                                <input value="{{ old('name') }}" type="text" name="name" id="name" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="category_id">Business Category*</label>
                                <select required name="category_id" id="category_id" class="form-control select2">
                                    @foreach($categories as $category)
                                        <option {{ old('category_id') == $category->id ? 'selected' : '' }} value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="field_of_work">Field of Work*</label>
                                <div class="input-group">
                                    <select required name="field_of_work[]" id="field_of_work" class="form-control select2" multiple aria-describedby="fieldofwork">
                                        @foreach($fields as $field)
                                            <option {{ old('field_of_work') == $field->id ? 'selected' : '' }} value="{{ $field->id }}">{{ $field->name }}</option>
                                        @endforeach
                                    </select>
                                    <span data-toggle="modal" data-target="#field_work" class="input-group-addon" id="fieldofwork" style="background-color: #3c8dbc; color: white !important;">
                                        <i class="fa fa-plus"></i> add your field
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="last_name">Business Logo</label>
                                <input type="file" name="logo" id="logo" accept="image/*" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="description">Company Description* </label>
                                <textarea name="description" required id="description" class="form-control">{{ old('description') }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="email">Company Email*</label>
                                <input type="email" name="email" id="email" class="form-control" value="{{ old('email') }}" required>
                            </div>
                            <div class="form-group">
                                <label for="city_id">Map Location*</label>
                                <input type="hidden" required name="map_location" id="map_location" value="{{ old('map_location') }}">
                                <div id="map"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="contact_person">Contact Person*</label>
                                <input type="text" name="contact_person" id="contact_person" class="form-control" value="{{ old('contact_person') }}" required>
                            </div>
                            <div class="form-group">
                                <label for="contact_numbers">Contact Numbers*</label>
                                <input type="text" name="contact_numbers" id="contact_numbers" class="form-control" value="{{ old('contact_numbers') }}" required>
                            </div>
                            <div class="form-group">
                                <label for="city_id">City*</label>
                                <select required name="city_id[]" id="city_id" class="form-control select2" multiple>
                                    @foreach($cities as $city)
                                        <option {{ old('category_id') == $city->id ? 'selected' : '' }} value="{{ $city->id }}">{{ $city->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="website">Company Website</label>
                                <input type="text" name="website" id="website" class="form-control" value="{{ old('website') }}">
                            </div>
                            <div class="form-group">
                                <label for="twitter">Company Twitter Username</label>
                                <div class="input-group">
                                    <div class="input-group-addon">https://twitter.com/</div>
                                    <input type="text" name="twitter" id="twitter" class="form-control" value="{{ old('twitter') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="facebook">Company Facebook Page</label>
                                <div class="input-group">
                                    <div class="input-group-addon">https://facebook.com/</div>
                                    <input type="text" name="facebook" id="facebook" class="form-control" value="{{ old('facebook') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="google_plus">Company Linked In Page</label>
                                <div class="input-group">
                                    <div class="input-group-addon">https://linkedin.com/</div>
                                   <input type="text" name="google_plus" id="google_plus" class="form-control" value="{{ old('google_plus') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="linked_in">Company Google+ Page</label>
                                <div class="input-group">
                                    <div class="input-group-addon">https://plus.google.com/</div>
                                    <input type="text" name="linked_in" id="linked_in" class="form-control" value="{{ old('linked_in') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="skype">Skype ID</label>
                                <input type="text" name="skype" id="skype" class="form-control" value="{{ old('skype') }}">
                            </div>
                            <hr>
                            <h5>Subscription Details</h5>
                            <hr>
                            <div class="form-group">
                                <label for="plan_id">Select Plan*</label>
                                <select required name="plan_id" id="plan_id" class="form-control">
                                    @foreach($plans as $plan)
                                        <option value="{{ $plan->id }}" {{ old('plan_id') == $plan->id ? 'selected' : '' }}>{{ title_case(str_replace('_',' ', $plan->name)) }} - KES {{ number_format($plan->cost, 2) }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <hr>
                            <h5>Business Images</h5>
                            <hr>
                            <div class="form-group">
                                <label for="last_name">5 Business Images</label>
                                <input type="file" name="images[]" id="images" accept="image/*" multiple class="form-control">
                                <div id="show_image" class="well well-sm">
                                </div>
                                <p id="setbtn" onclick="reset()" class="btn pull-right btn-sm btn-warning">Reset image selection</p>
                                <h5><strong>NOTE:</strong> Only the first five images will be used.</h5>
                            </div>
                            <hr>
                            <h5>Payment Details</h5>
                            <hr>
                            <div class="form-group">
                                <label for="skype">Paybill Business Number</label>
                                <h4>{{ env('PAYBILL_ACCOUNT') }}</h4>
                            </div>
                            <div class="form-group">
                                <label for="skype">Account Number</label>
                                <h4>{{ $acc = \Carbon\Carbon::now()->timestamp . str_random(3) }}</h4>
                                <input type="hidden" name="account_number" id="account_number" class="form-control" value="{{ $acc }}">
                            </div>
                            <div class="form-group">
                                <label for="skype">Amount</label>
                                <h4 id="toPay">{{ number_format($plans->first()->cost, 2) }}</h4>
                            </div>
                            <div class="form-group">
                                <label for="transaction_number">Transaction Number(Mpesa Ref: number or cheque number)*</label>
                                <input required type="text" name="transaction_number" id="transaction_number" class="form-control" value="{{ old('skype') }}">
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary" value="Create Business">
                                <input type="reset" class="btn btn-danger" value="Clear Form">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="field_work" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <form action="#" id="interests" method="POST" enctype="multipart/form-data">
                <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="myModalLabel">Your Field Of Work</h4>
                </div>
                <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="interest">Field of work</label>
                                <textarea  autofocus required class="form-control" name="interest" id="interest" cols="30" rows="3">{{ old('interest_m') }}</textarea>
                            </div>
                        </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Add Interests</button>
                </div>
            </div>
            </form>
        </div>
    </div>
@endsection
@section('footer')
<script src="http://maps.googleapis.com/maps/api/js?key={{ env('MAP_API') }}"></script>
<script>
    $('#interests').on('submit',function (e) {
        e.preventDefault();
        var interestData = {};
        interestData['interest'] = $('#interest').val();
        interestData['_token'] = '{{csrf_token()}}';
        interestData['ajax'] = 'ajax';
        $.ajax(
                {
                    url: '{{ route('client.interests.store') }}',
                    type: 'POST',
                    data: interestData,
                    success:  function (response) {
                        var addedField = '<option selected value="' + response['id'] + '">'+
                                        response['name'] + '</option>';
                        $('#field_of_work').append(addedField);
                        $("#field_of_work").select2({theme: "bootstrap"});
                        $('#interest').val("");
                        $('#field_work').modal('toggle');
                    },
                    error: function (response) {
                        alert('Sorry for this, seem like your field of work is available. Select from the list');
                        $('#field_work').modal('toggle');
                    }
                }
        )
    });
    $('#show_image').hide();
    $('#setbtn').hide();
    $('#images').change(function () {
        $('#show_image').show();
        $('#setbtn').show();
        data =  '<div class="alert alert-info alert-dismissible" role="alert">'+
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
                $("#images")[0].files[0].name+'</div>';
        $('#show_image').append(data);
    });
    function reset() {
        $("#images").val("");
        $.each($('#show_image div'),function (indx,vall) {
            $(vall).remove();
        });
        $('#show_image').hide();
        $('#setbtn').hide();
    }
    var map_locations = $('#map_location');
    $("#description").wysihtml5();
    $('.wysihtml5-sandbox').contents().find('body').on("keyup",function() {
        var length = $(this)[0].textContent.length;
        $('#chars').html(length);
    });
    var plans = {!! $plans->each(function ($value, $key) use ($plans) { return $plans[$key] = $value->get(['id', 'cost']); })->first()->keyBy('id') !!};
    var selectedPlan = $('#plan_id');
    selectedPlan.on('change', updateCost);
    function updateCost() {
        var selected = plans[selectedPlan.val()];
        $('#toPay').html('KES ' + selected.cost.toLocaleString());
    }
    updateCost();

    function initialize() {
        var mapProp = {
            center:new google.maps.LatLng(-1.3048035, 36.8473969),
            zoom:12,
            mapTypeId:google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map(document.getElementById("map"), mapProp);

        google.maps.event.addListener(map, 'click', function(e) {
            placeMarker(e.latLng, map);
        });

    }

    function placeMarker(position, map)
    {
        var marker = new google.maps.Marker({
            position: position,
            map: map
        });

        google.maps.event.addListener(marker,'click',function() {
            removeMarker(this);
        });
        map.panTo(position);
        var infowindow = new google.maps.InfoWindow({
            content: 'Latitude: ' + position.lat() + '<br>Longitude: ' + position.lng()
        });
//        infowindow.open(map, marker);

        var locations = [];
        if (map_locations.val().length > 2) {
            locations = [map_locations.val()];
        }
        locations.push(position.lat() + ',' + position.lng());
        map_locations.val(locations.join(';'));
    }

    function removeMarker(marker) {
        var locations = map_locations.val().split(';');
        var pin = marker.position.lat() + ',' + marker.position.lng();
        var index = locations.indexOf(pin);
        locations.splice(index, 1);
        map_locations.val(locations.join(';'));
        marker.setMap(null);
    }

    google.maps.event.addDomListener(window, 'load', initialize);

</script>
@endsection