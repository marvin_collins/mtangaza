@extends('client.layout')
@section('content')
    <section class="content-header">
        <h1>
            Businesses
            <small>Manage the businesses that are registered in the system.</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('client.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="{{ route('client.businesses.index') }}">Businesses</a></li>
            <li class="active">View Business</li>
        </ol>
    </section>

    <section class="content">
        <div class="box box-solid box-default">
            <div class="box-header with-border">
                <h3 class="box-title">View business</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <h5>Business Details</h5>
                        <hr>
                        <div class="form-group">
                            <label for="name">Business Name*</label>
                            <input value="{{ $business->name }}" type="text" name="name" id="name" class="form-control" disabled>
                        </div>
                        <div class="form-group">
                            <label for="category_id">Business Categories*</label>
                            <input type="text" value="{{ $business->category->name }}" name="category_id" id="category_id" class="form-control" disabled>
                        </div>
                        <div class="form-group">
                            <label for="field_of_work">Field of Work*</label>
                            <select name="field_of_work[]" id="field_of_work" class="form-control select2" multiple disabled>
                            @foreach($business->fieldsOfWork as $field)
                                <option selected>{{ $field->name }}</option>
                            @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="last_name">Business Logo</label>
                            <img src="{{ asset($business->logo) }}" alt="logo" class="img-responsive">
                        </div>
                        <div class="form-group">
                            <label for="description">Company Description (min: 100 words)</label>
                            <textarea name="description" id="description" class="form-control" disabled>{!! $business->description !!}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="email">Company Email</label>
                            <input type="email" name="email" id="email" class="form-control" value="{{ $business->email }}" required disabled>
                        </div>
                        <div class="form-group">
                            <label for="city_id">Map Location*</label>
                            <input type="hidden" name="map_location" id="map_location" value="{{ $business->map_location }}">
                            <div id="map"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="contact_person">Contact Person</label>
                            <input type="text" name="contact_person" id="contact_person" class="form-control" value="{{ $business->contact_person }}" required disabled>
                        </div>
                        <div class="form-group">
                            <label for="contact_numbers">Contact Numbers</label>
                            <input type="text" name="contact_numbers" id="contact_numbers" class="form-control" value="{{ $business->contact_numbers }}" required disabled>
                        </div>
                        <div class="form-group">
                            <label for="city_id">City*</label>
                            <select name="city_id[]" id="city_id" class="form-control select2" multiple disabled>
                            @foreach($business->cities as $city)
                                <option selected>{{ $city->name }}</option>
                            @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="website">Company Website</label>
                            <input type="url" name="website" id="website" class="form-control" value="{{ $business->website }}" disabled>
                        </div>
                        <div class="form-group">
                            <label for="twitter">Company Twitter Username</label>
                            <div class="input-group">
                                <div class="input-group-addon">https://twitter.com/</div>
                                <input type="text" name="twitter" id="twitter" class="form-control" value="{{ $business->twitter }}" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="facebook">Company Facebook Page</label>
                            <div class="input-group">
                                <div class="input-group-addon">https://facebook.com/</div>
                                <input type="text" name="facebook" id="facebook" class="form-control" value="{{ $business->facebook }}" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="google_plus">Company Linked In Page</label>
                            <div class="input-group">
                                <div class="input-group-addon">https://linkedin.com/</div>
                                <input type="text" name="google_plus" id="google_plus" class="form-control" value="{{ $business->google_plus }}" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="linked_in">Company Google+ Page</label>
                            <div class="input-group">
                                <div class="input-group-addon">https://plus.google.com/</div>
                                <input type="text" name="linked_in" id="linked_in" class="form-control" value="{{ $business->linked_in }}" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="skype">Skype ID</label>
                            <input type="text" name="skype" id="skype" class="form-control" value="{{ $business->skype }}" disabled>
                        </div>
                        <hr>
                        <h5>Subscription Details</h5>
                        @if($business->subscriptions->last())
                            <div class="form-group">
                                <label for="client_id">Plan</label>
                                <input type="text" name="client_id" id="client_id" class="form-control" value="{{ str_replace('_',' ', title_case($business->subscriptions->last()->plan->name)) }}" disabled>
                            </div>
                        @endif
                        <div class="form-group">
                            <label for="client_id">Client*</label>
                            <input type="text" name="client_id" id="client_id" class="form-control" value="{{ $business->client->identification_number . ' - ' . $business->client->title . ' ' . $business->client->first_name . ' ' . $business->client->last_name }}" disabled>
                        </div>

                        <div class="form-group">
                            <a href="{{ route('client.businesses.edit', $business->id) }}" class="btn btn-primary">Edit Business</a>
                            <a href="{{ URL::previous() }}" class="btn btn-danger">Back</a>
                        </div>


                    </div>

                </div>
                <hr>
                <h5>Business Images</h5>
                <hr>
                <div class="form-group row prodImages">
                    @foreach($business->images as $image)
                        <?php
                        $imageLocation = $image->image;
                        $parts = explode('/', $imageLocation);
                        $imageIndex = count($parts) - 1;
                        $imageThumb = 'thumb-' . $parts[$imageIndex];
                        $parts[$imageIndex] = $imageThumb;
                        $imagePath = implode('/', $parts);
                        ?>
                        <div class="col-sm-2">
                            <img src="{{ asset($imagePath) }}" alt="{{ $business->name }}" class="img-responsive">
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>


    <section>
        <div id="imagesView" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Business Images</h4>
                    </div>
                    <div class="modal-body">
                        <div id="business_images" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner" role="listbox">
                                @foreach($business->images as $image)
                                    <div class="item{{ $image->id == $business->images->first()->id ? ' active' : '' }}">
                                        <img src="{{ asset($image->image) }}" alt="{{ $business->name }}" class="img-responsive">
                                    </div>
                                @endforeach
                            </div>
                            <a class="left carousel-control" href="#business_images" role="button" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#business_images" role="button" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('footer')
    <script src="http://maps.googleapis.com/maps/api/js?key={{ env('MAP_API') }}"></script>
    <script>
        $("#description").wysihtml5();
        var map_locations = $('#map_location');

        function initialize() {
            var mapProp = {
                center: new google.maps.LatLng(-1.3048035, 36.8473969),
                zoom:12,
                mapTypeId:google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map(document.getElementById("map"), mapProp);
            setupMarkers(map);
        }

        function setupMarkers(map) {
            if (map_locations.val().length < 10) {
                return;
            }
            var locations = map_locations.val().split(';');
            var latLong = [];
            var position = null;
            for(var i = 0; i < locations.length; i++) {
                latLong = locations[i].split(',');
                position = new google.maps.LatLng(latLong[0], latLong[1]);
                var marker = new google.maps.Marker({
                    position: position,
                    map: map
                });
            }
            map.panTo(position);
        }

        function placeMarker(position, map)
        {
            var marker = new google.maps.Marker({
                position: position,
                map: map
            });

            google.maps.event.addListener(marker,'click',function() {
                removeMarker(this);
            });
            map.panTo(position);

            var locations = [];
            if (map_locations.val().length > 2) {
                locations = [map_locations.val()];
            }
            locations.push(position.lat() + ',' + position.lng());
            map_locations.val(locations.join(';'));
        }

        google.maps.event.addDomListener(window, 'load', initialize);

        function loadImages()
        {
            $('.carousel').carousel({
                interval: 1000
            });
            $('#imagesView').modal('show');
        }

        $('.prodImages img').on('click', loadImages);

    </script>
@endsection