@extends('client.layout')
@section('content')
    <section class="content-header">
        <h1>
            Interests
            <small>Manage Your Interests.</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('client.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="{{ route('client.interests.index') }}">Interests</a></li>
            <li class="active">Add Interest</li>
        </ol>
    </section>

    <section class="content">
        <div class="box box-solid box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Add Interest</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <form action="{{ route('client.interests.store') }}" role="form" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="field_of_work_id">Select Interests*</label>
                                <select name="field_of_work_id[]" id="field_of_work_id" class="form-control select2" multiple>
                                    @foreach($fields as $field)
                                        <option value="{{ $field->id }}">{{ $field->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <hr>
                            <h5>Other Interests</h5>
                            <div class="form-group">
                                <label for="interest">Other Interest Names</label>
                                <textarea class="form-control   " name="interest" id="interest" cols="30" rows="10">{{ old('interest') }}</textarea>
                            </div>
                            <h4><strong>NOTE:</strong> Separate the interests using a semi-colon (;)</h4>
                            <h4><strong>E.g.:</strong> Training;Manufacturing</h4>
                            <hr>
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary" value="Add Interests">
                                <input type="reset" class="btn btn-danger" value="Clear Form">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
