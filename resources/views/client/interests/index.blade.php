@extends('client.layout')
@section('content')
<section class="content-header">
    <h1>
        Interests
        <small>Manage Your Interests.</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('client.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="{{ route('client.interests.index') }}">Interests</a></li>
    </ol>
</section>
<section class="content">
    <div class="box box-solid box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Your Interests</h3>
            <div class="box-tools pull-right">
                <a href="{{ route('client.interests.create') }}" class="btn btn-xs btn-danger">Add Interest</a>
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <table class="table table-responsive table-condensed table-hover data-table">
                <thead>
                <tr class="success">
                    <th>Interest</th>
                    <th>Created At</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($interests as $interest)
                    <tr>
                        <td>{{ $interest->field->name }}</td>
                        <td>{{ $interest->created_at->format('d F Y') }}</td>
                        <td><a class="btn btn-xs btn-danger" href="{{ route('client.interests.destroy', $interest->id) }}" data-method="delete" rel="nofollow" data-confirm="Are you sure you want to delete this?" data-token="{{ csrf_token() }}">Delete</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</section>
@endsection