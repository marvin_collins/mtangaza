@extends('client.layout')
@section('content')
    <section class="content-header">
        <h1>
            Notifications
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('client.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="{{ route('client.businesses.index') }}">Notifications</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-solid box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">All Notifications</h3>
            </div>
            <div class="box-body">
                <table class="table table-responsive table-condensed table-hover data-table">
                    <thead>
                    <tr class="success">
                        <th>Notification</th>
                        {{--<th>Category</th>--}}
                        {{--<th>Contacts</th>--}}
                        {{--<th>Status</th>--}}
                        <th class="text-center">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($notifications as $notification)
                        <tr>
                            <td><strong>{{$notification['follower']}}</strong> send you a request to connect with your business <strong>
                                {{$notification['business']}}</strong> {{\Carbon\Carbon::parse($notification['date'])->diffForHumans()}} </td>
                            {{--<td>NotificationController</td>--}}
                            {{--<td>NotificationController</td>--}}
                            {{--<td>NotificationController</td>--}}
                            {{--<td>NotificationController</td>--}}
                            {{--<td>NotificationController{{ $business->created_at->format('d F, Y') }}</td>--}}
                            {{--<td><a class="btn btn-xs btn-info" href="{{ route('client.businesses.edit', $business->id) }}">Edit</a></td>--}}
                            <td class="text-center"><a href="{{url('client/notifications',['id'=>$notification['id']])}}" class="btn btn-xs btn-success">Confirm</a> <a href="{{url('client/notifications/decline',['id'=>$notification['id']])}}" class="btn btn-xs btn-danger">Decline</a> </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
@endsection