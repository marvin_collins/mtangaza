@extends('client.layout')
@section('content')
    <section class="content-header">
        <h1>
            Clients
            <small>Manage the clients that are registered in the system.</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('client.users.index') }}">Clients</a></li>
            <li class="active">Add Client</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-solid box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Add New Authorised User</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <form action="{{ route('client.users.store') }}" role="form" method="post">
                        {{ csrf_field() }}
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="title">Title*</label>
                                <select name="title" id="title" class="form-control">
                                    <option value="Mr." selected="selected">Mr.</option>
                                    <option value="Ms.">Ms.</option>
                                    <option value="Mrs.">Mrs.</option>
                                    <option value="Miss.">Miss.</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="first_name">First Name*</label>
                                <input value="{{ old('first_name') }}" type="text" name="first_name" id="first_name" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="middle_name">Middle Name*</label>
                                <input value="{{ old('middle_name') }}" type="text" name="middle_name" id="middle_name" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="last_name">Last Name*</label>
                                <input value="{{ old('last_name') }}"  type="text" name="last_name" id="last_name" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="nationality">Nationality*</label>
                                {{--//form not working on multiple select check tomorrow--}}
                                <select style="width:100%" name="nationality" id="nationality" class="form-control
                            select2" required>
                                    @foreach($nationalities as $nationality)
                                        {{--dd($city)--}}
                                        <option>{{$nationality}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="gender">Gender*</label>
                                <select required name="gender" id="gender" class="form-control">
                                    <option value="M" selected="selected">Male</option>
                                    <option value="F">Female</option>
                                    <option value="U">Undisclosed</option>
                                </select>
                            </div><div class="form-group">
                                <label for="city">City*</label>
                                <input required value="{{ old('city') }}" name="city" id="city" type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="identification_type">Identification Type*</label>
                                <select required name="identification_type" id="identification_type" class="form-control">
                                    <option value="National ID" selected="selected">National ID</option>
                                    <option value="Passport">Passport</option>
                                </select>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="identification_number">Identification Number*</label>
                                <input value="{{ old('identification_number') }}" name="identification_number"
                                       id="identification_number" type="text" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="email">Email*</label>
                                <input value="{{ old('email') }}" name="email" id="email" type="email" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="mobile">Mobile*</label>
                                <input value="{{ old('mobile') }}" name="mobile" id="mobile" type="text" class="form-control" pattern="[0-9]+$" placeholder="254722XXXXXX" required>
                            </div>
                            <div class="form-group">
                                <label for="address">Country</label>
                                <input value="Kenya" name="country" id="address" type="text" class="form-control" readonly>
                            </div>
                            <div class="form-group">
                                <label for="city">City*</label>
                                {{--//form not working on multiple select check tomorrow--}}
                                <select style="width:100%" name="city" id="city" class="form-control
                            select2" required>
                                    @foreach(\SmoDav\Models\City::all() as $city)
                                        <option>{{$city->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="address1">Address one*</label>
                                <input required value="{{ old('address1') }}" name="address1" id="address1" type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="address2">Address two</label>
                                <input value="{{ old('address2') }}" name="address2" id="address2" type="text" class="form-control">
                            </div>
                            {{--<hr>--}}
                            {{--<h5>PERMISSIONS</h5>--}}
                            {{--<hr>--}}
                            {{--<div class="form-group row">--}}
                                {{--<div class="col-sm-4">Permission</div>--}}
                                {{--<div class="col-sm-2">Access</div>--}}
                                {{--<div class="col-sm-2">View</div>--}}
                                {{--<div class="col-sm-2">Create</div>--}}
                                {{--<div class="col-sm-2">Update</div>--}}
                            {{--</div>--}}
                            {{--<div class="form-group row">--}}
                                {{--<div class="col-sm-4">Authorised Users</div>--}}
                                {{--<div class="col-sm-2"><input type="checkbox" name=""></div>--}}
                                {{--<div class="col-sm-2">View</div>--}}
                                {{--<div class="col-sm-2">Create</div>--}}
                                {{--<div class="col-sm-2">Update</div>--}}
                            {{--</div>--}}
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary" value="Create Account">
                                <input type="reset" class="btn btn-danger" value="Clear Form">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection