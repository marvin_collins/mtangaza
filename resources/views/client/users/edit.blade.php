@extends('client.layout')
@section('content')
    <section class="content-header">
        <h1>
            Clients
            <small>Manage the clients that are registered in the system.</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('manage.clients.index') }}">Clients</a></li>
            <li class="active">Edit Client Details</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-solid box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Edit client details</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <form action="{{ route('client.users.update', $client->id) }}" role="form" method="post">
                        {{ csrf_field() }}
                        {{ method_field('put') }}
                        <input type="hidden" name="uid" value="{{ $client->user->id }}">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="title">Title*</label>
                                <select name="title" id="title" class="form-control">
                                    <option value="Mr."{{ $client->title == "Mr." ? ' selected' : '' }}>Mr.</option>
                                    <option value="Ms."{{ $client->title == "Ms." ? ' selected' : '' }}>Ms.</option>
                                    <option value="Mrs."{{ $client->title == "Mrs." ? ' selected' : '' }}>Mrs.</option>
                                    <option value="Miss."{{ $client->title == "Miss." ? ' selected' : '' }}>Miss.</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="first_name">First Name*</label>
                                <input value="{{ $client->first_name  }}" type="text" name="first_name" id="first_name" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="last_name">Last Name*</label>
                                <input type="text" value="{{ $client->last_name }}" name="last_name" id="last_name" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="nationality">Nationality*</label>
                                <input value="{{ $client->nationality  }}" name="nationality" id="nationality" type="text" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="gender">Gender*</label>
                                <select name="gender" id="gender" class="form-control">
                                    <option value="M"{{ $client->gender == "M" ? ' selected' : '' }}>Male</option>
                                    <option value="F"{{ $client->gender == "F" ? ' selected' : '' }}>Female</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="identification_type">Identification Type*</label>
                                <select name="identification_type" id="identification_type" class="form-control">
                                    <option value="National ID"{{ $client->identification_type == "National ID" ? ' selected' : '' }}>National ID</option>
                                    <option value="Passport"{{ $client->identification_type == "Passport" ? ' selected' : '' }}>Passport</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="identification_number">Identification Number*</label>
                                <input value="{{ $client->identification_number }}" name="identification_number" id="identification_number" type="text" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="email">Email*</label>
                                <input value="{{ $client->email }}" name="email" id="email" type="email" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="mobile">Mobile*</label>
                                <input value="{{ $client->mobile }}" name="mobile" id="mobile" type="text" class="form-control" pattern="[0-9]+$" placeholder="254722XXXXXX" required>
                            </div>
                            <div class="form-group">
                                <label for="address">Address</label>
                                <input value="{{ $client->address }}" name="address" id="address" type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary" value="Edit Account">
                                <input type="reset" class="btn btn-warning" value="Clear Form">
                                <a href="{{ URL::previous() }}" class="btn btn-danger">Back</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection