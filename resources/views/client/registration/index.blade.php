<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>mTangaza | Client</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{ asset('css/admin.css') }}">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="col-lg-9" style="margin: 0 auto; float: none">
    <div class="login-logo">
        <a href="{{url('/')}}" >
            <img src="{{ asset('images/top-logo.png') }}" alt="mTangaza"></a>
    </div>
    <!-- /.login-logo -->

    <div class="box box-solid box-default">
        <div class="box-header with-border">
            <h3 class="text-center" >Register with us</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <form action="{{ url('newuser/register') }}" role="form" method="post">
                    {{ csrf_field() }}
                    <div class="col-md-6">
                        <div class="form-group">
                            @if ($errors->has('title'))
                                <span class="help-block">
                                        <strong style="color: red">The Title field is required.</strong>
                                    </span>
                            @endif
                            <label for="title">Title*</label>
                            <select name="title" id="title" class="form-control">
                                @if(old('title'))
                                    <option value="{{old('title')}}">
                                        {{old('title')}}
                                    </option>
                                @endif
                                <option value="Mr." >Mr.</option>
                                <option value="Ms.">Ms.</option>
                                <option value="Mrs.">Mrs.</option>
                                <option value="Miss.">Miss.</option>
                            </select>
                        </div>
                        <div class="form-group">
                            @if ($errors->has('first_name'))
                                <span class="help-block">
                                        <strong style="color: red">The First Name field is required.</strong>
                                    </span>
                            @endif
                            <label for="first_name">First Name*</label>
                            <input value="{{ old('first_name') }}" type="text" name="first_name" id="first_name" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="middle_name">Middle Name</label>
                            <input value="{{ old('middle_name') }}" type="text" name="middle_name" id="middle_name" class="form-control" >
                        </div>
                        <div class="form-group">
                            @if ($errors->has('last_name'))
                                <span class="help-block">
                                        <strong style="color: red">The Last Name field is required.</strong>
                                    </span>
                            @endif
                            <label for="last_name">Last Name*</label>
                            <input type="text" value="{{old('last_name')}}" name="last_name" id="last_name" class="form-control" required>
                        </div>
                        <div class="form-group">
                            @if ($errors->has('nationality'))
                                <span class="help-block">
                                        <strong style="color: red">The Nationality field is required.</strong>
                                    </span>
                            @endif
                            <label for="nationality">Nationality*</label>
                            <select  style="width:100%" name="nationality" id="nationality" class="form-control
                            select2" required>
                                @if(old('nationality'))
                                    <option value="{{old('nationality')}}">
                                        {{old('nationality')}}
                                    </option>
                                @endif
                                @foreach($nationalities as $nationality)
                                    <option value="{{$nationality}}">{{$nationality}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            @if ($errors->has('gender'))
                                <span class="help-block">
                                        <strong style="color: red">The Gender field is required.</strong>
                                    </span>
                            @endif
                            <label for="gender">Gender*</label>
                            <select name="gender" id="gender" class="form-control">
                                @if(old('gender'))
                                <option value="{{old("gender")}}" selected> {{old("gender") == "M" ? "Male" : " "}}{{old("gender") == "F" ? "Female" : " "}}{{old("gender") == "U" ? "Undisclosed" : " "}}</option>
                                @endif
                                <option value="M"> Male</option>
                                <option value="F">Female</option>
                                <option value="U">Undisclosed</option>
                            </select>
                        </div>
                        <div class="form-group">
                            @if ($errors->has('mobile'))
                                <span class="help-block">
                                        <strong style="color: red">The Mobile field is required.</strong>
                                    </span>
                            @endif
                            <label for="mobile">Mobile*</label>
                            <input value="{{ old('mobile') }}" name="mobile" id="mobile" type="text" class="form-control" pattern="[0-9]+$" placeholder="254722XXXXXX" required>
                        </div>


                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            @if ($errors->has('identification_type'))
                                <span class="help-block">
                                        <strong style="color: red">The Identification Type field is required.</strong>
                                    </span>
                            @endif
                            <label for="identification_type">Identification Type*</label>
                            <select selected="{{old("identification_type")}}" name="identification_type" id="identification_type" class="form-control">
                                @if(old('identification_type'))
                                    <option value="{{old('identification_type')}}">
                                        {{old('identification_type')}}
                                    </option>
                                @endif
                                <option value="National ID" >National ID</option>
                                <option value="Passport">Passport</option>
                            </select>
                        </div>

                        <div class="form-group">
                            @if ($errors->has('identification_number'))
                                <span class="help-block">
                                        <strong style="color: red">The Identification Number field is required.</strong>
                                    </span>
                            @endif
                            <label for="identification_number">Identification Number*</label>
                            <input value="{{ old('identification_number') }}" name="identification_number" id="identification_number" type="text" class="form-control" required>
                        </div>
                        <div class="form-group">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                <strong style="color: red">
                                        @foreach($errors->all() as $key => $error)
                                        @if($key == 'email')
                                            {{$error}}
                                    @endif
                                    @endforeach
                                </strong>
                                    </strong>
                                    </span>
                            @endif
                            <label for="email">Email*</label>
                            <input value="{{ old('email') }}" name="email" id="email" type="email" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="address">Country</label>
                            <input value="Kenya" name="country" id="country" type="text" class="form-control" readonly>
                        </div>
                        {{--<div class="form-group">--}}
                            {{--<label for="city">City*</label>--}}
                            {{--<input value="{{ old('city') }}" name="city" id="city" type="text" class="form-control">--}}
                        {{--</div>--}}
                        <div class="form-group">
                            @if ($errors->has('city'))
                                <span class="help-block">
                                        <strong style="color: red">The City field is required.</strong>
                                    </span>
                            @endif
                            <label for="city">City*</label>
                            {{--//form not working on multiple select check tomorrow--}}
                            <select style="width:100%" name="city" id="city" class="form-control
                            select2" required>
                                @if(old('city'))
                                    <option value="{{old('city')}}">
                                        {{old('city')}}
                                    </option>
                                @endif
                                @foreach(\SmoDav\Models\City::all() as $city)
                                    <option value="{{$city->name}}">{{$city->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group"} >
                            @if ($errors->has('address1'))
                                <span class="help-block">
                                        <strong style="color: red">The Address One field is required.</strong>
                                    </span>
                            @endif
                            <label for="address">Address One*</label>
                            <input  value="{{ old('address1') }}" name="address1" id="address1" type="text" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="address">Address Two</label>
                            <input value="{{ old('address2') }}" name="address2" id="address2" type="text" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>
                                <input name="newsletter" type="checkbox" id="newsletter" class="icheck">
                                I accept receiving promotional emails and newsletter from the mTangaza and its affiliates
                            </label>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" value="Create Account">
                            <input type="reset" class="btn btn-danger" value="Clear Form">

                        </div>
                    </div>
                </form>
                <div style="margin-left: 18px">
                <a href="{{url()->previous()}}"><button class="btn btn-danger">Back</button></a>
                    </div>
            </div>
        </div>
    </div>
    <!-- /.login-box-body -->
</div>

<script src="{{ asset('js/admin.js') }}"></script>


@if(session()->has('flash_message'))
    <script>

        $(function() {
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "positionClass": "toast-top-right",
                "onclick": null,
                "showDuration": "1000",
                "hideDuration": "1000",
                "timeOut": "9000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
            toastr['{{ session('flash_message_status') }}']("{!! session('flash_message') !!}");
        });
    </script>
@endif
@if(count($errors) > 0)
    <?php
    $displayErrors = "";
    foreach($errors->all() as $error)
    {
        $displayErrors .= $error . "<br>";
    }
    ?>

    <script>
        $(function() {
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "positionClass": "toast-top-right",
                "onclick": null,
                "showDuration": "1000",
                "hideDuration": "1000",
                "timeOut": "9000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
            toastr['error']("{!! 'Please correct the error' !!}");
        });
    </script>
@endif
<script> $('#title').select2(
            {
                tags: true,
                tokenSeparators: [',', ' ']
            }
    );</script>
</body>
</html>
