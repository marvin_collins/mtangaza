@extends('client.layout')
@section('content')
    <section class="content-header">
        <h1>
            Payments
            <small>Manage your payments.</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('client.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="{{ route('client.payments.index') }}">Payments</a></li>
            <li class="active">Make Payment</li>
        </ol>
    </section>

    <section class="content">
        <div class="box box-solid box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Make Payment</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <form action="{{ route('client.payments.store') }}" role="form" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="col-md-6">
                            <h5>Business Details</h5>
                            <hr>
                            <div class="form-group">
                                <label for="business_id">Payment For*</label>
                                <select name="business_id" id="business_id" class="form-control select2">
                                    @foreach($businesses as $business)
                                        <option {{ old('business_id') == $business->id ? 'selected' : '' }} value="{{ $business->id }}">{{ $business->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <h5>Subscription Details</h5>
                            <hr>
                            <div class="form-group">
                                <label for="upgradetype">Select Upgrade Option*</label>
                                <select name="upgradetype" id="upgradetype" class="form-control">
                                    <option value="end_of_sub">Upgrade After Current Subscription</option>
                                    <option value="immediate">Upgrade Immediately</option>
                                </select>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label for="plan_id">Select Plan*</label>
                                <select name="plan_id" id="plan_id" class="form-control">
                                    @foreach($plans as $plan)
                                        <option value="{{ $plan->id }}" {{ old('plan_id') == $plan->id ? 'selected' : '' }}>{{ title_case(str_replace('_',' ', $plan->name)) }} - KES {{ number_format($plan->cost, 2) }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <hr>
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary" value="Make Payment">
                                <input type="reset" class="btn btn-danger" value="Clear Form">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <h5>Payment Details</h5>
                            <hr>
                            <div class="form-group">
                                <label for="skype">Paybill Business Number</label>
                                <h4>{{ env('PAYBILL_ACCOUNT') }}</h4>
                            </div>
                            <div class="form-group">
                                <label for="skype">Account Number</label>
                                <h4>{{ $acc = \Carbon\Carbon::now()->timestamp . str_random(3) }}</h4>
                                <input type="hidden" name="account_number" id="account_number" class="form-control" value="{{ $acc }}">
                            </div>
                            <div class="form-group">
                                <label for="skype">Amount</label>
                                <h4 id="toPay">{{ number_format($plans->first()->cost, 2) }}</h4>
                            </div>
                            <div class="form-group">
                                <label for="transaction_number">Transaction Number(Mpesa Ref: number or cheque number)*</label>
                                <input type="text" required name="transaction_number" id="transaction_number" class="form-control" value="{{ old('skype') }}">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('footer')
<script src="http://maps.googleapis.com/maps/api/js?key={{ env('MAP_API') }}"></script>
<script>
    var plans = {!! $plans->each(function ($value, $key) use ($plans) { return $plans[$key] = $value->get(['id', 'cost']); })->first()->keyBy('id') !!};
    var selectedPlan = $('#plan_id');
    selectedPlan.on('change', updateCost);
    function updateCost() {
        var selected = plans[selectedPlan.val()];
        $('#toPay').html('KES ' + selected.cost.toLocaleString());
    }
    updateCost();
</script>
@endsection