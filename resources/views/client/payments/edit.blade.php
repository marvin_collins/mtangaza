@extends('client.layout')
@section('content')
    <section class="content-header">
        <h1>
            Businesses
            <small>Manage the businesses that are registered in the system.</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('client.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="{{ route('client.businesses.index') }}">Businesses</a></li>
            <li class="active">Edit Business</li>
        </ol>
    </section>

    <section class="content">
        <div class="box box-solid box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Edit business</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <form action="{{ route('client.businesses.update', $business->id) }}" role="form" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('put') }}
                        <div class="col-md-6">
                            <h5>Business Details</h5>
                            <hr>
                            <div class="form-group">
                                <label for="name">Business Name*</label>
                                <input value="{{ $business->name }}" type="text" name="name" id="name" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="category_id">Business Category*</label>
                                <select name="category_id" id="category_id" class="form-control select2">
                                    @foreach($categories as $category)
                                        <option {{ $business->category_id == $category->id ? 'selected' : '' }} value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="field_of_work">Field of Work*</label>
                                <select name="field_of_work[]" id="field_of_work" class="form-control select2" multiple>
                                    @foreach($fields as $field)
                                        <option {{ in_array($field->id, $business->fieldsOfWork) ? 'selected' : '' }} value="{{ $field->id }}">{{ $field->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="last_name">Business Logo</label>
                                <img src="{{ asset($business->logo) }}" alt="logo" class="img-responsive">
                                <input type="file" name="logo" id="logo" accept="image/*" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="description">Company Description (min: 100 words)</label>
                                <textarea name="description" id="description" class="form-control">{{ $business->description }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="email">Company Email</label>
                                <input type="email" name="email" id="email" class="form-control" value="{{ $business->email }}" required>
                            </div>
                            <div class="form-group">
                                <label for="city_id">Map Location*</label>
                                <input type="hidden" name="map_location" id="map_location" value="{{ $business->map_location }}">
                                <div id="map"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="contact_numbers">Company Contacts</label>
                                <input type="text" name="contact_numbers" id="contact_numbers" class="form-control" value="{{ $business->contact_numbers }}" required>
                            </div>
                            <div class="form-group">
                                <label for="city_id">City*</label>
                                <select name="city_id[]" id="city_id" class="form-control select2" multiple>
                                    @foreach($cities as $city)
                                        <option {{ in_array($city->id, $business->cities) ? 'selected' : '' }} value="{{ $city->id }}">{{ $city->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="website">Company Website</label>
                                <input type="url" name="website" id="website" class="form-control" value="{{ $business->website }}">
                            </div>
                            <div class="form-group">
                                <label for="twitter">Company Twitter Username</label>
                                <div class="input-group">
                                    <div class="input-group-addon">https://twitter.com/</div>
                                    <input type="text" name="twitter" id="twitter" class="form-control" value="{{ $business->twitter }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="facebook">Company Facebook Page</label>
                                <div class="input-group">
                                    <div class="input-group-addon">https://facebook.com/</div>
                                    <input type="text" name="facebook" id="facebook" class="form-control" value="{{ $business->facebook }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="google_plus">Company Linked In Page</label>
                                <div class="input-group">
                                    <div class="input-group-addon">https://linkedin.com/</div>
                                    <input type="text" name="google_plus" id="google_plus" class="form-control" value="{{ $business->google_plus }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="linked_in">Company Google+ Page</label>
                                <div class="input-group">
                                    <div class="input-group-addon">https://plus.google.com/</div>
                                    <input type="text" name="linked_in" id="linked_in" class="form-control" value="{{ $business->linked_in }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="skype">Skype ID</label>
                                <input type="text" name="skype" id="skype" class="form-control" value="{{ $business->skype }}">
                            </div>
                            <hr>
                            <h5>Subscription Details</h5>
                            <hr>
                            <div class="form-group">
                                <label for="client_id">Client*</label>
                                <select name="client_id" id="client_id" class="form-control select2">
                                    @foreach($clients as $client)
                                        <option value="{{ $client->id }}" {{ $business->client_id == $client->id ? 'selected' : '' }}>{{ $client->identification_number . ' - ' . $client->title . ' ' . $client->first_name . ' ' . $client->last_name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <input type="submit" class="btn btn-primary" value="Save Changes">
                                <a href="{{ URL::previous() }}" class="btn btn-danger">Back</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('footer')
    <script src="http://maps.googleapis.com/maps/api/js?key={{ env('MAP_API') }}"></script>
    <script>
        $("#description").wysihtml5();
        var map_locations = $('#map_location');

        function initialize() {
            var mapProp = {
                center: new google.maps.LatLng(-1.3048035, 36.8473969),
                zoom:12,
                mapTypeId:google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map(document.getElementById("map"), mapProp);

            google.maps.event.addListener(map, 'click', function(e) {
                placeMarker(e.latLng, map);
            });

            setupMarkers(map);
        }

        function setupMarkers(map) {
            if (map_locations.val().length < 10) {
                return;
            }
            var locations = map_locations.val().split(';');
            var latLong = [];
            var position = null;
            for(var i = 0; i < locations.length; i++) {
                latLong = locations[i].split(',');
                position = new google.maps.LatLng(latLong[0], latLong[1]);
                var marker = new google.maps.Marker({
                    position: position,
                    map: map
                });
                google.maps.event.addListener(marker,'click',function() {
                    removeMarker(this);
                });
            }
            map.panTo(position);
        }

        function placeMarker(position, map)
        {
            var marker = new google.maps.Marker({
                position: position,
                map: map
            });

            google.maps.event.addListener(marker,'click',function() {
                removeMarker(this);
            });
            map.panTo(position);

            var locations = [];
            if (map_locations.val().length > 2) {
                locations = [map_locations.val()];
            }
            locations.push(position.lat() + ',' + position.lng());
            map_locations.val(locations.join(';'));
        }

        function removeMarker(marker) {
            var locations = map_locations.val().split(';');
            var pin = marker.position.lat() + ',' + marker.position.lng();
            var index = locations.indexOf(pin);
            locations.splice(index, 1);
            map_locations.val(locations.join(';'));
            marker.setMap(null);
        }

        google.maps.event.addDomListener(window, 'load', initialize);

    </script>
@endsection