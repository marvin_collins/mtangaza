@extends('client.layout')
@section('content')
<section class="content-header">
    <h1>
        Payments
        <small>Manage your payments.</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('client.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="{{ route('client.payments.index') }}">Payments</a></li>
    </ol>
</section>
<section class="content">
    <div class="box box-solid box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">All Payments</h3>
            <div class="box-tools pull-right">
                <a href="{{ route('client.payments.create') }}" class="btn btn-xs btn-danger">Make Payment</a>
            </div>
        </div>
        <div class="box-body">
            <table class="table table-responsive table-condensed table-hover data-table">
                <thead>
                <tr class="success">
                    <th>Payment Date</th>
                    <th>Business Name</th>
                    <th>Amount</th>
                    <th>Transaction Number</th>
                    <th>Status</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($payments as $payment)
                    <tr>
                        <td>{{ $payment->created_at->format('d F, Y') }}</td>
                        <td><a href="{{ route('client.businesses.show', $payment->business->id) }}">{{ $payment->business->name }}</a></td>
                        <td class="text-right">KES {{ number_format($payment->amount_paid, 2) }}</td>
                        <td>{{ $payment->transaction_number }}</td>
                        <td><span class="btn btn-xs {{ $payment->status == 0 ? 'btn-warning' : ($payment->status == 1 ? 'btn-success' : 'btn-warning') }}">{{ $payment->status == 0 ? 'Pending' : ($payment->status == 1 ? 'Approved' : 'Disapproved') }}</span></td>
                        <td><a class="btn btn-xs btn-info" href="{{ route('client.payments.show', $payment->id) }}">View</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</section>
<hr>
<section class="content">
    <div class="box box-solid box-warning">
        <div class="box-header with-border">
            <h3 class="box-title">Pending Payments</h3>
            {{--<div class="box-tools pull-right">--}}
                {{--<a href="{{ route('client.payments.create') }}" class="btn btn-xs btn-danger">Make Payment</a>--}}
                {{--<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">--}}
                    {{--<i class="fa fa-minus"></i>--}}
                {{--</button>--}}
            {{--</div>--}}
        </div>
        <div class="box-body">
            <table class="table table-responsive table-condensed table-hover data-table">
                <thead>
                <tr class="success">
                    <th>Payment Date</th>
                    <th>Business Name</th>
                    <th>Amount</th>
                    <th>Transaction Number</th>
                    <th>Status</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($payments->where('status', \SmoDav\Models\Payment::STATUS_PENDING) as $payment)
                    <tr>
                        <td>{{ $payment->created_at->format('d F, Y') }}</td>
                        <td><a href="{{ route('client.businesses.show', $payment->business->id) }}">{{ $payment->business->name }}</a></td>
                        <td class="text-right">KES {{ number_format($payment->amount_paid, 2) }}</td>
                        <td>{{ $payment->transaction_number }}</td>
                        <td><span class="btn btn-xs {{ $payment->status == 0 ? 'btn-warning' : ($payment->status == 1 ? 'btn-success' : 'btn-warning') }}">{{ $payment->status == 0 ? 'Pending' : ($payment->status == 1 ? 'Approved' : 'Disapproved') }}</span></td>
                        <td><a class="btn btn-xs btn-info" href="{{ route('client.payments.show', $payment->id) }}">View</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</section>
<hr>
<section class="content">
    <div class="box box-solid box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Approved Payments</h3>
            {{--<div class="box-tools pull-right">--}}
                {{--<a href="{{ route('client.payments.create') }}" class="btn btn-xs btn-danger">Make Payment</a>--}}
                {{--<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">--}}
                    {{--<i class="fa fa-minus"></i>--}}
                {{--</button>--}}
            {{--</div>--}}
        </div>
        <div class="box-body">
            <table class="table table-responsive table-condensed table-hover data-table">
                <thead>
                <tr class="success">
                    <th>Payment Date</th>
                    <th>Business Name</th>
                    <th>Amount</th>
                    <th>Transaction Number</th>
                    <th>Status</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($payments->where('status', \SmoDav\Models\Payment::STATUS_APPROVED) as $payment)
                    <tr>
                        <td>{{ $payment->created_at->format('d F, Y') }}</td>
                        <td><a href="{{ route('client.businesses.show', $payment->business->id) }}">{{ $payment->business->name }}</a></td>
                        <td class="text-right">KES {{ number_format($payment->amount_paid, 2) }}</td>
                        <td>{{ $payment->transaction_number }}</td>
                        <td><span class="btn btn-xs {{ $payment->status == 0 ? 'btn-warning' : ($payment->status == 1 ? 'btn-success' : 'btn-warning') }}">{{ $payment->status == 0 ? 'Pending' : ($payment->status == 1 ? 'Approved' : 'Disapproved') }}</span></td>
                        <td><a class="btn btn-xs btn-info" href="{{ route('client.payments.show', $payment->id) }}">View</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</section>
<hr>
<section class="content">
    <div class="box box-solid box-danger">
        <div class="box-header with-border">
            <h3 class="box-title">Disapproved Payments</h3>
            {{--<div class="box-tools pull-right">--}}
                {{--<a href="{{ route('client.payments.create') }}" class="btn btn-xs btn-primary">Make Payment</a>--}}
                {{--<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">--}}
                    {{--<i class="fa fa-minus"></i>--}}
                {{--</button>--}}
            {{--</div>--}}
        </div>
        <div class="box-body">
            <table class="table table-responsive table-condensed table-hover data-table">
                <thead>
                <tr class="success">
                    <th>Payment Date</th>
                    <th>Business Name</th>
                    <th>Amount</th>
                    <th>Transaction Number</th>
                    <th>Status</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($payments->where('status', \SmoDav\Models\Payment::STATUS_DISAPPROVED) as $payment)
                    <tr>
                        <td>{{ $payment->created_at->format('d F, Y') }}</td>
                        <td><a href="{{ route('client.businesses.show', $payment->business->id) }}">{{ $payment->business->name }}</a></td>
                        <td class="text-right">KES {{ number_format($payment->amount_paid, 2) }}</td>
                        <td>{{ $payment->transaction_number }}</td>
                        <td><span class="btn btn-xs {{ $payment->status == 0 ? 'btn-warning' : ($payment->status == 1 ? 'btn-success' : 'btn-warning') }}">{{ $payment->status == 0 ? 'Pending' : ($payment->status == 1 ? 'Approved' : 'Disapproved') }}</span></td>
                        <td><a class="btn btn-xs btn-info" href="{{ route('client.payments.show', $payment->id) }}">View</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</section>
<hr>

@endsection