@extends('client.layout')
@section('content')
    <section class="content-header">
        <h1>
            Inquiries
            <small>Manage messages sent to your businesses.</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('client.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="{{ route('client.inquiries.index') }}">Inquiries</a></li>
            <li class="active">View Inquiry</li>
        </ol>
    </section>

    <section class="content">
        <div class="box box-solid box-default">
            <div class="box-header with-border">
                <h3 class="box-title">View Inquiry</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <hr>
                        <div class="form-group">
                            <label for="field_of_work_id">Subject</label>
                            <h4>{{ $inquiry->subject }}</h4>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label for="field_of_work_id">Sent By</label>
                            <h4>{{ $inquiry->full_name }}</h4>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label for="field_of_work_id">Email</label>
                            <h4>{{ $inquiry->email }}</h4>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label for="field_of_work_id">mobile</label>
                            <h4>{{ $inquiry->mobile }}</h4>
                        </div>
                        <hr>

                        <div class="form-group">
                            <a href="{{ URL::previous() }}" class="btn btn-danger">Back</a>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="field_of_work_id">Message</label>
                            <hr>
                            <p>{{ $inquiry->message }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
