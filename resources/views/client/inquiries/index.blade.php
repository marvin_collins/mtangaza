@extends('client.layout')
@section('content')
<section class="content-header">
    <h1>
        Inquiries
        <small>Manage messages sent to your businesses.</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('client.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="{{ route('client.inquiries.index') }}">Inquiries</a></li>
    </ol>
</section>
<section class="content">
    <div class="box box-solid box-primary">
        <div class="box-header with-border">
            <a href="{{ URL::previous() }}" class="btn btn-xs btn-danger">Back</a>
            <h3 class="box-title">Your Inquiries</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <table class="table table-responsive table-condensed table-hover data-table">
                <thead>
                <tr class="success">
                    <th>Subject</th>
                    <th>Left By</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>Created At</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($inquiries as $inquiry)
                    <tr>
                        <td>
                            <a href="{{ route('client.inquiries.show', $inquiry->id) }}">{{ $inquiry->subject }}</a></td>
                        <td>{{ $inquiry->full_name }}</td>
                        <td>{{ $inquiry->email }}</td>
                        <td>{{ $inquiry->mobile }}</td>
                        <td>{{ $inquiry->created_at->format('d F Y') }}</td>
                        <td><span class="btn btn-xs {{ $inquiry->status == 0 ? 'btn-danger' : 'btn-success' }}">{{ $inquiry->status == 0 ? 'UNREAD' : 'READ' }}</span></td>
                        <td><a class="btn btn-xs btn-danger" href="{{ route('client.inquiries.destroy', $inquiry->id) }}" data-method="delete" rel="nofollow" data-confirm="Are you sure you want to delete this?" data-token="{{ csrf_token() }}">Delete</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</section>
@endsection