@extends('client.layout')
@section('content')
<section class="content-header">
    <h1>
        Inquiries
        <small>Manage messages sent to your businesses.</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('client.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="{{ route('client.inquiries.index') }}">Inquiries</a></li>
    </ol>
</section>
<section class="content">
    <div class="box box-solid box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">All Registered Businesses</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <h4>Select a business to view inquiries.</h4>
            <table class="table table-responsive table-condensed table-hover data-table">
                <thead>
                <tr class="success">
                    <th>Business Name</th>
                    <th>Category</th>
                    <th>Contacts</th>
                    <th>Status</th>
                    <th>Created At</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($businesses as $business)
                    <tr>
                        <td><a href="{{ route('client.inquiries.business', $business->id) }}">{{ $business->name }}</a></td>
                        <td>{{ $business->category->name }}</td>
                        <td>{{ $business->contact_numbers }}</td>
                        <td><span class="btn btn-xs {{ $business->active == 0 ? 'btn-danger' : 'btn-success' }}">{{ $business->active == 0 ? 'Unpublished' : 'Published' }}</span></td>
                        <td>{{ $business->created_at->format('d F, Y') }}</td>
                        <td><a class="btn btn-xs btn-info" href="{{ route('client.businesses.edit', $business->id) }}">Edit</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</section>

@endsection