@extends('client.layout')
@section('content')
<section class="content-header">
    <h1>
        Handshakes
        <small>Manage Your Handshakes.</small>
    </h1>
    @if($active != "active")
        <strong><h5 class="">Please upgrade your business plan to use this feature</h5></strong>
    @endif
    <ol class="breadcrumb">
        <li><a href="{{ route('client.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="{{ route('client.handshakes.index') }}">Handshakes</a></li>
    </ol>
</section>
<section class="content">
    <div class="box box-solid box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Businesses You Are Connected to</h3>
            <div class="box-tools pull-right">
                @if($active != "active")
                    <a href="#" class="btn btn-xs btn-danger" disabled="disabled">Upgrade business plan to connect other business</a>
                @else
                <a href="{{ route('client.handshakes.create', ['id' => $business_id]) }}" class="btn btn-xs btn-danger">Connect</a>
                @endif
            </div>
        </div>
        <div class="box-body">
            <table class="table table-responsive table-condensed table-hover data-table">
                <thead>
                <tr class="success">
                    <th>Business</th>
                    <th>Contacts</th>
                    <th>Website</th>
                    <th>Email</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                    @foreach($following as $handshake)
                    <tr>
                        <td>{{ $handshake->business->name }}</td>
                        <td>{{ $handshake->business->contact_numbers }}</td>
                        <td>{{ $handshake->business->website }}</td>
                        <td>{{ $handshake->business->email }}</td>
                        <td><a class="btn btn-xs btn-info" href="{{ url('share', [$handshake->business_id]) }}">View</a>
                        <a class="btn btn-xs btn-danger" href="{{ route('client.handshakes.destroy', $handshake->id) }}" data-method="delete" rel="nofollow" data-confirm="Are you sure you want to unfollow?" data-token="{{ csrf_token() }}">Disconnect</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</section>
<hr>
<section class="content">
    <div class="box box-solid box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Businesses Connected to You</h3>
            {{--<div class="box-tools pull-right">--}}
                {{--<a href="{{ route('client.handshakes.create', ['id' => $business_id]) }}" class="btn btn-xs btn-danger">Follow Business</a>--}}
                {{--<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">--}}
                    {{--<i class="fa fa-minus"></i>--}}
                {{--</button>--}}
            {{--</div>--}}
        </div>
        <div class="box-body">
            <table class="table table-responsive table-condensed table-hover data-table">
                <thead>
                <tr class="success">
                    <th>Business</th>
                    <th>Contacts</th>
                    <th>Website</th>
                    <th>Email</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                    @foreach($followers as $handshake)
                    <tr>
                        <td>{{ $handshake->follower->name }}</td>
                        <td>{{ $handshake->follower->contact_numbers }}</td>
                        <td>{{ $handshake->follower->website }}</td>
                        <td>{{ $handshake->follower->email }}</td>
                        <td><a class="btn btn-xs btn-info" href="{{ url('share', [$handshake->follower_id])
                        }}">View</a>
                        <a class="btn btn-xs btn-danger" href="{{ route('client.handshakes.destroy', $handshake->id) }}" data-method="delete" rel="nofollow" data-confirm="Are you sure you want to unfollow?" data-token="{{ csrf_token() }}">Disconnect</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</section>
@endsection