@extends('client.layout')
@section('content')
    <section class="content-header">
        <a href="{{url()->previous()}}" class="btn btn-sm btn-danger">Back</a>
        <h1>
            Handshakes
            <small>Manage Your Handshakes.</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('client.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="{{ route('client.handshakes.index') }}">Handshakes</a></li>
            <li class="active">Connect</li>
        </ol>
    </section>
    <div class="row"><nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->


                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">

                    </ul>

                    <ul class="nav navbar-nav navbar-right">

                        <li class="dropdown">
                            <form action="{{route('client.handshakes.store')}}" method="post" enctype="multipart/form-data" class="navbar-form
                            navbar-left"
                                  >{{ csrf_field() }}
                                <div class="form-group">
                                    <input type="hidden" name="business_id" value="{{ $business_id }}">
                                    <select style="width:300px" name="bulkbusiness_id[]" id="business_id"
                                            class="form-control
                            select2" required multiple>

                                        @foreach($businesses as $busines)
                                            {{--dd($city)--}}
                                            <option {{ in_array([$busines->id], [$busines->name]) ? 'selected' : ''
                                            }} value="{{
                                     $busines->id }}">{{ $busines->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-default">Follow</button>
                            </form>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav></div>

    <section class="content">
        <div class="row">
    @foreach($businesses as $business)
        <div class="col-sm-3">
            <a href="{{ $business->id }}" class="link">
                <div class="box box-solid box-default">
                    <div class="box-header">
                        <h5>{{ $business->name }}</h5>
                    </div>
                    <div class="box-body">
                        <img src="{{ asset($business->logo) }}" alt="{{ $business->name }}" class="img-responsive img-center">
                    </div>
                    <div class="box-footer text-center">
                        <span>Follow</span>
                    </div>
                </div>
            </a>
        </div>
    @endforeach
        </div>
    </section>
@endsection
@section('footer')
    <style>
        .link:hover .box-footer span:before{
            content: 'Click to ';
        }
    </style>
    <script>
        $('.link').on('click', function (e) {
            var item = $(this);
            e.preventDefault();
            $.ajax({
                method: 'post',
                url: '{{ route('client.handshakes.store') }}',
                data: {
                    _token: '{{ csrf_token() }}',
                    business_id: '{{ $business_id }}',
                    status: '1',
                    selected: $(this).attr('href')
                },
                success: function(response) {
                    item.parent().fadeOut(1000);
                    setTimeout(function() {
                        item.parent().remove();
                    }, 1000);
                    window.location.reload();
                },
                error: function (response) {
                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "positionClass": "toast-top-right",
                        "onclick": null,
                        "showDuration": "1000",
                        "hideDuration": "1000",
                        "timeOut": "9000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };
                    toastr['error']("Sorry, an error occurred. Please try again.");
                }
            });
        });
    </script>
@endsection