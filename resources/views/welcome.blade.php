<!DOCTYPE html>
<html lang="en" unselectable>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>mTangaza</title>
    <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="//fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>
    <link href="{{ asset(elixir('css/app.css')) }}" rel="stylesheet">
    <meta id="tok" content="{{ csrf_token() }}">
    <link rel="icon" href="{{ asset('images/falicon.png') }}" type="image/x-icon">
    <script type="text/javascript" src="http://secure.skypeassets.com/i/scom/js/skype-uri.js"></script>
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<div id="app"></div>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBp1l35-viyZdZGJ7ZnMTGsVTUbZN4XDSk"></script>
<div style="margin-top: 0%"><img id="gif" class="center-block img-responsive" width="64px" height="64px" src="images/Progress.gif" alt="pinboard"  /></div>
<script type="text/javascript">
        $(document).ready(function() {

        });
        $(window).load(function() {
            $("#gif").hide();
        });
    </script>
<script src="{{ asset(elixir('js/app.js')) }}"></script>
</body>

</html>