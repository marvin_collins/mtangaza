import AuthPage from './pages/auth/auth.vue';
import LoginPage from './pages/auth/login.vue';
import Contact from './pages/contact.vue';
import Disclaimer from './pages/disclaimer.vue';
import Pricing from './pages/pricing.vue';
import HomePage from './pages/home.vue';
import SearchPage from './pages/search/search.vue';
import AllResults from './pages/search/all.vue';
import BusinessesPage from './pages/search/businesses.vue';
import FieldsPage from './pages/search/fields.vue';
import FieldBusinessesPage from './pages/search/fieldBusinesses.vue';
import ResultsPage from './pages/search/results.vue';
import SinglePage from './pages/search/single.vue';
import PinbordPage from './pages/pinboard.vue';

module.exports = {
    configRouter: (router) => {
        router.map({
            '/auth': {
                component: AuthPage,
                subRoutes: {
                    '/login': {
                        component: LoginPage,
                        guest: true
                    }
                }
            },
            '/disclaimer': {
                component: Disclaimer
            },
            '/contact': {
                component: Contact
            },
            '/pinboard':{
                component: PinbordPage
            },
            '/pricing': {
                component: Pricing
            },
            '/home': {
                component: HomePage
            },
            '/search': {
                name: 'search',
                component: SearchPage,
                subRoutes: {
                    '/companies': {
                        name: 'companies',
                        component: BusinessesPage,
                        grouping: 'companies'
                    },
                    '/all': {
                        component: AllResults,
                        grouping: 'all'
                    },
                    '/fields/:field_name': {
                        component: FieldBusinessesPage,
                        grouping: 'fields'
                    },
                    '/fields': {
                        component: FieldsPage,
                        grouping: 'field'
                    },
                    '/businesses/:business_name': {
                        component: SinglePage,
                        grouping: 'businesses'
                    }
                }
            }
        });

        router.alias({
            '': '/home',
            '/auth': '/auth/login',
            '/search': '/search/companies'
        });

        router.beforeEach(function (transition) {
            var token = localStorage.getItem('jwt-token');
            if (transition.to.auth) {
                if (!token || token === null) {
                    transition.redirect('/auth/login');
                }
            }
            if (transition.to.guest) {
                if (token) {
                    transition.redirect('/');
                }
            }
            transition.next();
        })
    }
};
