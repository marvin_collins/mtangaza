(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

$('.sidebar-menu li').removeClass('active');
$('.sidebar-menu a[href="' + window.location + '"]').parent('li').addClass('active');
$('.data-table').DataTable();
var laravel = {
    initialize: function initialize() {
        this.methodLinks = $('a[data-method]');
        this.registerEvents();
    },

    registerEvents: function registerEvents() {
        this.methodLinks.on('click', this.handleMethod);
    },

    handleMethod: function handleMethod(e) {
        var link = $(this);
        var httpMethod = link.data('method').toUpperCase();
        var form;

        // If the data-method attribute is not PUT or DELETE,
        // then we don't know what to do. Just ignore.
        if ($.inArray(httpMethod, ['PUT', 'DELETE']) === -1) {
            return;
        }

        // Allow user to optionally provide data-confirm="Are you sure?"
        if (link.data('confirm')) {
            if (!laravel.verifyConfirm(link)) {
                return false;
            }
        }

        form = laravel.createForm(link);
        form.submit();

        e.preventDefault();
    },

    verifyConfirm: function verifyConfirm(link) {
        return confirm(link.data('confirm'));
    },

    createForm: function createForm(link) {
        var form = $('<form>', {
            'method': 'POST',
            'action': link.attr('href')
        });

        var token = $('<input>', {
            'type': 'hidden',
            'name': '_token',
            'value': link.data('token')
        });

        var hiddenInput = $('<input>', {
            'name': '_method',
            'type': 'hidden',
            'value': link.data('method')
        });

        return form.append(token, hiddenInput).appendTo('body');
    }
};
laravel.initialize();

$('.icheck').iCheck({
    checkboxClass: 'icheckbox_square-blue',
    radioClass: 'iradio_square-blue',
    increaseArea: '20%'
});

$('.select2').select2({
    theme: "bootstrap"
});

},{}]},{},[1])

//# sourceMappingURL=compiled.js.map
