import Vue from 'vue';
import VueRouter from 'vue-router';
import rest from 'rest';
import pathPrefix from 'rest/interceptor/pathPrefix';
import mime from 'rest/interceptor/mime';
import defaultRequest from 'rest/interceptor/defaultRequest';
import errorCode from 'rest/interceptor/errorCode';
import interceptor from 'rest/interceptor';
import {load, Map, Marker} from 'vue-google-maps'
import jwtAuth from './interceptors/jwtAuth';
import { configRouter } from './routes';
import MainApp from './app.vue';
import Nav from './components/nav.vue';
import Footer from './components/footer.vue';
import Paginator from './components/pagination.vue';
var config = {
    env: 'production',
    // env: 'development',
    api: {
        base_url: window.location.origin + '/api',
        defaultRequest: {
            headers: {
                'X-Requested-With': 'rest.js',
                'Content-Type': 'application/json'
            }
        }
    },
    social: {
        facebook: 'facebook',
        twitter: '',
        github: ''
    },
    debug: false
};
Vue.use(VueRouter);
window.Vue = Vue;
window.VueRouter = VueRouter;
const router = new VueRouter({
    hashbang: false
});

var bootstrap = {
    initialize() {
        this.setupVue();
        this.setupRoutes();
        this.setupClient();
        this.boot();
    },
    setupRoutes() {
        configRouter(router);
    },
    setupClient() {
        window.client = rest.wrap(pathPrefix, { prefix: config.api.base_url })
            .wrap(mime)
            .wrap(defaultRequest, config.api.defaultRequest)
            .wrap(errorCode, { code: 400 })
            .wrap(jwtAuth);

    },
    setupVue() {
        Vue.component('main-nav', Nav);
        Vue.component('main-footer', Footer);
        Vue.component('pagination', Paginator);
        Vue.component('map', Map);
        Vue.component('marker', Marker);
    },
    boot() {
        const App = Vue.extend(MainApp);
        router.start(App, '#app');
        window.router = router;
    }
};

bootstrap.initialize();
