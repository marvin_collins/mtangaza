var elixir = require('laravel-elixir');
require('laravel-elixir-vueify');

elixir.config.js.browserify.transformers.push({name: 'envify'});
elixir.config.js.browserify.options.debug = true;

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
    mix.copy([
        'node_modules/jquery/dist/jquery.min.js',
        'node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js',
        'node_modules/toastr/build/toastr.min.js'
    ], 'resources/assets/js/admin');
    mix.copy([
        'node_modules/bootstrap-sass/assets/fonts/*'
    ], 'public/fonts');

    mix.copy([
        'node_modules/toastr/build/toastr.min.css'
    ], 'resources/assets/css/admin');
    
    mix.browserify('admin/scripts.js', 'resources/assets/js/admin/compiled.js')
        .sass('admin/style.scss', 'resources/assets/css/admin/compiled.css')
        .styles([
            'admin/compiled.css',
            'admin/font-awesome.min.css',
            'admin/ionicions.min.css',
            'admin/AdminLTE.min.css',
            'admin/skin-blue.min.css',
            'admin/icheck.css',
            'admin/toastr.min.css',
            'admin/bootstrap3-wysihtml5.min.css',
            'datatables.min.css',
            'select2.min.css'
        ], 'public/css/admin.css')
        .scripts([
            'admin/jquery.min.js',
            'admin/jquery.slimscroll.min.js',
            'admin/bootstrap.min.js',
            'admin/toastr.min.js',
            'admin/fastclick.min.js',
            'admin/app.min.js',
            'admin/icheck.min.js',
            'admin/bootstrap3-wysihtml5.all.min.js',
            'datatables.min.js',
            'select2.full.min.js',
            'admin/compiled.js'
        ], 'public/js/admin.js')
        .sass('app.scss')
        .browserify('main.js', 'public/js/app.js')
        .version(['css/app.css', 'js/app.js']);
});