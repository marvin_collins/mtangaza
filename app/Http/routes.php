<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//<script type="text/javascript" src="https://secure.skypeassets.com/i/scom/js/skype-uri.js"></script>
//<div id="SkypeButton_Call_smodav_1">
// <script type="text/javascript">
//    Skype.ui({
// "name": "dropdown",
// "element": "SkypeButton_Call_smodav_1",
// "participants": ["smodav"],
// "imageSize": 24
// });
// </script>
//</div>

use App\Http\Controllers\Auth\AdminAuthController;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Http\Request;
use SmoDav\Models\Business;

Route::get('/', function () {
    return view('welcome');
});
Route::get('api/search', 'SearchController@getResults');
Route::get('api/search/businesses', 'SearchController@getBusinesses');
Route::get('api/search/business', 'SearchController@getBusinessBySlug');
Route::get('api/search/fieldBusinesses', 'SearchController@getFieldBusinessesBySlug');
Route::get('api/search/fields', 'SearchController@getFields');
Route::get('api/featured', 'SearchController@getFeatured');
Route::get('api/refresh', 'SearchController@refreshIndex');
Route::post('api/inquiry', function (Request $request, Mailer $mailer) {
    $data = $request->all();
    $business = Business::whereSlug($data['slug'])->first();

    $mailer->send('emails.contact', ['data' => $data], function ($message) use ($data, $business) {
        $message
            ->from($data['email'], $data['name'])
            ->to($business->email)
            ->subject('Inquiry from Contact Form' . $data['subject']);
    });

    return 'success';
});

Route::post('api/contact', function (Request $request, Mailer $mailer) {
    $data = $request->all();
    $mailer->send('emails.contact', ['data' => $data], function ($message) use ($data) {
        $message
            ->from($data['email'], $data['name'])
            ->to('info@mtangaza.com')
            ->subject('Inquiry from Contact Form' . $data['subject']);
    });
    return 'success';
});

Route::group(['middleware' => ['guest']], function () {
    $admin = AdminAuthController::ADMIN_URL;
    Route::get($admin . '/login', ['as' => 'admin.login.index', 'uses' => 'Auth\AdminAuthController@getLogin']);
    Route::post($admin . '/login', ['as' => 'admin.login.store', 'uses' => 'Auth\AdminAuthController@postLogin']);
    Route::get('client/login', ['as' => 'client.login.index', 'uses' => 'Auth\ClientAuthController@getLogin']);
    Route::post('client/login', ['as' => 'client.login.store', 'uses' => 'Auth\ClientAuthController@postLogin']);
});

Route::group(['prefix' => 'client', 'middleware' => ['client']], function () {
    Route::get('/', ['as' => 'client.home', 'uses' => 'Client\ClientPagesController@home']);
    Route::get('/logout', ['as' => 'client.login.destroy', 'uses' => 'Auth\ClientAuthController@logout']);
    Route::get('/changePassword', ['as' => 'client.login.edit', 'uses' => 'Auth\ClientAuthController@edit']);
    Route::put('/changePassword', ['as' => 'client.login.update', 'uses' => 'Auth\ClientAuthController@update']);
    Route::resource('/users', 'Client\ClientUserController');
    Route::resource('/businesses', 'Client\ClientBusinessesController');
    Route::resource('/payments', 'Client\ClientPaymentsController');

    Route::resource('/clients', 'ClientsController');
    Route::resource('/features', 'FeaturesController');
    Route::resource('/plans', 'PlansController');
    Route::resource('/categories', 'CategoriesController');
    Route::resource('/fields', 'FieldsController');
    Route::resource('/interests', 'Client\InterestsController');
    Route::get('/handshakes/businesses/{businessId}', [
        'as' => 'client.handshakes.business',
        'uses' => 'Client\HandshakesController@business'
    ]);
    Route::resource('/handshakes', 'Client\HandshakesController');
    Route::get('/inquiries/businesses/{businessId}', [
        'as' => 'client.inquiries.business',
        'uses' => 'Client\InquiryController@business'
    ]);
    Route::resource('/inquiries', 'Client\InquiryController');
    Route::get('/notifications','NotificationController@index');
    Route::get('/notifications/{id}','NotificationController@update');
    Route::get('/notifications/decline/{id}','NotificationController@decline');
});

Route::group(['prefix' => AdminAuthController::ADMIN_URL, 'middleware' => ['admin']], function () {
    Route::get('/', ['as' => 'admin.home', 'uses' => 'AdminPagesController@home']);
    Route::get('/logout', ['as' => 'admin.login.destroy', 'uses' => 'Auth\AdminAuthController@logout']);
    Route::get('/changePassword', ['as' => 'admin.login.edit', 'uses' => 'Auth\AdminAuthController@edit']);
    Route::put('/changePassword', ['as' => 'admin.login.update', 'uses' => 'Auth\AdminAuthController@update']);
    Route::resource('/users', 'UsersController');
    Route::get('/clients/state/{userId}', ['as' => 'manage.clients.state', 'uses' => 'ClientsController@state']);
    Route::resource('/clients', 'ClientsController');
    Route::resource('/features', 'FeaturesController');
    Route::resource('/plans', 'PlansController');
    Route::resource('/categories', 'CategoriesController');
    Route::resource('/fields', 'FieldsController');
    Route::get('/businesses/state/{businessId}', [
        'as' => 'manage.businesses.state',
        'uses' => 'BusinessesController@state'
    ]);
    Route::resource('/businesses', 'BusinessesController');

    Route::resource('/pending', 'PendingBusinessesController');
    Route::resource('/payments', 'PaymentsController');
    Route::get('/newsletter','NewsletterController@index');
    Route::post('/newsletter/email','NewsletterController@email');

});
Route::get('/share/{id}','TestController@index');
Route::post('/email','TestController@sendMail');
Route::get('newuser','NewRegistration@index');
Route::post('newuser/register','NewRegistration@register');
Route::post('upload','TestController@upload');
Route::post('/business/images','TestController@deleteImage');
