<?php

namespace App\Http\Middleware;


use App\Http\Controllers\Auth\AdminAuthController;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Closure;
use SmoDav\Models\Auth\Role;

class Administrator
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! $user = Sentinel::check()) {
            flash('Please sign in to continue.', 'error');

            return redirect()->guest(AdminAuthController::ADMIN_URL . '/login');
        }

        if (! $user->hasAccess([Role::BACKEND_ACCESS])) {
            flash('Sorry, you are not authorised to access this section.', 'error');

            return redirect()->guest(AdminAuthController::ADMIN_URL . '/login');
        }

        return $next($request);
    }
}
