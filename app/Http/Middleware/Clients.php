<?php

namespace App\Http\Middleware;


use App\Http\Controllers\Auth\AdminAuthController;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Closure;
use SmoDav\Models\Auth\Role;

class Clients
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! $user = Sentinel::check()) {
            flash('Please sign in to continue.', 'error');

            return redirect()->guest('client/login');
        }

        if (! $user->hasAnyAccess([Role::CLIENT_ACCESS, 'superuser'])) {
            Sentinel::logout(null, true);
            flash('Sorry, you are not authorised to access this section.', 'error');

            return redirect()->guest('client/login');
        }

        if (! session()->has('client_id')) {
            Sentinel::logout(null, true);
            flash('Please log in to continue.', 'error');

            return redirect()->guest('client/login');
        }

        return $next($request);
    }
}
