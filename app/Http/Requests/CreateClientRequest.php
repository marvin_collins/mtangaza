<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use SmoDav\Models\Client;

class CreateClientRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Sentinel::hasAnyAccess([Client::PERMISSION_CREATE, 'superuser']);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'gender' => 'required',
            'nationality' => 'required',
            'identification_type' => 'required',
            'identification_number' => 'required',
            'mobile' => 'required'
        ];

        if ($this->method == 'PUT') {
            $rules['email'] = 'required | email | unique:users,email,' . $this->uid;

            return $rules;
        }
        $rules['email'] = 'required | email | unique:users';

        return $rules;
    }
}
