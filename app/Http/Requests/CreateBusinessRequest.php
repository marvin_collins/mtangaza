<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use SmoDav\Models\Business;

class CreateBusinessRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Sentinel::hasAnyAccess([Business::PERMISSION_CREATE, 'superuser']);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'client_id' => 'required',
            'category_id' => 'required',
            'contact_numbers' => 'required',
            'city_id' => 'required',
            'name' => 'required | min:3',
            'description' => 'required | min:10',
            'email' => 'required | email | min:5',
            'logo' => 'mimes:jpeg,jpg,png | max:1000',
        ];
    }
}
