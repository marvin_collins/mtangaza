<?php

namespace App\Http\Requests\Client;

use App\Http\Requests\Request;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use SmoDav\Models\Auth\Role;
use SmoDav\Models\SubClient;

class CreateSubClientRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Sentinel::hasAnyAccess([SubClient::PERMISSION_CREATE, Role::CLIENT_ACCOUNT_OWNER]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'gender' => 'required',
            'nationality' => 'required',
            'identification_type' => 'required',
            'identification_number' => 'required',
            'mobile' => 'required'
        ];

        if ($this->method == 'PUT') {
            $rules['email'] = 'required | email | unique:users,email,' . $this->uid;

            return $rules;
        }
        $rules['email'] = 'required | email | unique:users';

        return $rules;
    }
}
