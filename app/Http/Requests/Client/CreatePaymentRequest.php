<?php

namespace App\Http\Requests\Client;

use App\Http\Requests\Request;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use SmoDav\Models\Auth\Role;
use SmoDav\Models\Payment;

class CreatePaymentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Sentinel::hasAnyAccess([Payment::PERMISSION_CREATE, Role::CLIENT_ACCOUNT_OWNER]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'business_id' => 'required',
            'plan_id' => 'required',
            'transaction_number' => 'required',
        ];
    }
}
