<?php

namespace App\Http\Requests\Client;

use App\Http\Requests\Request;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use SmoDav\Models\Auth\Role;
use SmoDav\Models\Business;

class UpdateBusinessRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Sentinel::hasAnyAccess([Business::PERMISSION_CREATE, Role::CLIENT_ACCOUNT_OWNER]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id' => 'required',
            'contact_numbers' => 'required',
            'city_id' => 'required',
            'name' => 'required | min:3',
            'description' => 'required | min:10',
            'email' => 'required | email | min:5',
        ];
    }
}
