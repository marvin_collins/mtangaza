<?php

namespace App\Http\Requests\Fields;

use App\Http\Requests\Request;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use SmoDav\Models\FieldOfWork;

class CreateFieldRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required | unique:field_of_works',
            'image' => 'mimes:jpeg,jpg,png,PNG | max:1536'
        ];
    }
}
