<?php

namespace App\Http\Requests\Fields;

use App\Http\Requests\Request;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use SmoDav\Models\FieldOfWork;

class DeleteFieldRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Sentinel::hasAnyAccess([FieldOfWork::PERMISSION_DELETE, 'superuser']);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }
}
