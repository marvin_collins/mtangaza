<?php

namespace App\Http\Controllers;

use App\BusinessToBusinessShare;
use App\BusinessUpload;
use App\Upload;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use SmoDav\Models\Business;
use Illuminate\Support\Facades\Input;
use RecursiveIteratorIterator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use SmoDav\Models\BusinessDocument;
use SmoDav\Models\BusinessImage;
use SmoDav\Models\Client;
use SmoDav\Models\Followers;
use Illuminate\Support\Facades\Mail;
use Event;
use App\Events\sendMail;

class TestController extends Controller
{
    private $business;
    private $client_id;

    /**
     * TestController constructor.
     * @param $business
     */
    public function __construct(Business $business)
    {

        $this->business = $business;
        $this->client_id = session('client_id');
    }

    public function index($id)
    {

        $businessdetails = $this->business->where('id', $id)->first();

        $fileSharedPublic = BusinessUpload::all();
        $businessPublicfiles = $fileSharedPublic->map(function ($item,$key) use ($id) {
            if (in_array($id,array_flatten(json_decode($item->businesses_id)))) {
                return [
                    'name' => $item->name,
                    'url' => $item->doc_link
                ];
            }

        });
//        dd($id,Session::get('business_id'));



        $sharedB2B = BusinessToBusinessShare::all();
        $business2business = $sharedB2B->map(function ($item) use ($id) {
            $b2b_id = json_decode($item->follower_id);
            if (in_array(Session::get('business_id'),$b2b_id) && $item->business_id == $id) {
                return [
                    'name' => $item->name,
                    'url' => $item->doc_link
                ];
            }
        });

//        dd($business2business,$id ,$sharedB2B);


        return view('client.share.index')->with('businessdetails', $businessdetails)
            ->with('sharedDocs', $businessPublicfiles)
            ->with('specificShares', $business2business);
    }

    public function deleteImage(Request $request)
    {
        if(BusinessImage::findorfail($request->image_id)->delete())
        {
            return ['result'=>'success','image_id'=>$request->image_id];
        }
        else
        {
            return ['result'=>'failed'];
        }
    }

    public function upload(Request $request)
    {
        $file = Input::file('doc');
        if (is_null($file)) {
            flash('Make sure you add file', 'warning');
            return redirect()->back();
        }
        $size = File::size($file);
        $name = $request->get('name');
        $documentName = $name . str_random(10) . '.' . $file->getClientOriginalExtension();
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'doc' => 'required|mimes:png,jpg,jpeg,gif,doc,docx,pdf,xls,xlsx,pptx,ppt.csv'
        ]);

        if ($validator->fails()) {
            $errorMsg = '';
            foreach ($validator->messages()->getMessages() as $key => $message) {
                $count = count($validator->messages());
                $i = $count - $count;
                if ($i != $count) {
                    $errorMsg = $errorMsg . $message[$i];
                }
            }
            flash($errorMsg, 'warning');
            return redirect()->back();
        }
        if ($size > 6000000) {
            flash('The file you uploaded exceed the max limit (5mb)', 'warning');
            return redirect()->back();
        }
            $file->move(public_path() . '/images/uploadsfiles/', $documentName);
            $url = '/images/uploadsfiles/' . $documentName;
            if ($request->get('follower_id')) {
                $ar1 = $request->get('business_id');
                empty($ar1)? $ar1 =[$request->get('follower_id')] :array_push($ar1,$request->get('follower_id'));
                $uploadArray = [];
                $uploadArray['name'] = $name;
                $uploadArray['business_id'] = Session::get('business_id');
                $uploadArray['follower_id'] = json_encode($ar1);
                $uploadArray['doc_link'] = $url;
                $uploadArray['created_at'] = Carbon::now();
                $uploadArray['updated_at'] = Carbon::now();

//                dd($uploadArray);
                BusinessToBusinessShare::insert($uploadArray);
                flash('Document Uploaded successfully', 'success');
                return redirect()->back();

            }

            if(!$request->has('selected'))
            {
                flash('Make sure you select business', 'warning');
                return redirect()->back();
            }
                $businessDocArray = [];
                $businessDocArray['name'] = $name;
                $businessDocArray['businesses_id'] = json_encode($request->get('selected')) ;
                $businessDocArray['doc_link'] = $url;
                $businessDocArray['created_at'] = Carbon::now();
                $businessDocArray['updated_at'] = Carbon::now();
                BusinessUpload::insert($businessDocArray);

            flash('Document Uploaded successfully', 'success');
            return redirect()->back();


    }

//    public function sendMail(Request $request)
//    {
//        $title = $request->input('title');
//        $content = $request->input('content');
//
//        Mail::send('client.share.emailsend', ['title' => $title, 'content' => $content], function ($message)
//        {
//
//            $message->from('marvin@appslab.co.ke', 'Chris');
//
//            $message->to('marvincollins14@gmail.com');
//
//        });
//
//        dd(response()->json(['message' => 'Request completed']));
////        return view('client.share.emailsend');
//    }

    public function broadcastOn()
    {
        dd([]);
    }


}
