<?php

namespace App\Http\Controllers;

use App\Http\Requests\FeaturesRequest;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use SmoDav\Models\Module;

class FeaturesController extends Controller
{
    /**
     * @var Module
     */
    private $feature;

    /**
     * FeaturesController constructor.
     *
     * @param Module $feature
     */
    public function __construct(Module $feature)
    {
        $this->feature = $feature;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        dd($this->feature->all());
        return view('admin.features.index')->withFeatures($this->feature->all());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param FeaturesRequest $request
     *
     * @return \Illuminate\Http\Response
     * @internal param int $featureId
     */
    public function edit(FeaturesRequest $request, $featureId)
    {
        flash('Successfully completed action.', 'success');
        if ($featureId == 'all-1') {
            DB::table('modules')->update(['active' => 1]);

            return redirect()->route('manage.features.index');
        }

        if ($featureId == 'all-0') {
            DB::table('modules')->update(['active' => 0]);

            return redirect()->route('manage.features.index');
        }

        $feature = $this->feature->findOrFail($featureId);
        $feature->active = ! $feature->active;
        $feature->save();

        return redirect()->route('manage.features.index');
    }
}
