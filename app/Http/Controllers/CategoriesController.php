<?php

namespace App\Http\Controllers;

use App\Http\Requests\Categories\CreateCategoryRequest;
use App\Http\Requests\Categories\DeleteCategoryRequest;
use App\Http\Requests\Categories\EditCategoryRequest;
use Illuminate\Http\Request;

use App\Http\Requests;
use SmoDav\Models\Category;

class CategoriesController extends Controller
{
    /**
     * @var Category
     */
    private $category;

    /**
     * CategoriesController constructor.
     *
     * @param Category $category
     */
    public function __construct(Category $category)
    {
        $this->category = $category;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.categories.index')
            ->withCategories($this->category->all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param CreateCategoryRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCategoryRequest $request)
    {
        $data = $request->all();
        $data['slug'] = makeSlug($data['name'], 'name', $this->category);
        $this->category->create($data);
        flash('Successfully created new category', 'success');

        return redirect()->route('manage.categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $categoryId
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($categoryId)
    {
        $category = $this->category->findOrFail($categoryId);

        return view('admin.categories.edit')->withCategory($category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  EditCategoryRequest  $request
     * @param  int  $categoryId
     * @return \Illuminate\Http\Response
     */
    public function update(EditCategoryRequest $request, $categoryId)
    {
        $category = $this->category->findOrFail($categoryId);
        $data = $request->all();
        $data['slug'] = makeSlug($data['name'], 'name', $this->category, true);
        $category->fill($data);
        $category->save();
        flash('Successfully edited category', 'success');

        return redirect()->route('manage.categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $categoryId
     * @param DeleteCategoryRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($categoryId, DeleteCategoryRequest $request)
    {
        $category = $this->category->findOrFail($categoryId);

        // TODO: check for mapped categories
        $category->delete();
        flash('Successfully deleted category', 'success');

        return redirect()->route('manage.categories.index');
    }
}
