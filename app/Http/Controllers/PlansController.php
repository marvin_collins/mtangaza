<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use SmoDav\Models\Module;
use SmoDav\Models\ModulePlan;
use SmoDav\Models\ModulesPlan;
use SmoDav\Models\Plan;

class PlansController extends Controller
{
    /**
     * @var Plan
     */
    private $plan;
    /**
     * @var Module
     */
    private $modules;

    /**
     * PlansController constructor.
     *
     * @param Plan        $plan
     * @param Module     $modules
     */
    public function __construct(Plan $plan, Module $modules)
    {
        $this->plan = $plan;
        $this->modules = $modules;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.plans.index')
            ->withPlans($this->plan->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $planId
     * @return \Illuminate\Http\Response
     */
    public function show($planId)
    {
        $plan = $this->plan->with(['features'])->findOrFail($planId);
        $features = $this->modules->whereActive(true)->get()->keyBy('id');

        return view('admin.plans.show')
            ->withFeatures($features)
            ->withPlan($plan);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $planId
     * @return \Illuminate\Http\Response
     */
    public function edit($planId)
    {
        $plan = $this->plan->with(['features'])->findOrFail($planId);
        $features = $this->modules->whereActive(true)->get()->keyBy('id');
        
        return view('admin.plans.edit')
            ->withFeatures($features)
            ->withPlan($plan);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $planId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $planId)
    {
        $enabledFeatures = $request->all();
        $plan = $this->plan->findOrFail($planId);
        $selectedFeatues = [];
        $now = Carbon::now();
        foreach ($enabledFeatures as $key => $value) {
            if (starts_with($key, 'feature')) {
                $selectedFeatues [] = [
                    'module_id' => substr($key, strlen('feature_'), strlen('feature_')),
                    'plan_id' => $plan->id,
                    'created_at' => $now,
                    'updated_at' => $now
                ];
            }
        }
        $plan->cost = $request->get('cost');
        $plan->save();
        ModulesPlan::wherePlanId($plan->id)->delete();
        ModulesPlan::insert($selectedFeatues);
        flash('Successfully updated subscription plan', 'success');

        return redirect()->route('manage.plans.index');
    }

}
