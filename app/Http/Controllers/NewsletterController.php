<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Event;
use App\Events\NewsLetter;
use Illuminate\Http\Request;

use App\Http\Requests;

class NewsletterController extends Controller
{
    public function index()
    {
        return view('admin.newsletter.index');
    }

    public function email(Request $request)
    {
        Event::fire(new NewsLetter($request));
    }
}
