<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Http\Response;
use SearchIndex;
use SmoDav\Commons\DashboardSat;
use SmoDav\Models\Business;
use SmoDav\Models\FieldOfWork;
use SmoDav\Models\Module;
use SmoDav\Models\Subscription;

class SearchController extends Controller
{
    public function refreshIndex()
    {
        SearchIndex::clearIndex();
        foreach (Business::all() as $business) {
            if ($business->status == Business::STATUS_APPROVED) {
                SearchIndex::upsertToIndex($business);
            }
        }
        foreach (FieldOfWork::all() as $field) {
            SearchIndex::upsertToIndex($field);
        }
    }

    public function sendResults($query, $searchKey)
    {
        $results = SearchIndex::getResults($query);
        $results = collect($results['hits']['hits']);
        $results->each(function ($item, $key) use ($searchKey, $results) {
            $words = explode(' ', $searchKey);
            $score = $item['_score'];
            $id = $item['_id'];
            $item = $item['_source'];
            $name = $item['name'];
            $description = $item['description'];

            if (strlen($searchKey) > 0) {
                foreach ($words as $word) {
                    $name = preg_replace("/{$word}/i", "<em>\$0</em>", $name);
                    $description = preg_replace("/{$word}/i", "<em>\$0</em>", $description);
                }
            }

            $item = [
                'score' => $score,
                'id' => $id,
                'name' => $name,
                'description' => $description,
                'logo' => $item['logo'],
                'url' => $item['url']
            ];
            $results[$key] = $item;
        });

        $results = $results->sortBy('name');
        $results->values()->all();

        if (strlen($searchKey) < 1) {
            $newResults = collect();
            foreach ($results as $result) {
                $newResults->push($result);
            }
            $results = $newResults;
        }

        return response()->json($results);
    }

    public function getResults(Request $request)
    {
        $searchKey = $request->get('query');
        $query = $this->getQuery($searchKey);

        return $this->sendResults($query, $searchKey);
    }

    public function getBusinesses(Request $request)
    {
        $searchKey = $request->get('query');
        $query = $this->getQuery($searchKey, 'business');

        return $this->sendResults($query, $searchKey);
    }

    public function getFields(Request $request)
    {
        $searchKey = $request->get('query');
        $query = $this->getQuery($searchKey, 'field_of_work');

        return $this->sendResults($query, $searchKey);
    }

    private function getQuery($searchKey, $type = null)
    {
        $query = [
            'body' =>
                    [
                        'from' => 0,
                        'size' => 500,
                        'filter' => [
                            'match' => [
                                'active' => 1
                            ]
                        ],
                        'query' =>
                            [
                                'match' =>
                                    [
                                        '_all' => [
                                            'query' => $searchKey,
                                            'zero_terms_query' => 'all',
                                        ]
                                    ],

                            ],
                    ]
        ];

        if ($type) {
            $query ['type'] = $type;
        }

        return $query;
    }

    public function getBusinessBySlug(Request $request)
    {
        $slug = $request->get('query');
        if (strlen($slug) < 2) {
            abort(404);
        }
        $business = Business::with(['cities', 'images', 'fieldsOfWork'])->whereSlug($slug)->first();
        $business->cities = $business->cities->toArray();
        $business->images = $business->images->toArray();
        $business->fieldsOfWork = $business->fieldsOfWork->toArray();

        $plan = $this->getActivePlan($business->id);
        $businessDetails = $this->filterDetails($plan, $business);
        $businessDetails['cities'] = $business->cities;
        $businessDetails['fieldsOfWork'] = $business->fieldsOfWork;
        $businessDetails['name'] = $business->name;
        $businessDetails['slug'] = $business->slug;

        return response()->json($businessDetails);
    }

    public function getFieldBusinessesBySlug(Request $request)
    {
        $slug = $request->get('query');

        if (strlen($slug) < 2) {
            abort(404);
        }

        $field = FieldOfWork::whereSlug($slug)->first();
        $businesses = $field->business()->whereActive(1)->whereStatus(1)->get();
        $businesses = $businesses->each(function ($value) {
            $value->url = 'businesses/' . $value->slug;
        });

        return response()->json($businesses);
    }

    private function getActivePlan($businessId)
    {
        $today = Carbon::now();
        $subscription = Subscription::where('subscription_end', '>', $today)
            ->where('subscription_start', '<=', $today)
            ->whereBusinessId($businessId)
            ->first();

        return $subscription->plan()->with(['features'])->first();
    }

    private function filterDetails($plan, $details)
    {
        $features = $plan->features;

        $businessDetails = [];
        foreach ($features as $feature) {
            $featureAttribute = $details->getConstant($feature->constant);
            if (is_array($featureAttribute)) {
                foreach ($featureAttribute as $attribute) {
                    $businessDetails[$attribute] = $details->$attribute;
                }
                $businessDetails[str_replace('-', '_', $feature->slug)] = true;
                continue;
            }
            $businessDetails[$featureAttribute] = $details->$featureAttribute;
        }
        DashboardSat::dashboardsat()->setHitsToday($details->plan_id);
        $businessDetails['plan_id'] = $details->plan_id;

        return $businessDetails;
    }

    public function getFeatured()
    {
        $today = Carbon::now();
        $plans = Module::whereName(Module::FEATURED)->first()->plans;
        $handshakePlans = Module::whereName(Module::BUSINESS_RELATIONS)
            ->first()
            ->plans
            ->map(function ($value) {
                return $value->id;
            })
            ->toArray();

        $businesses = Business::all()->keyBy('id');
        $activeSubscriptions = Subscription::where('subscription_end', '>', $today)->get();

        $featuredBusinesses = collect();
        foreach ($plans as $plan) {
            $featuredSubs = $activeSubscriptions->whereLoose('plan_id', $plan->id);
            foreach ($featuredSubs as $subscription) {
                $subBusiness = $businesses->get($subscription->business_id);
                if ($subBusiness->active == 1) {
                    $featuredBusinesses->push($subBusiness);
                }
            }
        }

        $featuredBusinesses = $featuredBusinesses->each(function ($business) use ($handshakePlans) {
            $business->handshake = false;

            if (in_array($business->plan_id, $handshakePlans)) {
                $business->handshake = true;
            }
        });

        return response()->json([
            'businesses' => $featuredBusinesses->unique()->sortBy('name',SORT_DESC)->flatten(),
            'fields' => FieldOfWork::where('featured',1)->get()->chunk(12)
        ]);
    }
}
