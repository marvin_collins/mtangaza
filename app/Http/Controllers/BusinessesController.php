<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateBusinessRequest;
use App\Http\Requests\EditBusinessRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Intervention\Image\Facades\Image;
use SmoDav\Managers\Artist;
use SmoDav\Models\Business;
use SmoDav\Models\BusinessCity;
use SmoDav\Models\BusinessesFieldOfWork;
use SmoDav\Models\BusinessImage;
use SmoDav\Models\Category;
use SmoDav\Models\City;
use SmoDav\Models\Client;
use SmoDav\Models\FieldOfWork;
use SmoDav\Models\Payment;
use SmoDav\Models\PendingChange;
use SmoDav\Models\Plan;

class BusinessesController extends Controller
{
    /**
     * @var Business
     */
    private $business;

    /**
     * BusinessesController constructor.
     *
     * @param Business $business
     */
    public function __construct(Business $business)
    {
        $this->business = $business;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $businesses = $this->business->with(['client', 'category'])->get();
        $pending = PendingChange::with(['client', 'category'])->get();

        return view('admin.businesses.index')
            ->withPending($pending)
            ->withBusinesses($businesses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.businesses.create')
            ->withClients(Client::all())
            ->withCategories(Category::all())
            ->withFields(FieldOfWork::all())
            ->withPlans(Plan::all())
            ->withCities(City::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateBusinessRequest $request
     * @param Artist                 $artist
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateBusinessRequest $request)
    {
        $data = $request->all();
        $data['name'] = ucwords($data['name']);
        $data['logo'] = $this->createLogo($request);
        $data['active'] = 0;
        $data['slug'] = makeSlug($data['name'], 'slug', $this->business);
        $business = $this->business->create($data);
        $this->getImages($request, $business);
        $this->mapFieldsOfWork($data['field_of_work'], $business->id);
        $this->mapCities($data['city_id'], $business->id);
        flash('Successfully created new business', 'success');

        return redirect()->route('manage.businesses.index');
    }

    private function createLogo(Request $request, $business = null)
    {
        $logo = $this->getCurrentLogo($business);
        if ($request->file('logo') && $request->file('logo')->isValid()) {
            $logo = (new Artist())->useFile($request->file('logo'))->createThumbnail()->save();
            if ($business && $business->logo != 'images/default-logo.png') {
                unlink(public_path($business->logo));
            }
        }

        return $logo;
    }

    private function getImages(Request $request, $business)
    {
        if ($request->file('images') && $request->file('images')[0]) {
            $images = array_slice($request->file('images'), 0, 5, true);
            $toInsert = [];
            $now = Carbon::now();
            foreach ($images as $image) {
                $toInsert [] = [
                    'business_id' => $business->id,
                    'image' => (new Artist())->useFile($image)->createImageAndThumbnail()->save(),
                    'position' => 'products',
                    'created_at' => $now,
                    'updated_at' => $now
                ];
            }
            BusinessImage::whereBusinessId($business->id)->delete();
            BusinessImage::insert($toInsert);

            return $toInsert;
        }

        return false;
    }

    private function getCurrentLogo($business = null)
    {
        $logo = 'images/default-logo.png';
        if ($business) {
            $logo = $business->logo;
        }

        return $logo;
    }

    private function mapFieldsOfWork($fieldsOfWork, $businessId)
    {
        $fields = [];
        $timestamp = Carbon::now();
        foreach ($fieldsOfWork as $field) {
            $fields [] = [
                'business_id' => $businessId,
                'field_of_work_id' => $field,
                'created_at' => $timestamp,
                'updated_at' => $timestamp
            ];
        }

        BusinessesFieldOfWork::insert($fields);
    }

    private function mapCities($cities, $businessId)
    {
        $timestamp = Carbon::now();
        $fields = [];
        foreach ($cities as $city) {
            $fields [] = [
                'business_id' => $businessId,
                'city_id' => $city,
                'created_at' => $timestamp,
                'updated_at' => $timestamp
            ];
        }
        BusinessCity::insert($fields);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $businessId
     * @return \Illuminate\Http\Response
     */
    public function show($businessId)
    {
        $business = $this->business->with(['client','subscriptions.plan', 'category', 'fieldsOfWork', 'cities', 'images'])
            ->findOrFail($businessId);

        return view('admin.businesses.show')
            ->withBusiness($business);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $b#/usinessId
     * @return \Illuminate\Http\Response
     */
    public function edit($businessId)
    {
//        $business = $this->business->with(['client','subscriptions.plan', 'category', 'fieldsOfWork', 'cities', 'images'])
//            ->findOrFail($businessId);
        $business = $this->business->with(['client', 'category', 'fieldsOfWork', 'cities'])->findOrFail($businessId);
        $fields = $business->fieldsOfWork;
        $cities = $business->cities;
        $business->fieldsOfWork = $fields->each(function ($item, $key) use ($fields) {
            $fields[$key] = $item->id;
        })->toArray();
        $business->cities = $cities->each(function ($item, $key) use ($cities) {
            $cities[$key] = $item->id;
        })->toArray();

        return view('admin.businesses.edit')
            ->withBusiness($business)
            ->withClients(Client::all())
            ->withCategories(Category::all())
            ->withFields(FieldOfWork::all())
            ->withPlans(Plan::all())
            ->withCities(City::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  EditBusinessRequest  $request
     * @param  int  $businessId
     * @return \Illuminate\Http\Response
     */
    public function update(EditBusinessRequest $request, $businessId)
    {
        $message = 'Successfully edited business details';
        $business = $this->business
            ->with(['client', 'category', 'fieldsOfWork', 'cities', 'subscriptions'])
            ->findOrFail($businessId);
        $data = $request->all();
//        if ($request->plan_id != $business->plan_id)
//        {
//            Payment::where('business_id',$businessId)->first()
//                ->update(['status'=>Payment::STATUS_PENDING]);
//            dd(Payment::where('business_id',$businessId)->first());
//
//            $message = 'You changed your business plan wait for approval';
//            $data['plan_id'] = $request->plan_id;
//            $data['active'] = 0;
//            $data['status'] = 0;
//        }
        $data['name'] = ucwords($data['name']);
        $data['logo'] = $this->createLogo($request, $business);
        $data['slug'] = makeSlug($data['name'], 'slug', $this->business, true);
        $business->fill($data);
        $business->save();
        $this->getImages($request, $business);
        $this->updateFieldsOfWork($business->fieldsOfWork, $business->id, $data['field_of_work']);
        $this->updateCities($business->cities, $business->id, $data['city_id']);
//        dd(Business::find($businessId)->plan_id);
        flash($message, 'success');

        return redirect()->route('manage.businesses.index');
    }

    private function updateFieldsOfWork($fields, $businessId, $selectedFields)
    {
        $businessFields = new BusinessesFieldOfWork();

        $businessFields_id = [];
        foreach ($fields as $field){
            $businessFields_id [] = $field->id;
        }

       BusinessesFieldOfWork::where('business_id',$businessId)->whereIn('field_of_work_id',$businessFields_id)
            ->delete();

            $fields_to_insert = [];
            $timestamp = Carbon::now();
            foreach ($selectedFields as $field) {
                $fields_to_insert [] = [
                    'business_id' => $businessId,
                    'field_of_work_id' => $field,
                    'created_at' => $timestamp,
                    'updated_at' => $timestamp
                ];
            }

            $businessFields->insert($fields_to_insert);
    }

    private function updateCities($cities, $businessId, $selectedCities)
    {
        $businessCities = new BusinessCity();
        $cities = $cities->each(function ($item, $key) use ($cities) {
            $cities[$key] = $item->id;
        });
        $toInsert = collect($selectedCities)->diff($cities);
        $toRemove = collect($cities)->diff($selectedCities);

        if (count($toInsert) > 0) {
            $fields = [];
            $timestamp = Carbon::now();
            foreach ($toInsert as $field) {
                $fields [] = [
                    'business_id' => $businessId,
                    'city_id' => $field,
                    'created_at' => $timestamp,
                    'updated_at' => $timestamp
                ];
            }
            $businessCities->insert($fields);
        }
        if (count($toRemove) > 0) {
            foreach ($toRemove as $field) {
                if ($field == $toRemove->first()) {
                    $businessCities = $businessCities->whereId($fields);
                    continue;
                }
                $businessCities = $businessCities->orWhereId($fields);
            }
            $businessCities->delete();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->business->whereId($id)->delete();
        flash('Successfully deleted business account', 'success');

        return redirect()->route('manage.businesses.index');
    }

    public function state($userId)
    {
        $business = $this->business->findOrFail($userId);
        $performedAction = $business->active == 1 ? 'deactivated' : 'activated';
        $business->active == 1 ? $business->active = 0 : $business->active = 1;
        $business->save();
        flash('Successfully ' . $performedAction . ' business account', 'success');

        return redirect()->route('manage.businesses.index');
    }
}
