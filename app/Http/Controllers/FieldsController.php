<?php

namespace App\Http\Controllers;

use App\Http\Requests\Fields\CreateFieldRequest;
use App\Http\Requests\Fields\DeleteFieldRequest;
use App\Http\Requests\Fields\EditFieldRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Image;
use Illuminate\Support\Facades\Input;
use SmoDav\Models\FieldOfWork;

class FieldsController extends Controller
{
    /**
     * @var FieldOfWork
     */
    private $field;

    /**
     * FieldsController constructor.
     *
     * @param FieldOfWork $field
     */
    public function __construct(FieldOfWork $field)
    {
        $this->field = $field;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('admin.fields.index')
            ->withFields($this->field->all()->sortBy('name'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.fields.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateFieldRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateFieldRequest $request)
    {
        $data = $request->all();
        $data['slug'] = makeSlug($data['name'], 'name', $this->field);
//        $data['featured'] = $request->has('featured') ? '1':'0';
        $logo = Input::file('image');

        $url = 'null';
        if($request->has('featured') && empty($logo))
        {
            flash('Add image', 'error');
            return redirect()->route('manage.fields.index');
        }
        if(!empty($logo))
        {
            if($logo->getSize() == false)
            {
                flash('The image you are uploading has fault', 'error');
                return redirect()->route('manage.fields.index');
            }
            $url = self::createImage($logo);
        }
        $data['image_path'] = $url;
        $this->field->create(
            [
                'name' => $request->name,
                'slug' => makeSlug($data['name'], 'name', $this->field),
                'featured' => $request->has('featured') ? '1':'0',
                'image_path' => $url,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        );

        if($request->has('ajax'))
        {
            return FieldOfWork::where('name',$request->name)->first();
        }
        flash('Successfully created new field', 'success');

        return redirect()->route('manage.fields.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $fieldId
     * @return \Illuminate\Http\Response
     */
    public function edit($fieldId)
    {
        $field = $this->field->findOrFail($fieldId);
        return view('admin.fields.edit')->withField($field);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  EditFieldRequest  $request
     * @param  int  $fieldId
     * @return \Illuminate\Http\Response
     */
    public function update(EditFieldRequest $request, $fieldId)
    {
        $field = $this->field->findOrFail($fieldId);
        $logo = Input::file('image_path');
        $url = 'null';
        if($request->has('featured') && empty($logo))
        {
            return redirect()->route('manage.fields.index');
        }
        if(empty($logo) && $request->featured == 'on' )
        {
            flash('Add image', 'error');
            $url = $field->image_path;
        }
        if(!empty($logo))
        {
            $url = self::createImage($logo);
        }

        FieldOfWork::where('id',$fieldId)
            ->update([
                'name' => $request->name,
                'image_path' => $url,
                'featured' => $request->has('featured') ? '1':'0',
                'slug' => makeSlug($request->name, 'name', $this->field, true),
            ]);
        flash('Successfully edited field', 'success');

        return redirect()->route('manage.fields.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $fieldId
     * @param DeleteFieldRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($fieldId, DeleteFieldRequest $request)
    {
        $field = $this->field->findOrFail($fieldId);

        // TODO: check for mapped categories
        $field->delete();
        flash('Successfully deleted field', 'success');

        return redirect()->route('manage.fields.index');
    }

    private function createImage($logo)
    {
        $document_name = str_random(10) . '.';
        $destination = public_path('images/fieldofwork/thumbnail');
        $img = Image::make($logo->getRealPath());
        $img->resize(50,50, function ($constraint){
            $constraint->aspectRatio();
        });
        $img->resizeCanvas(50, 50, 'center', false, array(255, 255, 255, 0))
            ->save($destination.'/'.$document_name.'png');
        $logo->move('images/fieldofwork/', $document_name. $logo->getClientOriginalExtension());
        $url = 'images/fieldofwork/thumbnail/'.$document_name.'png';
        return $url;
    }
}
