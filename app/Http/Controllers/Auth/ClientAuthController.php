<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;

use App\Http\Requests;
use SmoDav\Models\Auth\Role;

class ClientAuthController extends Controller
{
    const CLIENT_URL = 'client';

    public function getLogin()
    {
        return view('client.pages.login');
    }

    public function postLogin(LoginRequest $request)
    {
        $credentials = $request->only(['email', 'password']);
        try {
            $user = Sentinel::authenticate($credentials, $request->has('remember'));
        } catch (\Exception $ex) {
            return redirect()->back()->withErrors($ex->getMessage());
        }

        if (! $user) {
            flash('Invalid credentials! Please try again', 'error');
            return redirect()->back();
        }

        if (! $user->active) {
            Sentinel::logout(null, true);
            flash('Sorry, your account has been disabled.', 'error');

            return redirect()->back();
        }
        $client = $user->client()->first();
        if (! $client) {
            $client = $user->subClient()->first();
            if (! $client) {
                Sentinel::logout(null, true);
                flash('Sorry, your account is not allowed to access this section.', 'error');

                return redirect()->back();
            }
            $client->id = $client->client_id;
        }
        session(['client_id' => $client->id]);
        flash('Successfully signed in, welcome', 'success');

        return redirect()->route('client.home');
    }

    public function logout()
    {
        Sentinel::logout(null, true);
        session()->flush();
        flash('Successfully signed out.', 'info');

        return redirect()->route('client.login.index');
    }

    public function edit()
    {
        return view('client.pages.password');
    }

    public function update(Request $request)
    {
        $user = Sentinel::getUser();
        $credentials = [
            'email'    => $user->email,
            'password' => $request->get('old_password'),
        ];
        if ($request->get('password_confirmation') != $request->get('password')) {
            return redirect()
                ->back()
                ->withErrors(['message' => 'Sorry, your password confirmation does not match.']);
        }

        if ($request->get('old_password') == $request->get('password')) {
            return redirect()
                ->back()
                ->withErrors(['message' => 'Sorry, you have entered the same old and new password.']);
        }

        if (! Sentinel::validateCredentials($user, $credentials)) {
            return redirect()
                ->back()
                ->withErrors(['message' => 'Sorry, the entered old password does not match our records']);
        }

        $credentials = [
            'password' => $request->get('password')
        ];
        Sentinel::update($user, $credentials);
        Sentinel::logout(null, true);
        session()->flush();
        flash('Successfully changed password. Please sign in again to continue.', 'info');

        return redirect()->route('client.login.index');
    }
}
