<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;

use App\Http\Requests;
use SmoDav\Models\Business;
use SmoDav\Models\Followers;

class NotificationController extends Controller
{
    protected $notification;
    protected $clientId;

    /**
     * NotificationController constructor.
     * @param $notification
     */
    public function __construct(Followers $followers)
    {
        $this->clientId = session('client_id');
        $this->notification = $followers;
    }

    public function index()
    {
        $client_business = Business::all();
        $notifications = Followers::whereHas('business',
        function($query) {
            $query->where('client_id',$this->clientId);
        })->with('business')->where('status',0)->get()
        ->map(function ($follow) use($client_business)
        {
            return ['id' => $follow->id,
                'business' => $client_business->where('id', $follow->business_id)->first()->name,
                'follower' => $client_business->where('id', $follow->follower_id)->first()->name,
                'date' => $follow->created_at];
        });


        return view('client.notifications.index')
            ->withNotifications($notifications);
    }

    public function update($id)
    {
        $follow = Followers::findorfail($id);
        $follow->update(['status'=>1]);
        flash('Confirmation successful','success');
        return redirect()->back();
    }

    public function decline($id)
    {
        Followers::destroy($id);
        flash('Declined successful','success');
        return redirect()->back();
    }
}
