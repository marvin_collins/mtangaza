<?php

namespace App\Http\Controllers;

use App\Http\Requests\Client\CreatePaymentRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use SmoDav\Models\Business;
use SmoDav\Models\Payment;
use SmoDav\Models\Plan;
use SmoDav\Models\Subscription;

class PaymentsController extends Controller
{
    private $payment;

    /**
     * BusinessesController constructor.
     *
     * @param Payment $payment
     */
    public function __construct(Payment $payment)
    {
        $this->payment = $payment;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $payments = $this->payment->with(['business'])->get();
//        dd($payments);

        return view('admin.payments.index')->withPayments($payments);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $businesses = Business::all();

        return view('admin.payments.create')
            ->withPlans(Plan::all())
            ->withBusinesses($businesses);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['transaction_number'] = strtoupper($data['transaction_number']);
        $data['client_id'] = Business::with(['client'])->findOrFail($data['business_id'])->client->id;
        $data['paybill_number'] = env('PAYBILL_ACCOUNT');
        $data['amount_paid'] = Plan::findOrFail($data['plan_id'])->cost;
        $data['status'] = Payment::STATUS_PENDING;
        Payment::create($data);
        flash('Successfully posted payment.', 'success');

        return redirect()->route('manage.payments.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $paymentId
     * @return \Illuminate\Http\Response
     */
    public function show($paymentId)
    {
        $payment = $this->payment->with(['business'])->findOrFail($paymentId);

        return view('admin.payments.show')->withPayment($payment);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param  int    $paymentId
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $paymentId)
    {
        $payment = $this->payment->with(['business'])->findOrFail($paymentId);
        switch ($request->get('act')) {
            case 'approve':
                $this->approveBusiness($payment);
                break;
            case 'disapprove':
                $payment->status = Payment::STATUS_DISAPPROVED;
                $payment->save();
                flash('Successfully disapproved payment.', 'success');
                break;
            default:
                abort(404);
        }

        return redirect()->route('manage.payments.index');
    }

    private function approveBusiness($payment)
    {
        $payment->status = Payment::STATUS_APPROVED;
        $business = $payment->business;
        $business->status = Business::STATUS_APPROVED;
        $business->active = 1;
        $today = Carbon::now();
        $currentSubscription = Carbon::parse($business->subscription_end);
        if ($payment->upgradetype == 'immediate'){
            $startDay = $today->addMinutes(5);
            $endDay = $today->addYear(1);
        }
        else{
            $latestDay = $currentSubscription->lt($today) ? $today : $currentSubscription;
            $startDay = Carbon::parse($latestDay);
            $endDay = Carbon::parse($latestDay)->addYear(1);
        }

        $subscription = $business->subscriptions()->create([
            'plan_id' => $payment->plan_id,
            'subscription_start' => $startDay,
            'subscription_end' => $endDay
        ]);

        $business->subscription_end = $subscription->subscription_end;

        $payment->save();
        $business->save();
        flash('Successfully approved and published business.', 'success');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
