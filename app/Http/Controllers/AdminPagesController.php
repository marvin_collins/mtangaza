<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use SmoDav\Commons\DashboardSat;
use SmoDav\Models\Business;
use SmoDav\Models\Followers;
use SmoDav\Models\Plan;

class AdminPagesController extends Controller
{
    public function home(Request $request)
    {
        $data = Business::all();
        $registered_business_today = $data->map(function ($values,$key){
            if(Carbon::parse($values->created_at)->format('d-m-y') == Carbon::parse(Carbon::today())->format('d-m-y'))
                {
                    return $values;
                }
        });
        $today_hits = DashboardSat::dashboardsat()->getHitsTodays();
        $total_hits = DashboardSat::dashboardsat()->getTotalHits();


        $plan = Plan::all();
        $businessess = Business::all();
        $followers = Followers::all();
        $silver_id = $plan->where('name',Plan::SILVER_PACKAGE)->first()->id;
        $gold_id = $plan->where('name',Plan::GOLD_PACKAGE)->first()->id;
        $platinum_id = $plan->where('name',Plan::PLATINUM_PACKAGE)->first()->id;
        $silver_count = count($data->where('plan_id',$silver_id));
        $gold_count = count($data->where('plan_id',$gold_id));
        $platinum_count = count($data->where('plan_id',$platinum_id));
        $registered = count($data->where('active',1)->where('status',1));
        $not_registered = count($data->where('active',0)->where('status',0));
        return view('admin.pages.dashboard')
            ->withRegistered($registered)
            ->withSilver($silver_count)
            ->withGold($gold_count)
            ->withTodaybusinesses(DashboardSat::dashboardsat()->getRegisteredTodayBusiness($businessess))
            ->withMonthbusinesses(DashboardSat::dashboardsat()->getRegisteredMonthBusiness($businessess))
            ->withYearbusinesses(DashboardSat::dashboardsat()->getRegisteredYearBusiness($businessess))
            ->withAllbusinesses(count($businessess))
            ->withTodayfollowers(DashboardSat::dashboardsat()->getRegisteredTodayFollowers($followers))
            ->withMonthfollowers(DashboardSat::dashboardsat()->getRegisteredMonthFollowers($followers))
            ->withYearfollowers(DashboardSat::dashboardsat()->getRegisteredYearFollowers($followers))
            ->withAllfollowers(count($followers))
            ->withTodayhits($today_hits)
            ->withTotalhits($total_hits)
            ->withPlatinum($platinum_count)
            ->withNotregistered($not_registered);
    }
}
