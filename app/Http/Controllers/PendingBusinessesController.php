<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use SmoDav\Models\Business;
use SmoDav\Models\BusinessImage;
use SmoDav\Models\PendingChange;
use SmoDav\Models\PendingImageChange;

class PendingBusinessesController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $business = PendingChange::with(['client', 'category', 'fieldsOfWork', 'cities', 'images'])
            ->findOrFail($id);

        return view('admin.businesses.show')
            ->withPending(true)
            ->withBusiness($business);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param  int    $businessId
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $businessId)
    {
        if (! $request->has('act')) {
            abort(404);
            return 'false';
        }

        switch ($request->get('act')) {
            case 'approve':
                return $this->approve($businessId);
                break;
            case 'disapprove':
                return $this->disapprove($businessId);
                break;
            default:
                abort(404);
                return 'false';
                break;
        }
    }

    private function approve($businessId)
    {
        $pending = PendingChange::findOrFail($businessId);
        $business = Business::findOrFail($businessId);
        $business->fill($pending->toArray());
        $business->save();
        $images = PendingImageChange::whereBusinessId($businessId)->get()->toArray();
//        dd($images);
        if (count($images) > 0) {
            BusinessImage::whereBusinessId($businessId)->delete();
            BusinessImage::insert($images);
        }
        $pending->delete();
        PendingImageChange::whereBusinessId($businessId)->delete();
        flash('Successfully published changes', 'success');

        return redirect()->route('manage.businesses.index');
    }

    private function disapprove($businessId)
    {
        PendingChange::whereId($businessId)->delete();
        PendingImageChange::whereBusinessId($businessId)->delete();
        flash('Successfully disapproved changes', 'success');

        return redirect()->route('manage.businesses.index');
    }

}
