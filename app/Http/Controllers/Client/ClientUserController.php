<?php

namespace App\Http\Controllers\Client;

use App\Http\Requests\Client\CreateSubClientRequest;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use SmoDav\Managers\Registrar;
use SmoDav\Models\Auth\Role;
use SmoDav\Models\Auth\User;
use SmoDav\Models\SubClient;

class ClientUserController extends Controller
{
    /**
     * @var SubClient
     */
    private $client;
    private $client_id;

    /**
     * ClientUserController constructor.
     *
     * @param SubClient $client
     */
    public function __construct(SubClient $client)
    {
        $this->client_id = session('client_id');
        $this->client = $client;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = $this->client->with(['user'])->whereClientId($this->client_id)->get();
        return view('client.users.index')->withClients($clients);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $nationalities = array(
            'Afghan',
            'Albanian',
            'Algerian',
            'American',
            'Andorran',
            'Angolan',
            'Antiguans',
            'Argentinean',
            'Armenian',
            'Australian',
            'Austrian',
            'Azerbaijani',
            'Bahamian',
            'Bahraini',
            'Bangladeshi',
            'Barbadian',
            'Barbudans',
            'Batswana',
            'Belarusian',
            'Belgian',
            'Belizean',
            'Beninese',
            'Bhutanese',
            'Bolivian',
            'Bosnian',
            'Brazilian',
            'British',
            'Bruneian',
            'Bulgarian',
            'Burkinabe',
            'Burmese',
            'Burundian',
            'Cambodian',
            'Cameroonian',
            'Canadian',
            'Cape Verdean',
            'Central African',
            'Chadian',
            'Chilean',
            'Chinese',
            'Colombian',
            'Comoran',
            'Congolese',
            'Costa Rican',
            'Croatian',
            'Cuban',
            'Cypriot',
            'Czech',
            'Danish',
            'Djibouti',
            'Dominican',
            'Dutch',
            'East Timorese',
            'Ecuadorean',
            'Egyptian',
            'Emirian',
            'Equatorial Guinean',
            'Eritrean',
            'Estonian',
            'Ethiopian',
            'Fijian',
            'Filipino',
            'Finnish',
            'French',
            'Gabonese',
            'Gambian',
            'Georgian',
            'German',
            'Ghanaian',
            'Greek',
            'Grenadian',
            'Guatemalan',
            'Guinea-Bissauan',
            'Guinean',
            'Guyanese',
            'Haitian',
            'Herzegovinian',
            'Honduran',
            'Hungarian',
            'I-Kiribati',
            'Icelander',
            'Indian',
            'Indonesian',
            'Iranian',
            'Iraqi',
            'Irish',
            'Israeli',
            'Italian',
            'Ivorian',
            'Jamaican',
            'Japanese',
            'Jordanian',
            'Kazakhstani',
            'Kenyan',
            'Kittian and Nevisian',
            'Kuwaiti',
            'Kyrgyz',
            'Laotian',
            'Latvian',
            'Lebanese',
            'Liberian',
            'Libyan',
            'Liechtensteiner',
            'Lithuanian',
            'Luxembourger',
            'Macedonian',
            'Malagasy',
            'Malawian',
            'Malaysian',
            'Maldivan',
            'Malian',
            'Maltese',
            'Marshallese',
            'Mauritanian',
            'Mauritian',
            'Mexican',
            'Micronesian',
            'Moldovan',
            'Monacan',
            'Mongolian',
            'Moroccan',
            'Mosotho',
            'Motswana',
            'Mozambican',
            'Namibian',
            'Nauruan',
            'Nepalese',
            'New Zealander',
            'Nicaraguan',
            'Nigerian',
            'Nigerien',
            'Northconst Korean',
            'Northern Irish',
            'Norwegian',
            'Omani',
            'Pakistani',
            'Palauan',
            'Panamanian',
            'Papua New Guinean',
            'Paraguayan',
            'Peruvian',
            'Polish',
            'Portuguese',
            'Qatari',
            'Romanian',
            'Russian',
            'Rwandan',
            'Saint Lucian',
            'Salvadoran',
            'Samoan',
            'San Marinese',
            'Sao Tomean',
            'Saudi',
            'Scottish',
            'Senegalese',
            'Serbian',
            'Seychellois',
            'Sierra Leonean',
            'Singaporean',
            'Slovakian',
            'Slovenian',
            'Solomon Islander',
            'Somali',
            'South African',
            'South Korean',
            'Spanish',
            'Sri Lankan',
            'Sudanese',
            'Surinamer',
            'Swazi',
            'Swedish',
            'Swiss',
            'Syrian',
            'Taiwanese',
            'Tajik',
            'Tanzanian',
            'Thai',
            'Togolese',
            'Tongan',
            'Trinidadian/Tobagonian',
            'Tunisian',
            'Turkish',
            'Tuvaluan',
            'Ugandan',
            'Ukrainian',
            'Uruguayan',
            'Uzbekistani',
            'Venezuelan',
            'Vietnamese',
            'Welsh',
            'Yemenite',
            'Zambian',
            'Zimbabwean'
        );
        return view('client.users.create')->with('nationalities',$nationalities);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateSubClientRequest $request
     * @param Registrar               $registrar
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateSubClientRequest $request, Registrar $registrar)
    {
        $formData = $request->all();
        $formData['client_id'] = $this->client_id;
        $registrar
            ->registerAndActivate($formData['first_name'], $formData['last_name'], $formData['email'])
            ->assignRole(Role::ACCOUNT_OWNER);

        $formData['user_id'] = $registrar->getUser()->id;
        $this->client->create($formData);

        session()->flash('client_email', $formData['email']);
        session()->flash('client_password', $registrar->getUserPassword());
        flash('Successfully created new authenticated user account', 'success');

        return redirect()->route('client.users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client = $this->client->findOrFail($id);

        return view('client.users.edit')->withClient($client);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CreateSubClientRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateSubClientRequest $request, $id)
    {
        $client = $this->client->findOrFail($id);
        $client->fill($request->all());
        $client->save();
        $user = $client->user;
        $user->fill($request->only(['email']));
        $user->save();
        flash('Successfully edited authorised user account', 'success');

        return redirect()->route('client.users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = $this->client->findOrFail($id);
        $client->user()->delete();
        $client->delete();
        flash('Successfully deleted authorised user account', 'success');

        return redirect()->route('client.users.index');
    }

    public function state($userId)
    {
        $user = User::findOrFail($userId);
        $performedAction = $user->active == 1 ? 'deactivated' : 'activated';
        $user->active == 1 ? $user->active = 0 : $user->active = 1;
        $user->save();
        flash('Successfully ' . $performedAction . ' authorised user account', 'success');

        return redirect()->route('client.users.index');
    }
}
