<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use SmoDav\Models\Business;
use SmoDav\Models\Inquiry;

class InquiryController extends Controller
{
    /**
     * @var Inquiry
     */
    private $inquiry;
    private $clientId;

    /**
     * InquiryController constructor.
     *
     * @param Inquiry $inquiry
     */
    public function __construct(Inquiry $inquiry)
    {
        $this->inquiry = $inquiry;
        $this->clientId = session('client_id');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $businesses = Business::whereClientId($this->clientId)->get();

        return view('client.inquiries.businessIndex')->withBusinesses($businesses);
        return view('client.inquiries.index')
            ->withInquiries($this->inquiry->whereClientId($this->clientId));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $inquiryId
     * @return \Illuminate\Http\Response
     */
    public function show($inquiryId)
    {
        $inquiry = $this->inquiry->findOrFail($inquiryId);
        if ($inquiry->status == 0) {
            $inquiry->status = 1;
            $inquiry->save();
        }

        return view('client.inquiries.show')->withInquiry($inquiry);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $inquiryId
     * @return \Illuminate\Http\Response
     */
    public function destroy($inquiryId)
    {
        $this->inquiry->findOrFail($inquiryId)->delete();
        flash('Successfully deleted inquiry.', 'success');

        return redirect()->back();
    }

    public function business($businessId)
    {
        $inquiries = $this->inquiry
            ->where('business_id', $businessId)
            ->get();

        return view('client.inquiries.index')
            ->withBusinessId($businessId)
            ->withInquiries($inquiries);
    }

}
