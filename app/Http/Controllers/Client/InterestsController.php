<?php

namespace App\Http\Controllers\Client;

use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use SmoDav\Models\FieldOfWork;
use SmoDav\Models\Interest;

class InterestsController extends Controller
{
    /**
     * @var Interest
     */
    private $interest;
    private $clientId;

    /**
     * InterestsController constructor.
     *
     * @param Interest $interest
     */
    public function __construct(Interest $interest)
    {
        $this->interest = $interest;
        $this->clientId = session('client_id');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('client.interests.index')
            ->withInterests(
                $this->interest->with(['field'])->whereClientId($this->clientId)->get()
            );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('client.interests.create')->withFields(FieldOfWork::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $now = Carbon::now();
        $rows = [];
        $interest = $request->get('interest');
        if ($request->has('field_of_work_id')) {
            $fields = $request->get('field_of_work_id');
            foreach ($fields as $field) {
                $rows [] = [
                    'client_id' => $this->clientId,
                    'field_of_work_id' => $field,
                    'created_at' => $now,
                    'updated_at' => $now
                ];
            }
        }

        if ($interest != '') {
            $createRows = collect();
            $insertFields = collect(explode(';', $interest));
            foreach ($insertFields as $field) {
                $createRows->push([
                    'name' => $field,
                    'slug' => str_slug($field),
                    'created_at' => $now,
                    'updated_at' => $now
                ]);
            }

            FieldOfWork::insert($createRows->unique()->toArray());

            $inserted = FieldOfWork::whereName($insertFields[0]);
            for ($i = 1; $i < count($insertFields); $i++) {
                $inserted = $inserted->orWhere('name', $insertFields[$i]);
            }
            $inserted = $inserted->get();

            foreach ($inserted as $field) {
                $rows [] = [
                    'client_id' => $this->clientId,
                    'field_of_work_id' => $field->id,
                    'created_at' => $now,
                    'updated_at' => $now
                ];
            }
        }
        Interest::insert($rows);

        if ($request->has('ajax'))
        {
            $interest = FieldOfWork::where('name',$request->interest)->first();
            return $interest;
        }
        flash('Successfully created new interests', 'success');
        
        return redirect()->route('client.interests.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->interest->findOrFail($id)->delete();
        flash('Successfully deleted interest', 'success');
        
        return redirect()->route('client.interests.index');
    }
}
