<?php

namespace App\Http\Controllers\Client;

use App\Http\Requests\Client\CreateBusinessRequest;
use App\Http\Requests\Client\UpdateBusinessRequest;
use Carbon\Carbon;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use SmoDav\Managers\Artist;
use SmoDav\Models\Business;
use SmoDav\Models\BusinessCity;
use SmoDav\Models\BusinessesFieldOfWork;
use SmoDav\Models\BusinessImage;
use SmoDav\Models\Category;
use SmoDav\Models\City;
use SmoDav\Models\Client;
use SmoDav\Models\FieldOfWork;
use SmoDav\Models\Payment;
use SmoDav\Models\PendingChange;
use SmoDav\Models\PendingImageChange;
use SmoDav\Models\Plan;

class ClientBusinessesController extends Controller
{
    /**
     * @var Business
     */
    private $business;
    private $clientId;

    /**
     * BusinessesController constructor.
     *
     * @param Business $business
     */
    public function __construct(Business $business)
    {
        $this->business = $business;
        $this->clientId = session('client_id');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $businesses = $this->business->where('client_id', $this->clientId)->with(['category'])->get();

        return view('client.businesses.index')
            ->withBusinesses($businesses);
    }

    /**
     * w the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('client.businesses.create')
            ->withCategories(Category::all())
            ->withFields(FieldOfWork::all())
            ->withPlans(Plan::all())
            ->withCities(City::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateBusinessRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateBusinessRequest $request)
    {
        $data = $request->all();
        if (substr($request->website,0,7) === "http://")
        {
            $data['website'] = $request->website;
        }
        elseif (substr($request->website,0,4) === "www.")
        {
            $data['website'] = 'http://'.substr($request->website,4);
        }
        elseif (substr($request->website,0,8) === "https://"){
           $data['website'] = $request->website;
        }
        else{
            $data['website'] = 'http://'.$request->website;
        }
        $data['name'] = ucwords($data['name']);
        $data['client_id'] = $this->clientId;
        $data['logo'] = $this->getLogo($request);
        $data['active'] = 0;
        $data['status'] = Business::STATUS_PENDING;
        $data['slug'] = makeSlug($data['name'], 'slug', $this->business);
        $business = $this->business->create($data);
        $this->getImages($request, $business);
        $this->mapFieldsOfWork($data['field_of_work'], $business->id);
        $this->mapCities($data['city_id'], $business->id);

        Payment::create([
            'business_id' => $business->id,
            'plan_id' => $data['plan_id'],
            'client_id' => $this->clientId,
            'paybill_number' => env('PAYBILL_ACCOUNT'),
            'account_number' => $data['account_number'],
            'amount_paid' => Plan::findOrFail($data['plan_id'])->cost,
            'transaction_number' => $data['transaction_number'],
            'status' => Payment::STATUS_PENDING,
        ]);

        flash('Successfully created new business. Please wait for approval.', 'success');

        return redirect()->route('client.businesses.index');
    }

    private function getLogo(Request $request, $business = null)
    {
        $now = Carbon::now()->timestamp;
        $logo = 'images/default-logo.png';
        if ($business) {
            $logo = $business->logo;
        }

        if ($request->file('logo') && $request->file('logo')->isValid()) {
            $filename = str_random(10) . $now . '.' . $request->file('logo')->getClientOriginalExtension();
            $uploaded = Image::make($request->file('logo'))
                ->resize(280, 280, function ($constraint) {
                    $constraint->aspectRatio();
                });
            $image = Image::canvas(300, 300)
                ->insert($uploaded, 'center');

            $image->save(public_path('images/logos/' . $filename));
            $logo = 'images/logos/' . $filename;

            if ($business && $business->logo != 'images/default-logo.png') {
                unlink(public_path($business->logo));
            }
        }

        return $logo;
    }

    private function mapFieldsOfWork($fieldsOfWork, $businessId)
    {
        $fields = [];
        $timestamp = Carbon::now();
        foreach ($fieldsOfWork as $field) {
            $fields [] = [
                'business_id' => $businessId,
                'field_of_work_id' => $field,
                'created_at' => $timestamp,
                'updated_at' => $timestamp
            ];
        }

        BusinessesFieldOfWork::insert($fields);
    }

    private function mapCities($cities, $businessId)
    {
        $timestamp = Carbon::now();
        $fields = [];
        foreach ($cities as $city) {
            $fields [] = [
                'business_id' => $businessId,
                'city_id' => $city,
                'created_at' => $timestamp,
                'updated_at' => $timestamp
            ];
        }
        BusinessCity::insert($fields);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $businessId
     * @return \Illuminate\Http\Response
     */
    public function show($businessId)
    {
        $business = $this->business->with(['client', 'category','subscriptions.plan', 'fieldsOfWork', 'cities'])
            ->findOrFail($businessId);

        return view('client.businesses.show')
            ->withBusiness($business);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $businessId
     * @return \Illuminate\Http\Response
     */
    public function edit($businessId)
    {
        $business = $this->business->with(['client','subscriptions.plan','images', 'category', 'fieldsOfWork', 'cities'])
            ->findOrFail($businessId);
        $fields = $business->fieldsOfWork;
        $cities = $business->cities;
        $business->fieldsOfWork = $fields->each(function ($item, $key) use ($fields) {
            $fields[$key] = $item->id;
        })->toArray();
        $business->cities = $cities->each(function ($item, $key) use ($cities) {
            $cities[$key] = $item->id;
        })->toArray();

        return view('client.businesses.edit')
            ->withBusiness($business)
            ->withCategories(Category::all())
            ->withFields(FieldOfWork::all())
            ->withPlans(Plan::all())
            ->withCities(City::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateBusinessRequest  $request
     * @param  int  $businessId
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBusinessRequest $request, $businessId)
    {
        $business = $this->business
            ->with(['client', 'category', 'fieldsOfWork', 'cities', 'subscriptions'])
            ->findOrFail($businessId);
        $data = $request->all();

        if (substr($request->website,0,7) === "http://")
        {
            $data['website'] = $request->website;
        }
        elseif (substr($request->website,0,4) === "www.")
        {
            $data['website'] = 'http://'.substr($request->website,4);
        }
        elseif (substr($request->website,0,8) === "https://"){
            $data['website'] = $request->website;
        }
        elseif(empty($request->website))
        {
            $data['website'] = '';
        }
        else{
            $data['website'] = 'http://'.$request->website;
        }

        $data['name'] = ucwords($data['name']);
        $data['logo'] = $this->getLogo($request, $business);
        $data['slug'] = makeSlug($data['name'], 'slug', $this->business, true);
        $business->fill($data);
        $pending = PendingChange::find($business->id);
        if (is_null($pending)) {
            $pending = new PendingChange();
        }

        $pending->fill(
            collect($business->toArray())
                ->except(['client', 'category', 'fields_of_work', 'cities', 'subscriptions'])
                ->toArray()
        );
        $pending->save();

        $this->getImages($request, $business, true);
        $this->updateFieldsOfWork($business->fieldsOfWork, $business->id, $data['field_of_work']);
        $this->updateCities($business->cities, $business->id, $data['city_id']);
        flash('Successfully edited business details. Please wait for approval', 'success');

        return redirect()->route('client.businesses.index');
    }

    private function updateFieldsOfWork($fields, $businessId, $selectedFields)
    {
        $businessFields = new BusinessesFieldOfWork();

        $businessFields_id = [];
        foreach ($fields as $field){
            $businessFields_id [] = $field->id;
        }

        BusinessesFieldOfWork::where('business_id',$businessId)->whereIn('field_of_work_id',$businessFields_id)
            ->delete();

        $fields_to_insert = [];
        $timestamp = Carbon::now();
        foreach ($selectedFields as $field) {
            $fields_to_insert [] = [
                'business_id' => $businessId,
                'field_of_work_id' => $field,
                'created_at' => $timestamp,
                'updated_at' => $timestamp
            ];
        }

        $businessFields->insert($fields_to_insert);
    }

    private function updateCities($cities, $businessId, $selectedCities)
    {
        $businessCities = new BusinessCity();
        $cities = $cities->each(function ($item, $key) use ($cities) {
            $cities[$key] = $item->id;
        });
        $toInsert = collect($selectedCities)->diff($cities);
        $toRemove = collect($cities)->diff($selectedCities);

        if (count($toInsert) > 0) {
            $fields = [];
            $timestamp = Carbon::now();
            foreach ($toInsert as $field) {
                $fields [] = [
                    'business_id' => $businessId,
                    'city_id' => $field,
                    'created_at' => $timestamp,
                    'updated_at' => $timestamp
                ];
            }
            $businessCities->insert($fields);
        }
        if (count($toRemove) > 0) {
            foreach ($toRemove as $field) {
                if ($field == $toRemove->first()) {
                    $businessCities = $businessCities->whereId($fields);
                    continue;
                }
                $businessCities = $businessCities->orWhereId($fields);
            }
            $businessCities->delete();
        }
    }

    private function getImages(Request $request, $business, $edit = false)
    {
        if ($request->file('images') && $request->file('images')[0]) {
            $images = array_slice($request->file('images'), 0, 5, true);
            $toInsert = [];
            $now = Carbon::now();
            foreach ($images as $image) {
                $toInsert [] = [
                    'business_id' => $business->id,
                    'image' => (new Artist())->useFile($image)->createImageAndThumbnail()->save(),
                    'position' => 'products',
                    'created_at' => $now,
                    'updated_at' => $now
                ];
            }

            if ($edit) {
                PendingImageChange::whereBusinessId($business->id)->delete();
                PendingImageChange::insert($toInsert);

                return $toInsert;
            }
            BusinessImage::whereBusinessId($business->id)->delete();
            BusinessImage::insert($toInsert);

            return $toInsert;
        }

        return false;
    }
}
