<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests;

class ClientPagesController extends Controller
{
    public function home()
    {
        return view('client.pages.dashboard');
    }
}
