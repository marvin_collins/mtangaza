<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Http\Requests\Client\CreatePaymentRequest;
use Illuminate\Http\Request;

use App\Http\Requests;
use SmoDav\Models\Business;
use SmoDav\Models\Payment;
use SmoDav\Models\Plan;

class ClientPaymentsController extends Controller
{
    private $clientId;
    /**
     * @var Payment
     */
    private $payment;

    /**
     * BusinessesController constructor.
     *
     * @param Payment $payment
     */
    public function __construct(Payment $payment)
    {
        $this->clientId = session('client_id');
        $this->payment = $payment;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $payments = $this->payment
            ->with(['business'])
            ->whereClientId($this->clientId)
            ->get();

        return view('client.payments.index')->withPayments($payments);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $businesses = Business::where('client_id', $this->clientId)->get();

        return view('client.payments.create')
            ->withPlans(Plan::all())
            ->withBusinesses($businesses);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreatePaymentRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePaymentRequest $request)
    {
        $clientBusiness = Business::findorfail($request->business_id);
        $clientBusiness->plan_id = $request->plan_id;
        $clientBusiness->save();

        $data = $request->all();
        $data['client_id'] = $this->clientId;
        $data['transaction_number'] = strtoupper($data['transaction_number']);
        $data['paybill_number'] = env('PAYBILL_ACCOUNT');
        $data['amount_paid'] = Plan::findOrFail($data['plan_id'])->cost;
        $data['status'] = Payment::STATUS_PENDING;
        $data['upgradetype'] = $request->upgradetype;

        Payment::create($data);
        flash('Successfully posted payment. Please wait for approval.', 'success');

        return redirect()->route('client.payments.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $paymentId
     * @return \Illuminate\Http\Response
     */
    public function show($paymentId)
    {
        $payment = $this->payment->with(['business'])->findOrFail($paymentId);

        return view('client.payments.show')->withPayment($payment);
    }
}
