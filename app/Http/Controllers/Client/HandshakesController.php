<?php

namespace App\Http\Controllers\Client;

use App\Events\NewFollowRequest;
use Illuminate\Support\Facades\Event;
use App\Events\NotificationMail;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use SmoDav\Models\Business;
use SmoDav\Models\Followers;
use SmoDav\Models\Module;
use SmoDav\Models\ModulesPlan;
use SmoDav\Models\Plan;
use SmoDav\Models\Subscription;

class HandshakesController extends Controller
{

    private $clientId;
    /**
     * @var Followers
     */
    private $followers;

    /**
     * HandshakesController constructor.
     *
     * @param Followers $followers
     *
     * @internal param Handshake $handshake
     */
    public function __construct(Followers $followers)
    {
//        dd(Followers::all());
        $this->clientId = session('client_id');
        $this->followers = $followers;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $businesses = Business::whereClientId($this->clientId)->get();
        return view('client.handshakes.businessIndex')->withBusinesses($businesses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $businesse = Plan::whereHas('features', function ($query) {
            $query->where('name', Module::BUSINESS_RELATIONS);
        })->with(['subscriptions.business'])->get()->map(function ($plan) {
            return $plan->subscriptions->map(function ($subscription) {
                return $subscription->business;
            });
        })->flatten();
        $businesses = collect();
        foreach ($businesse as $biz)
        {
            if ($biz->status == 1)
            {
                $businesses->push($biz);
            }
        }

        $businessId = $request->get('id');
        $following = Followers::whereFollowerId($businessId)
            ->get(['business_id'])->keyBy('business_id');
        $businesses = $businesses->keyBy('id');
        $businesses = $businesses->reject(function ($value) use ($following, $businessId) {
            return in_array($value->id, $following->keys()->toArray()) || $businessId == $value->id;
        });

        return view('client.handshakes.create')
            ->withBusinessId($businessId)
            ->withBusinesses($businesses);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        if($request->get('bulkbusiness_id')) {
            $arrayItems = array();
            $follows = collect($data['bulkbusiness_id'])->map(function ($items) use($arrayItems, $data){

                $arrayItems['follower_id'] = $data['business_id'];
                $arrayItems['business_id'] = $items;
                $arrayItems['created_at'] = Carbon::now();
                $arrayItems['updated_at'] = Carbon::now();
//                Event::fire(new NotificationMail($data['business_id'],$items));

                return $arrayItems;
            })->toArray();

            Followers::insert($follows);

            $businesses = Business::whereIn('id', array_merge([$request->get('business_id')], $request->get('bulkbusiness_id')))
                ->get()->keyBy('id');
            $follower = $businesses->pull($request->get('business_id'));

            event(new NewFollowRequest($follower, $businesses));

            return redirect()->back();
        }

        Followers::create([
            'follower_id' => $data['business_id'],
            'business_id' => $data['selected']
        ]);

        $businesses = Business::whereIn('id', [$request->get('business_id'), $request->get('selected')])
            ->get()->keyBy('id');
        $followed = $businesses->pull($request->get('business_id'));

        event(new NewFollowRequest($followed, $businesses));


        return json_encode(['status' => 1, 'message' => 'Successfully followed business']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $follower = Followers::findOrFail($id);
        $follower->delete();
        flash('Successfully unfollowed business', 'success');

        return redirect()->route('client.handshakes.business', $follower->follower_id);
    }

    public function business($businessId)
    {
        $businesses = Plan::whereHas('features', function ($query) {
            $query->where('name', Module::BUSINESS_RELATIONS);
        })->with(['subscriptions.business'])->get()->map(function ($plan) {
            return $plan->subscriptions->map(function ($subscription) {
                return $subscription->business->id;
            });
        })->flatten()->toArray();

        $handsake = " ";

        in_array($businessId, $businesses) ? $handsake = "active" : " ";

        Session::put('business_id',$businessId);
        $followers = $this->followers->with(['follower'])->whereBusinessId($businessId)->where('status',1)->get();
        $following = $this->followers->with(['business'])->whereFollowerId($businessId)->where('status',1)->get();

        return view('client.handshakes.index')
            ->withBusinessId($businessId)
            ->withFollowers($followers)
            ->withActive($handsake)
            ->withFollowing($following);
    }
}
