<?php
/**
 * Created by PhpStorm.
 * User: marvin
 * Date: 11/7/16
 * Time: 10:21 AM
 */

namespace App\Listeners;


use App\Events\NewsLetter;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Contracts\Mail\Mailer;
use SmoDav\Models\Client;

class EmailNewsLetter {
    protected $mailer;

    /**
     * NewsLetter constructor.
     * @param $mailer
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function handle(NewsLetter $letter)
    {
        $all_clients = Client::where('newsletter','on')->get();
        $data = $letter->request->toArray();
        foreach ($all_clients as $client)
        {
            $data['firstname'] = $client->first_name;
            $data['last_name'] = $client->last_name;
            $data['email'] = $client->email;
            $this->mailer->send('emails.newsletter',['data'=>$data],function ($message) use($data)
            {
                $message->from('newsletter@mtangaza.com','mTangaza')
                    ->to($data['email'],$data['firstname']. ' ' . $data['last_name']);


                if (!empty($data->files[0]))
                {
                    foreach ($data->files as $file)
                    {
                        $message->attach($file);
                    }

                }

                $message->subject($data['subject']);

            });
        }
    }

}