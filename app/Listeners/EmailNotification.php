<?php
/**
 * Created by PhpStorm.
 * User: marvin
 * Date: 11/8/16
 * Time: 9:01 AM
 */

namespace App\Listeners;


use App\Events\NotificationMail;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailNotification implements ShouldQueue
{
    public $mailer;

    /**
     * EmailNotification constructor.
     * @param $mailer
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function handle(NotificationMail $notificationMail)
    {
        $user = $notificationMail->toArray();

        $user['password'] = $notificationMail->follower;
        $this->mailer->send(
            'emails.welcome',
            ['user' => $user],
            function ($message) use ($user) {
                $message
                    ->from('no-reply@mtangaza.com', 'mTangaza - Welcome')
                    ->to($user['password'], $user['password'] . ' ' . $user['password'])
                    ->subject('Welcome to mTangaza');
            }
        );
    }
}