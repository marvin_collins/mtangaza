<?php

namespace App\Listeners;

use App\Events\NewFollowRequest;
use Illuminate\Mail\Mailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendFollowRequest
{
    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * Create the event listener.
     *
     * @param Mailer $mailer
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  NewFollowRequest  $event
     * @return void
     */
    public function handle(NewFollowRequest $event)
    {
        foreach ($event->followed as $followed) {
            $this->mailer->send('emails.followers', ['follower' => $event->follower], function ($message) use ($followed) {
                    $message
                        ->from('no-reply@mtangaza.com', 'mTangaza')
                        ->to($followed->email, $followed->name)
                        ->subject('New Follower Request');
                }
            );
        }
    }
}
