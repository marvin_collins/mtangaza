<?php

namespace App\Listeners;

use App\Events\sendMail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailFired
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  sendMail  $event
     * @return void
     */
    public function handle(sendMail $event)
    {
        //
    }
}
