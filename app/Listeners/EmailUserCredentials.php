<?php

namespace App\Listeners;

use App\Events\UserHasRegistered;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailUserCredentials implements ShouldQueue
{
    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * Create the event listener.
     *
     * @param Mailer $mailer
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  UserHasRegistered  $event
     * @return void
     */
    public function handle(UserHasRegistered $event)
    {
        $user = $event->user->toArray();
        $user['password'] = $event->password;
        $this->mailer->send(
            'emails.welcome',
            ['user' => $user],
            function ($message) use ($user) {
                $message
                    ->from('no-reply@mtangaza.com', 'mTangaza - Welcome')
                    ->to($user['email'], $user['first_name'] . ' ' . $user['last_name'])
                    ->subject('Welcome to mTangaza');
            }
        );

    }
}
