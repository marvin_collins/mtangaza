<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Artisan;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->exec(base_path('elasticsearch/bin/elasticsearch') . ' -d')
            ->everyMinute()
            ->when(function () {
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "http://localhost:9200/");
                curl_setopt($ch, CURLOPT_HEADER, 0);
                $result = curl_exec($ch);
                curl_close($ch);

                return $result == false;
            });

        $schedule->call(function () {
            Artisan::call('queue:work', [
                '--daemon', '--sleep' => '20'
            ]);
        })
            ->name('queue')
            ->everyMinute()
            ->withoutOverlapping()
            ->appendOutputTo(storage_path('logs/schedule.txt'));

    }
}
