<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use SearchIndex;
use SmoDav\Models\Business;
use SmoDav\Models\BusinessImage;
use SmoDav\Models\FieldOfWork;
use SmoDav\Models\PendingImageChange;

class IndexerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        FieldOfWork::created(function ($field) {
            SearchIndex::upsertToIndex($field);
        });
        FieldOfWork::updated(function ($field) {
            SearchIndex::upsertToIndex($field);
        });
        FieldOfWork::deleting(function ($field) {
            SearchIndex::removeFromIndex($field);
        });

        Business::created(function ($business) {
            if ($business->status == Business::STATUS_APPROVED) {
                SearchIndex::upsertToIndex($business);
            }
        });
        Business::updated(function ($business) {
            if ($business->status == Business::STATUS_APPROVED) {
                SearchIndex::upsertToIndex($business);
            }
        });
        Business::deleting(function ($business) {
            SearchIndex::removeFromIndex($business);
            if ($business->logo != 'images/default-logo.png') {
                unlink(public_path($business->logo));
            }
        });

        BusinessImage::deleting(function ($image) {
            unlink(public_path($image->image));
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
