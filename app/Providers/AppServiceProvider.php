<?php

namespace App\Providers;

use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\ServiceProvider;
use SearchIndex;
use SmoDav\Managers\Notification;
use SmoDav\Models\Category;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
//        dd(Notification::getUnread());
        view()->composer(['admin.layout', 'client.layout'], function ($view) {
            $user = Sentinel::getUser();
            $view->withUser($user)->withNotifications(Notification::getUnread());
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
