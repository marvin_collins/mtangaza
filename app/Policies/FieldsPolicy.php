<?php

namespace App\Policies;

use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Auth\Access\HandlesAuthorization;
use SmoDav\Models\FieldOfWork;

class FieldsPolicy
{
    use HandlesAuthorization;

    public function create()
    {
        return Sentinel::hasAnyAccess([FieldOfWork::PERMISSION_CREATE, 'superuser']);
    }

    public function update()
    {
        return Sentinel::hasAnyAccess([FieldOfWork::PERMISSION_UPDATE, 'superuser']);
    }

    public function view()
    {
        return Sentinel::hasAnyAccess([FieldOfWork::PERMISSION_VIEW, 'superuser']);
    }

    public function delete()
    {
        return Sentinel::hasAnyAccess([FieldOfWork::PERMISSION_DELETE, 'superuser']);
    }
}
