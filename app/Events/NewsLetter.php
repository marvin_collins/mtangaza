<?php
/**
 * Created by PhpStorm.
 * User: marvin
 * Date: 11/7/16
 * Time: 9:57 AM
 */

namespace App\Events;


use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;

class NewsLetter extends Event
{
    use SerializesModels;
    public $request;

    /**
     * NewsLetter constructor.
     * @param $message
     * @param $file
     * @param $email
     */
    public function __construct(Request $req)
    {
        $this->request = $req;
    }

    public function broadcastOn()
    {
        return [];
    }


}