<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Support\Collection;
use SmoDav\Models\Business;

class NewFollowRequest extends Event implements ShouldQueue
{
    use SerializesModels;
    /**
     * @var
     */
    public $follower;
    /**
     * @var Collection
     */
    public $followed;

    /**
     * Create a new event instance.
     *
     * @param Business $follower
     * @param Collection $followed
     */
    public function __construct(Business $follower, Collection $followed)
    {
        $this->follower = $follower;
        $this->followed = $followed;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
