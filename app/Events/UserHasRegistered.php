<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use SmoDav\Models\Auth\User;

class UserHasRegistered extends Event
{
    use SerializesModels;
    /**
     * @var User
     */
    public $user;
    /**
     * @var
     */
    public $password;

    /**
     * Create a new event instance.
     *
     * @param User $user
     * @param      $password
     */
    public function __construct(User $user, $password)
    {
        $this->user = $user;
        $this->password = $password;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
