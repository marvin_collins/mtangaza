<?php
/**
 * Created by PhpStorm.
 * User: marvin
 * Date: 11/8/16
 * Time: 9:03 AM
 */

namespace App\Events;


use Illuminate\Queue\SerializesModels;

class NotificationMail extends Event
{
    use SerializesModels;

    public $follower;
    public $business;

    /**
     * NotificationMail constructor.
     * @param $follower
     * @param $business
     */
    public function __construct($follower, $business)
    {
        $this->follower = $follower;
        $this->business = $business;
    }

    public function broadcastOn()
    {
        return [];
    }

}