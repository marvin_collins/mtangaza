<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Today extends Model
{
    protected  $fillable = ['ip_address','plan_id','total'];
}
