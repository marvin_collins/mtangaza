<?php

namespace SmoDav\Models;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    const MODULE_ID = 4;
    const SILVER_PACKAGE = 'silver_package';
    const GOLD_PACKAGE = 'gold_package';
    const PLATINUM_PACKAGE = 'platinum_package';

    protected $fillable = ['name', 'description', 'cost'];

    public function features()
    {
        return $this->belongsToMany(Module::class, 'modules_plans');
    }

    public function subscriptions()
    {
        return $this->hasMany(Subscription::class);
    }
}
