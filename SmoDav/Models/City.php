<?php

namespace SmoDav\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public function businesses()
    {
        return $this->belongsToMany(Business::class, 'business_cities');
    }
}
