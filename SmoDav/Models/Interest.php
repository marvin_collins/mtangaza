<?php

namespace SmoDav\Models;

use Illuminate\Database\Eloquent\Model;

class Interest extends Model
{
    protected $table = 'businesses_interests';
    
    protected $fillable = ['clients_id', 'field_of_work_id'];

    public function field()
    {
        return $this->belongsTo(FieldOfWork::class, 'field_of_work_id');
    }
}
