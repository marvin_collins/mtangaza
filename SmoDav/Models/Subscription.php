<?php

namespace SmoDav\Models;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $fillable = [
        'business_id', 'plan_id', 'subscription_start', 'subscription_end'
    ];
    const PERMISSION_CREATE = 'subscription.create';

    const PERMISSION_UPDATE = 'subscription.update';

    const PERMISSION_VIEW   = 'subscription.view';

    const PERMISSION_DELETE = 'subscription.delete';

    public function business()
    {
        return $this->belongsTo(Business::class);
    }

    public function plan()
    {
        return $this->belongsTo(Plan::class);
    }
}
