<?php

namespace SmoDav\Models;

use Illuminate\Database\Eloquent\Model;

class Inquiry extends Model
{
    protected $fillable = ['business_id', 'full_name', 'email', 'mobile', 'subject', 'message', 'status'];
    
    const UNREAD = 0;
    const READ = 1;
}
