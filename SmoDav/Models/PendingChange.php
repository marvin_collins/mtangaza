<?php

namespace SmoDav\Models;

use Illuminate\Database\Eloquent\Model;

class PendingChange extends Model
{
    protected $fillable = [
        'id', 'client_id', 'category_id', 'name', 'slug', 'logo', 'description', 'email','contact_person',
        'contact_numbers', 'map_location', 'website', 'twitter', 'facebook',
        'linked_in', 'google_plus', 'skype', 'active', 'status', 'plan_id', 'subscription_end'
    ];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function fieldsOfWork()
    {
        return $this->belongsToMany(FieldOfWork::class, 'businesses_field_of_works', 'business_id');
    }

    public function images()
    {
        return $this->hasMany(PendingImageChange::class, 'business_id');
    }

    public function subscriptions()
    {
        return $this->hasMany(Subscription::class, 'business_id');
    }

    public function cities()
    {
        return $this->belongsToMany(City::class, 'business_cities', 'business_id');
    }

    public function payments()
    {
        return $this->hasMany(Payment::class, 'business_id');
    }
}
