<?php

namespace SmoDav\Models;

use Illuminate\Database\Eloquent\Model;
use SmoDav\Models\Auth\User;

class SubClient extends Model
{
    protected $fillable = [
        'user_id', 'client_id', 'title', 'email', 'first_name', 'last_name', 'gender', 'nationality',
        'identification_type', 'identification_number', 'mobile', 'address'
    ];

    const PERMISSION_CREATE = 'sub_client.create';

    const PERMISSION_UPDATE = 'sub_client.update';

    const PERMISSION_VIEW   = 'sub_client.view';

    const PERMISSION_DELETE = 'sub_client.delete';

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function delete()
    {
        $this->user()->delete();

        return parent::delete();
    }

    public function businesses()
    {
        return $this->hasMany(Business::class);
    }


}
