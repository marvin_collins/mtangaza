<?php

namespace SmoDav\Models;

use Illuminate\Database\Eloquent\Model;

class BusinessesFieldOfWork extends Model
{
    protected $fillable = ['business_id', 'field_of_work_id'];
}
