<?php

namespace SmoDav\Models;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $fillable = ['name', 'slug', 'description', 'active'];

    const PERMISSION_UPDATE = 'module.update';

    const FEATURED = 'Featured Listing';
    const DESCRIPTION = 'Business Description';
    const LOGO = 'Business Logo';
    const WEBSITE = 'Website Address';
    const SKYPE = 'Skype Click to Call';
    const SOCIAL_MEDIA = 'Social Media Links';
    const DIRECT_INQUIRY = 'Direct Inquiry Form';
    const IMAGES = 'Personal Images Upload';
    const MAP = 'Google Map';
    const CONTACT_ADDRESS = 'Business Contacts';
    const BUSINESS_RELATIONS = 'Direct Business Relations';
    const ATTACHMENTS = 'Attachments For Clients';
    const SMS = 'SMS Notification';
    const EMAIL = 'Email Notification';
    const TRACKING = 'Advert Tracking';

    public function plans()
    {
        return $this->belongsToMany(Plan::class, 'modules_plans');
    }
}
