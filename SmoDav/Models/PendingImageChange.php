<?php

namespace SmoDav\Models;

use Illuminate\Database\Eloquent\Model;

class PendingImageChange extends Model
{
    protected $fillable = ['id', 'business_id', 'image', 'position'];
}
