<?php

namespace SmoDav\Models;

use Illuminate\Database\Eloquent\Model;

class Followers extends Model
{
    protected $fillable = ['follower_id','status', 'business_id'];
    
    public function business()
    {
        return $this->belongsTo(Business::class,'business_id','id');
    }

    public function follower()
    {
        return $this->belongsTo(Business::class, 'follower_id');
    }
}
