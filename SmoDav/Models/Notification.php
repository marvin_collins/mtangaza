<?php

namespace SmoDav\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    const UNREAD = 0;
    const READ = 1;

    const CRITICAL = 1;
    const URGENT = 2;
    const NORMAL = 3;
    const BELOW_NORMAL = 4;
    const LOW_PRIORITY = 5;
}
