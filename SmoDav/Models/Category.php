<?php

namespace SmoDav\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\SearchIndex\Searchable;

class Category extends Model implements Searchable
{
    const PERMISSION_CREATE = 'category.create';

    const PERMISSION_UPDATE = 'category.update';

    const PERMISSION_VIEW   = 'category.view';

    const PERMISSION_DELETE = 'category.delete';

    protected $fillable = ['name', 'slug'];

    public function getSearchableBody()
    {
        return [
            'name' => $this->name,
            'slug' => $this->slug,
            'description' => '',
            'logo' => 'images/default-logo.png',
            'active' => 1,
            'url' => 'categories/' . $this->slug
        ];
    }

    public function getSearchableType()
    {
        return 'category';
    }

    public function getSearchableId()
    {
        return $this->id;
    }

    public function businesses()
    {
        return $this->hasMany(Business::class);
    }
}
