<?php

namespace SmoDav\Models\Auth;

use Cartalyst\Sentinel\Roles\EloquentRole;

/**
 * Class Role
 *
 * @category PHP
 * @package  SmoDav\Models\Auth
 * @author   David Mjomba <smodavprivate@gmail.com>
 */
class Role extends EloquentRole
{

    /**
     * The module number for the class
     */
    const MODULE_ID = 2;

    /**
     * Database representation of superuser slug
     */
    const SUPERUSER = 'superuser';

    /**
     * Database representation of administrator slug
     */
    const ADMINISTRATOR = 'administrator';

    /**
     * Database representation of account owner slug
     */
    const ACCOUNT_OWNER = 'account_owner';

    /**
     * Database representation of account user slug
     */
    const ACCOUNT_USER = 'account_user';

    /**
     * Slug for backend access
     */
    const BACKEND_ACCESS = 'administration.access';

    const CLIENT_ACCESS = 'client.access';

    const CLIENT_ACCOUNT_OWNER = 'client.owner';

}
