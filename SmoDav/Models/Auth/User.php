<?php
/**
 * @category PHP
 * @author   David Mjomba <smodavprivate@gmail.com>
 */

namespace SmoDav\Models\Auth;

use Cartalyst\Sentinel\Users\EloquentUser;
use SmoDav\Models\Client;
use SmoDav\Models\SubClient;

class User extends EloquentUser
{
    const MODULE_ID = 1;

    const PERMISSION_ACCESS = 'user.any';

    const PERMISSION_CREATE = 'user.create';

    const PERMISSION_UPDATE = 'user.update';

    const PERMISSION_VIEW   = 'user.view';

    const PERMISSION_DELETE = 'user.delete';
    
    public function client()
    {
        return $this->hasOne(Client::class);
    }

    public function subClient()
    {
        return $this->hasOne(SubClient::class);
    }
}