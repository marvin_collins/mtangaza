<?php

namespace SmoDav\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\SearchIndex\Searchable;

class Business extends Model implements Searchable
{
    public $contact_form = true;

    public $featured = true;

    protected $fillable = [
        'client_id', 'category_id', 'name', 'slug', 'logo', 'description', 'email','contact_person',
        'contact_numbers', 'map_location', 'website', 'twitter', 'facebook',
        'linked_in', 'google_plus', 'skype', 'active', 'status', 'plan_id', 'subscription_end'
    ];

    protected $dates = ['subscription_end'];

    const PERMISSION_ACCESS = 'business.any';

    const PERMISSION_CREATE = 'business.create';

    const PERMISSION_UPDATE = 'business.update';

    const PERMISSION_VIEW   = 'business.view';

    const PERMISSION_DELETE = 'business.delete';

    const STATUS_PENDING = 0;

    const STATUS_APPROVED = 1;

    const STATUS_DISAPPROVED = 2;

    // maps fields to modules
    const FEATURED = 'featured';
    const DESCRIPTION = 'description';
    const LOGO = 'logo';
    const WEBSITE = 'website';
    const SKYPE = 'skype';
    const SOCIAL_MEDIA = ['google_plus', 'linked_in', 'twitter', 'facebook'];
    const DIRECT_INQUIRY = 'contact_form';
    const IMAGES = 'images';
    const MAP = 'map_location';
    const CONTACT_ADDRESS = ['contact_numbers', 'contact_person'];
    const BUSINESS_RELATIONS = 'handshakes';
    const ATTACHMENTS = 'attachments';
    const SMS = 'sms';
    const EMAIL = 'email';
    const TRACKING = 'track';

    public function getSearchableBody()
    {
        return [
            'name' => $this->name,
            'slug' => $this->slug,
            'description' => $this->description,
            'logo' => $this->logo,
            'website' => $this->website,
            'active' => $this->active,
            'url' => 'businesses/' . $this->slug,
            'work_field' => $this->fieldsOfWork
        ];
    }

    public function getSearchableType()
    {
        return 'business';
    }

    public function getSearchableId()
    {
        return $this->id;
    }

    public function getConstant($constant)
    {
        return constant('self::' . $constant);
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }
    public function followers()
    {
        return $this->hasMany(Followers::class,'business_id','id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function fieldsOfWork()
    {
        return $this->belongsToMany(FieldOfWork::class, 'businesses_field_of_works');
    }

    public function images()
    {
        return $this->hasMany(BusinessImage::class);
    }

    public function subscriptions()
    {
        return $this->hasMany(Subscription::class);
    }

    public function cities()
    {
        return $this->belongsToMany(City::class, 'business_cities');
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }
}
