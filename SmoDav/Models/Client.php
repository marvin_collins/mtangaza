<?php

namespace SmoDav\Models;

use Illuminate\Database\Eloquent\Model;
use SmoDav\Models\Auth\Role;
use SmoDav\Models\Auth\User;

class Client extends Model
{
    protected $fillable = [
        'user_id', 'title', 'email', 'first_name', 'middle_name','last_name', 'gender', 'nationality',
        'identification_type', 'identification_number', 'mobile','country','city','address1',    'address2','newsletter'
    ];

    const MODULE_ID = 3;

    const PERMISSION_CREATE = 'client.create';

    const PERMISSION_UPDATE = 'client.update';

    const PERMISSION_VIEW   = 'client.view';

    const PERMISSION_DELETE = 'client.delete';

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function delete()
    {
        $this->user()->delete();
        
        return parent::delete();
    }

    public function businesses()
    {
        return $this->hasMany(Business::class);
    }

}
