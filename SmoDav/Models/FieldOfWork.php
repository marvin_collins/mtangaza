<?php

namespace SmoDav\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\SearchIndex\Searchable;

class FieldOfWork extends Model implements Searchable
{
    const PERMISSION_CREATE = 'fields.create';

    const PERMISSION_UPDATE = 'fields.update';

    const PERMISSION_VIEW   = 'fields.view';

    const PERMISSION_DELETE = 'fields.delete';

    protected $fillable = ['name', 'slug','featured','image_path'];

    public function getSearchableBody()
    {
        return [
            'name' => $this->name,
            'slug' => $this->slug,
            'description' => 'Field of work',
            'logo' => 'images/default-logo.png',
            'active' => 1,
            'url' => 'fields/' . $this->slug
        ];
    }

    public function getSearchableType()
    {
        return 'field_of_work';
    }

    public function getSearchableId()
    {
        return $this->id;
    }

    public function business()
    {
        return $this->belongsToMany(Business::class, 'businesses_field_of_works');
    }

    public function interests()
    {
        return $this->hasMany(Interest::class);
    }

}
