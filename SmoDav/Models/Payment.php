<?php

namespace SmoDav\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'business_id', 'client_id', 'plan_id', 'paybill_number', 'account_number',
        'amount_paid', 'transaction_number', 'status','upgradetype'
    ];

    const PERMISSION_ACCESS = 'payment.any';

    const PERMISSION_CREATE = 'payment.create';

    const PERMISSION_UPDATE = 'payment.update';

    const PERMISSION_VIEW   = 'payment.view';

    const PERMISSION_DELETE = 'payment.delete';

    const STATUS_PENDING = 0;

    const STATUS_APPROVED = 1;

    const STATUS_DISAPPROVED = 2;

    public function business()
    {
        return $this->belongsTo(Business::class);
    }
}
