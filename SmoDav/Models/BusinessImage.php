<?php

namespace SmoDav\Models;

use Illuminate\Database\Eloquent\Model;

class BusinessImage extends Model
{
    protected $fillable = ['business_id', 'image', 'position'];
    
    public function business()
    {
        return $this->belongsTo(Business::class);
    }
}
