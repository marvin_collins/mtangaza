<?php
/**
 * Created by PhpStorm.
 * User: marvin
 * Date: 11/5/16
 * Time: 1:13 PM
 */

namespace SmoDav\Managers;


use SmoDav\Models\Business;
use SmoDav\Models\Followers;

class Notification
{
    public static function getUnread()
    {
        $client_business = Business::all();
        $notifications = Followers::whereHas('business',
            function($query) {
                $query->where('client_id',session('client_id'));
            })->with('business')->where('status',0)->get()
            ->map(function ($follow) use($client_business)
            {
                return ['id' => $follow->id,
                    'business' => $client_business->where('id', $follow->business_id)->first()->name,
                    'follower' => $client_business->where('id', $follow->follower_id)->first()->name,
                    'date' => $follow->created_at];
            });
        return $notifications;
    }
}