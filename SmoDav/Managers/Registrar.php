<?php
/**
 * @category PHP
 * @author   David Mjomba <smodavprivate@gmail.com>
 */

namespace SmoDav\Managers;

use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use SmoDav\Models\Auth\User;

/**
 * Class Registrar
 *
 * @category PHP
 * @package  SmoDav\Managers
 * @author   David Mjomba <smodavprivate@gmail.com>
 */
class Registrar
{
    /**
     * The user instance
     *
     * @var User $user
     */
    private $user;

    /**
     * The generated user password
     *
     * @var string
     */
    private $userPassword;

    /**
     * Registers a new system user and generates random initial password
     *
     * @param $firstName
     * @param $lastName
     * @param $email
     *
     * @return $this
     */
    public function register($firstName, $lastName, $email)
    {
        $this->userPassword = str_random(15);
        $credentials = [
            'first_name' => $firstName,
            'last_name' => $lastName,
            'email' => $email,
            'password' => $this->userPassword,
            'active' => true
        ];
        $this->user = Sentinel::create($credentials);

        return $this;
    }

    /**
     * Assign the user a given system role
     *
     * @param $roleSlug
     *
     * @return $this
     */
    public function assignRole($roleSlug)
    {
        $role = Sentinel::findRoleBySlug($roleSlug);
        $role->users()->attach($this->user);

        return $this;
    }

    public function registerAndActivate($firstName, $lastName, $email)
    {
        $this->userPassword = str_random(15);
        $credentials = [
            'first_name' => $firstName,
            'last_name' => $lastName,
            'email' => $email,
            'password' => $this->userPassword,
            'active' => true
        ];
        $this->user = Sentinel::registerAndActivate($credentials);

        return $this;
    }

    /**
     * Get the generated user password
     *
     * @return string
     */
    public function getUserPassword()
    {
        return $this->userPassword;
    }

    /**
     * Set the user instance
     *
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get the user instance
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }
}
