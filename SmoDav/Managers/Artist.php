<?php
/**
 * @category PHP
 * @author   David Mjomba <smodavprivate@gmail.com>
 */

namespace SmoDav\Managers;

use Carbon\Carbon;
use Intervention\Image\Facades\Image;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class Artist
 *
 * @category PHP
 * @package  SmoDav\Managers
 * @author   David Mjomba <smodavprivate@gmail.com>
 */
class Artist
{
    /**
     * @var string
     */
    public $outputPath;

    /**
     * @var string
     */
    public $filename;

    /**
     * @var int
     */
    private $thumbWidth;

    /**
     * @var int
     */
    private $thumbHeight;

    /**
     * @var int
     */
    private $imgWidth;

    /**
     * @var int
     */
    private $imgHeight;

    /**
     * @var
     */
    private $createdImage;

    /**
     * @var string
     */
    private $extension;

    /**
     * @var UploadedFile
     */
    private $uploadedImage;

    /**
     * @var bool
     */
    private $thumbnail = false;

    /**
     * Artist constructor.
     */
    public function __construct()
    {
        $this->thumbWidth = env('THUMB_WIDTH', 300);
        $this->thumbHeight = env('THUMB_HEIGHT', 300);
        $this->imgWidth = env('IMG_WIDTH', 700);
        $this->imgHeight = env('IMG_HEIGHT', 700);
        $this->outputPath = env('IMG_OUTPUT_PATH', 'images/');
    }

    /**
     * Set up the Artist to sketch which image
     *
     * @param UploadedFile $image
     *
     * @return $this
     */
    public function useFile(UploadedFile $image)
    {
        $this->uploadedImage = $image;
        $this->extension = $image->getClientOriginalExtension();
        $now = Carbon::now()->timestamp;
        $this->filename = str_random(10) . $now . '.' . $this->extension;

        return $this;
    }

    /**
     * Create and image and its thumbnail
     *
     * @return $this
     */
    public function createImageAndThumbnail()
    {
        $createdImages = [];
        $this->createThumbnail();
        $createdImages [] = [
            'image' => $this->createdImage,
            'thumb' => true
        ];
        $this->create();
        $createdImages [] = [
            'image' => $this->createdImage,
            'thumb' => false
        ];
        $this->createdImage = $createdImages;

        return $this;
    }

    /**
     * Create only the thumbnail
     *
     * @return $this
     */
    public function createThumbnail()
    {
        $this->thumbnail = true;
        $this->createdImage = $this->generateImage($this->thumbWidth, $this->thumbHeight);

        return $this;
    }

    /**
     * Create only the image
     *
     * @return $this
     */
    public function create()
    {
        $this->thumbnail = false;
        $this->createdImage = $this->generateImage($this->imgWidth, $this->imgHeight);

        return $this;
    }

    /**
     * Generate the image using the dimensions with a 10px padding
     *
     * @param $width
     * @param $height
     *
     * @return mixed
     */
    private function generateImage($width, $height)
    {
        $uploaded = Image::make($this->uploadedImage)
            ->resize($width - 10, $height - 10, function ($constraint) {
                $constraint->aspectRatio();
            });
        $generatedImage = Image::canvas($width, $height)
            ->insert($uploaded, 'center');

        return $generatedImage;
    }

    /**
     * Set up the Artist to save the file
     *
     * @return string
     */
    public function save()
    {
        if (is_array($this->createdImage)) {
            $filename = '';
            foreach ($this->createdImage as $image) {
                $filename = $this->saveImage($image['image'], $image['thumb']);
            }

            return $filename;
        }
        return $this->saveImage($this->createdImage, $this->thumbnail);
    }

    /**
     * Save the image to storage
     *
     * @param $image
     * @param $thumbnail
     *
     * @return string
     */
    public function saveImage($image, $thumbnail)
    {
        $filename = $this->filename;
        if ($thumbnail) {
            $filename = 'thumb-' . $filename;
        }
        $image->save(public_path($this->outputPath . '/' . $filename));

        return $this->outputPath . '/' . $filename;
    }
}
