<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

function makeSlug($inputString, $compareColumn, Model $model, $duplicate = false)
{
    $slug = Str::slug($inputString);
    $count = $model->whereRaw($compareColumn . " RLIKE '^{$slug}(-[0-9]+)?$'")->count();
    if ($duplicate) {
        $count = null;
    }
    return $count ? "{$slug}-{$count}" : $slug;
}

function generateView(Request $request, $view)
{
    return view($request->ajax() ? 'ajax.' . $view : $view);
}

function flash($message, $status)
{
    session()->flash('flash_message', '<strong>mTangaza</strong><br>' . $message);
    session()->flash('flash_message_status', $status);
}

function generatePDF($view, $orientation = 'portrait')
{
    $pdf = new DOMPDF();
    $pdf->set_option('enable_remote', true);
    $pdf->load_html($view->render());
    $pdf->set_paper('A4', $orientation);
    $pdf->render();

    header('Content-Type: application/pdf');
    return $pdf->stream('payroll.pdf', ['Attachment' => 0]);
}
