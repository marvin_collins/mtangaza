<?php


namespace SmoDav\Commons;
use App\Today;
use Carbon\Carbon;
use Request;
use SmoDav\Models\Business;

class DashboardSat
{
    public static function dashboardsat()
    {
        return  new self();
    }

    public function setHitsToday($plan_id)
    {
        $now = Carbon::now();
        $user_ip_address = Request::ip();
        $all_hits_today = Today::all(['ip_address','plan_id','created_at'])->toArray();
        if (empty($all_hits_today))
        {
          self::insertData($user_ip_address,$plan_id,$now);
            return null;
        }

        foreach ($all_hits_today as $values)
        {
            if(Carbon::parse($values['created_at'])->format('d-m-y')
                == Carbon::parse(Carbon::today())->format('d-m-y'))
            {

                if(in_array($plan_id,array_flatten(Today::select('plan_id')
                ->where('created_at','>',Carbon::today())->get()->toArray())) &&
                        in_array($user_ip_address,array_flatten(Today::select('ip_address')
                            ->where('created_at','>',Carbon::today())->get()->toArray())))
                {
                    $hit =Today::where('plan_id', $plan_id)
                        ->where('created_at','>',Carbon::today())->first();
                    $hit->update(['total' => ($hit->total + 1)]);
                    break ;
                }
                else {
                    self::insertData($user_ip_address, $plan_id, $now);
                    break ;
                }
            }

        }
    }

    public function getHitsTodays()
    {
        $today_date = Carbon::today();
        $all_hits = Today::all();
        $today_hits = $all_hits->reject(function($values) use($today_date)
        {
            return $values->created_at <  $today_date;
        });
        return [
            'silver' => $today_hits->where('plan_id',1)->sum('total'),
            'gold' => $today_hits->where('plan_id',2)->sum('total'),
            'platinum' => $today_hits->where('plan_id',3)->sum('total')
            ];
    }

    public function getTotalHits()
    {
        $all_hits = Today::all();
        return [
            'silver' => $all_hits->where('plan_id',1)->sum('total'),
            'gold' => $all_hits->where('plan_id',2)->sum('total'),
            'platinum' => $all_hits->where('plan_id',3)->sum('total')
        ];
    }

    public function getRegisteredTodayBusiness($all_business)
    {
        $today_date = Carbon::today();
        $today_business = $all_business->reject(function($values) use($today_date)
        {
            return $values->created_at <  $today_date;
        });
        return count($today_business);
    }

    public function getRegisteredMonthBusiness($all_business)
    {
        $today_date = Carbon::now();
        $month_business = $all_business->reject(function($values) use($today_date)
        {
            return Carbon::parse($values->created_at)->format('m') <  $today_date->month;
        });
        return count($month_business);
    }

    public function getRegisteredYearBusiness($all_business)
    {
        $today_date = Carbon::now();
        $year_business = $all_business->reject(function($values) use($today_date)
        {
            return Carbon::parse($values->created_at)->format('Y') <  $today_date->year;
        });
        return count($year_business);
    }


    public function getRegisteredTodayFollowers($followers)
    {
        $today_date = Carbon::today();
        $today_business = $followers->reject(function($values) use($today_date)
        {
            return $values->created_at <  $today_date;
        });
        return count($today_business);
    }

    public function getRegisteredMonthFollowers($followers)
    {
        $today_date = Carbon::now();
        $month_business = $followers->reject(function($values) use($today_date)
        {
            return Carbon::parse($values->created_at)->format('m') <  $today_date->month;
        });
        return count($month_business);
    }

    public function getRegisteredYearFollowers($followers)
    {
        $today_date = Carbon::now();
        $year_business = $followers->reject(function($values) use($today_date)
        {
            return Carbon::parse($values->created_at)->format('Y') <  $today_date->year;
        });
        return count($year_business);
    }
    public function insertData($user_ip_address,$plan_id,$now)
    {
        Today::insert([
            'ip_address' => $user_ip_address,
            'plan_id' => $plan_id,
            'total' => 1,
            'created_at' => $now,
            'updated_at' => $now,
        ]);
    }
}