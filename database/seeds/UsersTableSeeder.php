<?php

use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Database\Seeder;
use SmoDav\Models\Auth\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        rxT2kr6TarOX7aU lvGurbZTy8idaSU

        $credentials = [
            'email'    => 'smodavprivate@gmail.com',
            'password' => 'Mybd4y!26'
        ];
//        xFZcfLVe5UJZTJe
//        LORImwuqC6hGZ7U
        $user = Sentinel::registerAndActivate($credentials);
        $role = Sentinel::findRoleBySlug(Role::SUPERUSER);
        $role->users()->attach($user);

        $credentials = [
            'first_name' => 'Wise & Agile',
            'email'    => 'webmaster@wizag.biz',
            'password' => '!!Qwerty123!!'
        ];

        $user = Sentinel::registerAndActivate($credentials);
        $role = Sentinel::findRoleBySlug(Role::ADMINISTRATOR);
        $role->users()->attach($user);
    }
}
//qSpj47hWQRRloaF
//maman@ubbj.com
//WTDxaRwMr5reuhk