<?php

use Illuminate\Database\Seeder;
use SmoDav\Models\City;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon\Carbon::now();
        $inserts = [
            [
                'id' => null,
                'name' => 'Baragoi',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Bungoma',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Busia',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Butere',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Dadaab',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Diani Beach',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Eldoret',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Emali',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Embu',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Garissa',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Gede',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Hola',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Homa Bay',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Isiolo',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Kitui',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Kibwezi',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Makindu',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Wote',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Mutomo',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Kajiado',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Kakamega',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Kakuma',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Kapenguria',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Kericho',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Kiambu',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Kilifi',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Kisii',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Kisumu',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Kitale',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Lamu',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Langata',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Litein',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Lodwar',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Lokichoggio',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Londiani',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Loyangalani',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Machakos',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Malindi',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Mandera',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Maralal',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Marsabit',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Meru',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Mombasa',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Moyale',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Mumias',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Muranga',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Nairobi',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Naivasha',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Nakuru',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Namanga',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Nanyuki',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Naro Moru',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Narok',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Nyahururu',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Nyeri',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Ruiru',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Shimoni',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Takaungu',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Thika',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Vihiga',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Voi',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Wajir',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Watamu',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Webuye',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => 'Wundanyi',
                'created_at' => $now,
                'updated_at' => $now
            ]
        ];
        City::insert($inserts);
    }
}
