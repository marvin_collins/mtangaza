<?php

use Illuminate\Database\Seeder;
use SmoDav\Models\Plan;

class PlansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon\Carbon::now();
        $inserts = [
            [
                'id' => null,
                'name' => Plan::SILVER_PACKAGE,
                'description' => 'Simple startup package',
                'cost' => 5000,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => Plan::GOLD_PACKAGE,
                'description' => 'Standard business package',
                'cost' => 10000,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => null,
                'name' => Plan::PLATINUM_PACKAGE,
                'description' => 'Premium business package',
                'cost' => 15000,
                'created_at' => $now,
                'updated_at' => $now
            ]
        ];
        Plan::insert($inserts);
    }
}
