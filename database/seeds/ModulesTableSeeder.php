<?php

use Illuminate\Database\Seeder;
use SmoDav\Models\Module;

class ModulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon\Carbon::now();
        $inserts = [
            [
                'id' => 1,
                'name' => Module::FEATURED,
                'slug' => str_slug(Module::FEATURED),
                'constant' => 'FEATURED',
                'description' => 'The business will appear as a featured advert all over the website',
                'active' => true,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => 2,
                'name' => Module::DESCRIPTION,
                'slug' => str_slug(Module::DESCRIPTION),
                'constant' => 'DESCRIPTION',
                'description' => 'The business listing will have its own elaborate description.',
                'active' => true,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => 3,
                'name' => Module::LOGO,
                'slug' => str_slug(Module::LOGO),
                'constant' => 'LOGO',
                'description' => 'The business will be allowed to have its own logo displayed on the site.',
                'active' => true,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => 4,
                'name' => Module::WEBSITE,
                'slug' => str_slug(Module::WEBSITE),
                'constant' => 'WEBSITE',
                'description' => 'The business will have a link to its own website.',
                'active' => true,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => 5,
                'name' => Module::SKYPE,
                'slug' => str_slug(Module::SKYPE),
                'constant' => 'SKYPE',
                'description' => 'The business will have a click to call button for their skype account.',
                'active' => true,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => 6,
                'name' => Module::SOCIAL_MEDIA,
                'slug' => str_slug(Module::SOCIAL_MEDIA),
                'constant' => 'SOCIAL_MEDIA',
                'description' => 'The business will publish its own social media links to be viewed.',
                'active' => true,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => 7,
                'name' => Module::DIRECT_INQUIRY,
                'slug' => str_slug(Module::DIRECT_INQUIRY),
                'constant' => 'DIRECT_INQUIRY',
                'description' => 'The business will have its own inquiry form that the users can contact them through.',
                'active' => true,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => 8,
                'name' => Module::IMAGES,
                'slug' => str_slug(Module::IMAGES),
                'constant' => 'IMAGES',
                'description' => 'The business will have space to upload their preferred images.',
                'active' => true,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => 9,
                'name' => Module::MAP,
                'slug' => str_slug(Module::MAP),
                'constant' => 'MAP',
                'description' => 'The business will have a Google Map where they will pin their location.',
                'active' => true,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => 10,
                'name' => Module::CONTACT_ADDRESS,
                'slug' => str_slug(Module::CONTACT_ADDRESS),
                'constant' => 'CONTACT_ADDRESS',
                'description' => 'The business will have their email and mobile contacts displayed.',
                'active' => true,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => 11,
                'name' => Module::BUSINESS_RELATIONS,
                'slug' => str_slug(Module::BUSINESS_RELATIONS),
                'constant' => 'BUSINESS_RELATIONS',
                'description' => 'The business will have direct contact with other businesses.',
                'active' => true,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => 12,
                'name' => Module::ATTACHMENTS,
                'slug' => str_slug(Module::ATTACHMENTS),
                'constant' => 'ATTACHMENTS',
                'description' => 'The business will be allocated space to upload files for the user\'s view.',
                'active' => true,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => 13,
                'name' => Module::SMS,
                'slug' => str_slug(Module::SMS),
                'constant' => 'SMS',
                'description' => 'The business account owner can be notified through SMS.',
                'active' => true,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => 14,
                'name' => Module::EMAIL,
                'slug' => str_slug(Module::EMAIL),
                'constant' => 'EMAIL',
                'description' => 'The business account owner can be notified through Emails.',
                'active' => true,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => 15,
                'name' => Module::TRACKING,
                'slug' => str_slug(Module::TRACKING),
                'constant' => 'TRACKING',
                'description' => 'The business advert can be analysed and generate click reports.',
                'active' => true,
                'created_at' => $now,
                'updated_at' => $now
            ]
        ];
        Module::insert($inserts);
    }
}
