<?php

use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Database\Seeder;
use SmoDav\Models\Auth\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Sentinel::getRoleRepository()->createModel()->create([
            'id' => 1,
            'name' => 'Superuser',
            'slug' => Role::SUPERUSER,
            'permissions' => [
                'superuser' => true,
                Role::BACKEND_ACCESS => true
            ]
        ]);

        Sentinel::getRoleRepository()->createModel()->create([
            'id' => 2,
            'name' => 'Administrator',
            'slug' => Role::ADMINISTRATOR,
            'permissions' => [
                Role::BACKEND_ACCESS => true
            ]
        ]);

        Sentinel::getRoleRepository()->createModel()->create([
            'id' => 3,
            'name' => 'Account Owner',
            'slug' => Role::ACCOUNT_OWNER,
            'permissions' => [
                Role::BACKEND_ACCESS => false,
                Role::CLIENT_ACCESS => true,
                Role::CLIENT_ACCOUNT_OWNER => true,
            ]
        ]);

        Sentinel::getRoleRepository()->createModel()->create([
            'id' => 4,
            'name' => 'Account User',
            'slug' => Role::ACCOUNT_USER,
            'permissions' => [
                Role::BACKEND_ACCESS => false,
                Role::CLIENT_ACCESS => true,
                Role::CLIENT_ACCOUNT_OWNER => false,
            ]
        ]);

    }
}
