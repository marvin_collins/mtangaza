<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePendingChangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pending_changes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('client_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->string('name');
            $table->string('slug');
            $table->string('logo');
            $table->text('description');
            $table->string('email');
            $table->string('contact_person');
            $table->string('contact_numbers');
            $table->text('map_location');
            $table->string('website');
            $table->string('twitter');
            $table->string('facebook');
            $table->string('linked_in');
            $table->string('google_plus');
            $table->string('skype');
            $table->boolean('active');
            $table->tinyInteger('status')->default(0);
            $table->integer('plan_id');
            $table->timestamp('subscription_end')->nullable();
            $table->timestamps();

            $table->foreign('client_id')
                ->references('id')
                ->on('clients')
                ->onDelete('cascade');

            $table->foreign('category_id')
                ->references('id')
                ->on('categories');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pending_changes');
    }
}
